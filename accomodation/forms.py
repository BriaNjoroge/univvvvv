import datetime

from django import forms
from login.models import University
from college.models import College
from django.forms import ModelForm
from hostel.models import Hostel, HostelRoom
from accomodation.models import AccomodationRequest
from django.contrib import messages
# import the logging library
import logging
from fee_request.models import Fee_Request_accomodation
from django.db.models import Q

# Get an instance of a logger
logger = logging.getLogger(__name__)


class RequestAccomdationForm(forms.Form):
    # FOOD_CHOICES = (('VEG', 'VEG'), ('NON VEG', 'NON VEG'),('BOTH (VEG & NON VEG)', 'BOTH (VEG & NON VEG)'),('JAIN', 'JAIN'))

    ROOM_CHOICES = (('AC', 'AC'), ('NON AC', 'NON AC'))

    SHARING_CHOICES = (('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5'))

    room_pref = forms.ChoiceField(label='Room Preference *', choices=ROOM_CHOICES,
                                  widget=forms.Select(attrs={'placeholder': 'Room Preference', 'class': "form-control"}))

    # food_pref = forms.ChoiceField(label='Food Prefrence', choices=FOOD_CHOICES , widget=forms.Select(attrs={'placeholder':'Food Prefrence','class': "form-control"}))

    sharing_pref = forms.ChoiceField(label='Sharing Prefrence *', choices=SHARING_CHOICES, widget=forms.Select(
        attrs={'placeholder': 'Sharing Preference', 'class': "form-control"}))

    # hostels = forms.ModelMultipleChoiceField(Hostel.objects.filter(is_active=True), widget=forms.SelectMultiple(attrs={'placeholder':'Room Prefrence','class': "form-control", 'size':5}))

    hostels = forms.MultipleChoiceField(label='Select Hostels *', widget=forms.SelectMultiple(
        attrs={'placeholder': 'Room Preference', 'class': "form-control", 'size': 5}))

    def __init__(self, *args, **kwargs):
        self.student_id = kwargs.pop('student_id', None)
        gender = kwargs.pop('gender', None)

        super(RequestAccomdationForm, self).__init__(*args, **kwargs)

        all_hostel = set(Hostel.objects.filter(hostel_type=gender).values_list('name', flat=True))

        CHOICES = list()
        for hostel in all_hostel:
            CHOICES.append((hostel, hostel))

        self.fields['hostels'].choices = CHOICES
        logger.info("Choices of hostels")

    # rigid = forms.BooleanField(label='rigid', required=False, widget = forms.CheckboxInput(attrs={'class': "checkbox"}))

    def clean(self):
        data = AccomodationRequest.objects.filter(student_id_id=self.student_id)

        data = data.exclude(status=2)
        data = data.exclude(status=4)
        data = data.exclude(status=6)
        print(len(data))
        if data.exists():
            self.add_error('hostels', "You are already Applied for accomodation.")


class AssignHostelForm(forms.Form):
    ROOM_CHOICES = (('AC', 'AC'), ('NON AC', 'NON AC'), ('ANY (AC/NON AC)', 'ANY (AC/NON AC)'))

    hostel_prefrence = forms.CharField(label='Hostel Prefrence', max_length=50,
                                       widget=forms.TextInput(attrs={'class': "form-control", "readonly": True}),
                                       required=False)

    room_prefrence = forms.CharField(label='Room Prefrence', max_length=50,
                                     widget=forms.TextInput(attrs={'class': "form-control", "readonly": True}),
                                     required=False)

    sharing_prefrence = forms.CharField(label='Sharing Prefrence', max_length=50,
                                        widget=forms.TextInput(attrs={'class': "form-control", "readonly": True}),
                                        required=False)

    hostels = forms.ChoiceField(label='Select Hostels *', widget=forms.Select(
        attrs={'placeholder': 'Room Prefrence', 'class': "form-control", "id": "id_hostel1"}))

    rooms = forms.ChoiceField(label='Select Room *',
                              widget=forms.Select(attrs={'placeholder': 'Room Prefrence', 'class': "form-control"}))

    start_date = forms.DateField(label='Start Date *',
                                 widget=forms.widgets.DateTimeInput(attrs={"type": "date", 'class': "form-control"}))

    end_date = forms.DateField(label='End Date *',
                               widget=forms.widgets.DateTimeInput(attrs={"type": "date", 'class': "form-control"}))

    remarks = forms.CharField(label='Remarks', max_length=50,
                              widget=forms.TextInput(attrs={'placeholder': 'Remakrs', 'class': "form-control"}),
                              required=False)

    accomdation_id = forms.CharField(widget=forms.HiddenInput())
    student_id = forms.CharField(widget=forms.HiddenInput())

    def __init__(self, *args, **kwargs):
        super(AssignHostelForm, self).__init__(*args, **kwargs)

        all_hostel = set(Hostel.objects.values_list('name', flat=True))

        CHOICES = list()
        for hostel in all_hostel:
            CHOICES.append((hostel, hostel))

        self.fields['hostels'].choices = CHOICES

        all_rooms = set(HostelRoom.objects.values_list('id', 'room_name'))
        self.fields['rooms'].choices = all_rooms

    def clean(self):
        cleaned_data = super(AssignHostelForm, self).clean()
        start_date = cleaned_data.get("start_date")
        end_date = cleaned_data.get("end_date")

        if start_date > datetime.date.today():
            self.add_error('start_date', "Can't select future date")

        if end_date < start_date:
            self.add_error('end_date', "End date should be greater than start date.")


class RejectAccomdationForm(forms.Form):
    accomdation_id = forms.CharField(widget=forms.HiddenInput(attrs={"id": "acc_id"}))
    remarks = forms.CharField(label='Remakrs *', max_length=30,
                              widget=forms.TextInput(attrs={'placeholder': 'Remakrs', 'class': "form-control"}))
    # room_id = forms.CharField(widget=forms.HiddenInput(attrs={"id": "room_id"}))


class AccomodationFeesUpdateForm(forms.Form):
    accomdation_id = forms.CharField(label='Accomodation Request ID *', widget=forms.HiddenInput(attrs={"id": "acc_id1"}))

    approved = forms.ChoiceField(label='Approved *', widget=forms.Select(attrs={'class': "form-control"}),
                                                choices=[(1, "Yes"), (0, "No")])

    amount = forms.CharField(label='Amount *', max_length=30,
                              widget=forms.TextInput(attrs={'placeholder': 'Amount', 'class': "form-control", "readonly": True}))

    remarks = forms.CharField(label='Remakrs *', max_length=50,
                              widget=forms.TextInput(attrs={'placeholder': 'Remakrs', 'class': "form-control"}))


