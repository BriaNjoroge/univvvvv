from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
from hostel.models import HostelRoom, Hostel
from student.models import Student
from fee_request.models import Fee_Request_accomodation

# Create your models here.
# Create your models here.
class AccomodationRequest(models.Model):
    student_id = models.ForeignKey(Student, on_delete=models.CASCADE)
    room_pref = models.CharField(max_length=10)
    # food_pref = models.CharField(max_length=10)
    sharing_pref = models.CharField(max_length=20)
    # rigid = models.BooleanField(default = True)
    remarks = models.CharField(max_length=50, default="")
    created_date = models.DateTimeField(auto_now_add = True)
    updated_date = models.DateTimeField(auto_now = True)

    # hostel_id = models.ForeignKey(Hostel, on_delete = models.CASCADE)
    hostel_ids = models.CharField(max_length=50, default="")
    
    status = models.SmallIntegerField(default = 0)

    temp_room_id = models.ForeignKey(HostelRoom, on_delete=models.CASCADE, blank=True, null=True)
    start_date = models.DateField(null=True)
    end_date = models.DateField(null=True)
    fee = models.ForeignKey(Fee_Request_accomodation, on_delete=models.CASCADE, blank=True, null=True)

# status
# 0 : pending
# 1 : accepted by uni admin
# 2 : rejected by uni admin
# 3 : pending (fees paid by user and waiting for admin confirmation)
# 4 : canceled by user
# 5 : fees approved
# 6 : fees rejected


class Accomodation(models.Model):
    student_id = models.ForeignKey(Student, on_delete=models.CASCADE)
    accomodation_request_id = models.ForeignKey(AccomodationRequest, on_delete = models.CASCADE)
    room_id = models.ForeignKey(HostelRoom, on_delete = models.CASCADE)
    start_date = models.DateField()
    end_date = models.DateField()
    is_active = models.BooleanField(default = True)
    created_date = models.DateTimeField(auto_now_add = True)
    updated_date = models.DateTimeField(auto_now = True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)

    fee = models.ForeignKey(Fee_Request_accomodation, on_delete=models.CASCADE, blank=True, null=True)