from django.conf.urls import url
from django.urls import path, include 
from . import views

urlpatterns = [
    path('view_request', views.view_request, name='view_accomodation_request'),
    path('load_room', views.load_room, name='load_room'),
    path('get_hostel_ids', views.get_hostel_ids, name='get_hostel_ids'),
    # path('semester/<int:id>', views.display_semester, name='display_semester'),
    # path('active_pro/<int:id>', views.active_pro, name='active_pro'), 
    # path('disable_pro/<int:id>', views.disable_pro, name='disable_pro'),       
]