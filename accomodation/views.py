from django.contrib import messages
from django.shortcuts import render, redirect
from .forms import RequestAccomdationForm, AssignHostelForm, RejectAccomdationForm, AccomodationFeesUpdateForm
from .models import AccomodationRequest, Accomodation
from hostel.models import Hostel, HostelRoom
from django.db.models import F
from django.http.response import JsonResponse

from fee_request.models import Fee_Request_accomodation
# import the logging library
import logging

# Get an instance of a logger
logger = logging.getLogger(__name__)

# Create your views here.
def view_request(request):
    kwargs = {'capacity': F('current_capacity')}
    empty_rooms = HostelRoom.objects.filter(is_active=True).exclude(**kwargs)

    data = {}

    accomodation_form = RequestAccomdationForm()
    assign_hostel_form = AssignHostelForm()
    reject_accomodation_form = RejectAccomdationForm()
    accomodation_feesupdate_form = AccomodationFeesUpdateForm()

    if request.method == 'POST':

        if 'submit_request' in request.POST:
            logger.info("submit request form")

            accomodation_form = RequestAccomdationForm(request.POST)

            if accomodation_form.is_valid():
                hostel_list = request.POST.getlist('hostels')
                logger.info(f"hostel_list : {hostel_list} ")

                acc_data = AccomodationRequest()
                acc_data.student_id_id = request.user.Student.id
                acc_data.room_pref = request.POST.get('room_pref')
                acc_data.food_pref = request.POST.get('food_pref')
                acc_data.sharing_pref = request.POST.get('sharing_pref')
                acc_data.hostel_ids = request.POST.getlist('hostels')
                # ridge = request.POST.get('rigid', None)
                # if ridge:
                #     acc_data.rigid = 1
                # else:
                #     acc_data.rigid = 0
                acc_data.created_by_id = request.user.id
                acc_data.status = 0
                acc_data.save()

                # hostels = request.POST.get("hostels")
                # rooms = request.POST.get("rooms")
                # hostel_room = HostelRoom.objects.get(id = rooms, hostel_id_id = hostels)
                # hostel_room.current_capacity += 1
                # hostel_room.save()

                return redirect('/accomodation/view_request')

        elif "assign_request" in request.POST:

            logger.info("Assign request form")

            assign_hostel_form = AssignHostelForm(request.POST)

            if assign_hostel_form.is_valid():
                acc_data = AccomodationRequest.objects.get(id = request.POST.get('accomdation_id'))
                acc_data.temp_room_id_id = request.POST.get('rooms')
                acc_data.start_date = request.POST.get('start_date')
                acc_data.end_date = request.POST.get('end_date')
                acc_data.status = 1
                acc_data.save()

                room_id = request.POST.get('rooms')
                room_data = HostelRoom.objects.get(id=room_id)
                room_data.current_capacity += 1
                room_data.remaining_capacity -= 1
                room_data.save()
                messages.success(request,"Accomodation has been assigned successfully")

                return redirect('/accomodation/view_request')

        elif "reject_request" in request.POST:
            logger.info('reject request form')
            reject_accomodation_form = RejectAccomdationForm(request.POST)

            if reject_accomodation_form.is_valid():
                acc_data = AccomodationRequest.objects.get(id=request.POST.get('accomdation_id'))
                acc_data.status = 2
                acc_data.remarks = request.POST.get('remarks')
                acc_data.save()
                messages.success(request,"Accomodation has been rejected successfully")


                # room_id = request.POST.get('room_id')

                # room_data = HostelRoom.objects.get(id = room_id)
                # room_data.current_capacity -= 1
                # room_data.save()

                return redirect('/accomodation/view_request')

        elif "accept_payment_request" in request.POST:
            logger.info('reject request form')
            accomodation_feesupdate_form = AccomodationFeesUpdateForm(request.POST)

            if accomodation_feesupdate_form.is_valid():
                acc_data = AccomodationRequest.objects.get(id=request.POST.get('accomdation_id'))
                acc_data.remarks = request.POST.get('remarks')

                fee_data = Fee_Request_accomodation.objects.get(id = acc_data.fee.id)

                approved = request.POST.get('approved', None)
                if int(approved) == 1:
                    acc_data.status = 5
                    fee_data.approved = 1

                    acc_data1 = Accomodation()
                    acc_data1.student_id_id = acc_data.student_id_id
                    acc_data1.accomodation_request_id_id = acc_data.id
                    acc_data1.room_id_id = acc_data.temp_room_id_id
                    acc_data1.start_date = acc_data.start_date
                    acc_data1.end_date = acc_data.end_date
                    acc_data1.created_by_id = request.user.id
                    acc_data1.fee_id = fee_data.id

                    acc_data1.save()
                else:
                    acc_data.status = 6
                    fee_data.approved = 0
                print(fee_data)

                acc_data.save()
                fee_data.save()

                return redirect('/accomodation/view_request')

    data["form"] = accomodation_form
    data["assign_hostel_form"] = assign_hostel_form
    data["reject_accomodation_form"] = reject_accomodation_form
    data["accomodation_feesupdate_form"] = accomodation_feesupdate_form

    accomodation_data = AccomodationRequest.objects.all()

    data['accomodation'] = accomodation_data

    pending_accomodation_data = accomodation_data.filter(status=0)
    data['pending_accomodation'] = pending_accomodation_data

    hostel_data = Hostel.objects.filter(is_active=True)
    data['hostel_data'] = hostel_data
    data['empty_rooms'] = empty_rooms

    payment_approval_request_data = accomodation_data.filter(status=3)
    data['payment_approval_request_data'] = payment_approval_request_data

    return render(request, 'display_request.html', data)



def view_request1(request):
    kwargs = {'capacity': F('current_capacity')}
    empty_rooms = HostelRoom.objects.filter(is_active=True).exclude(**kwargs)

    data = {}

    accomodation_form = RequestAccomdationForm()
    assign_hostel_form = AssignHostelForm()
    reject_accomodation_form = RejectAccomdationForm()
    
    if request.method == 'POST':

        if 'submit_request' in request.POST:
            logger.info("submit request form")

            accomodation_form = RequestAccomdationForm(request.POST)

            if accomodation_form.is_valid() :

                hostel_list = request.POST.getlist('hostels')
                logger.info(f"hostel_list : {hostel_list} ")

                acc_data = AccomodationRequest()
                acc_data.student_id_id = request.user.Student.id
                acc_data.room_pref = request.POST.get('room_pref')
                acc_data.food_pref = request.POST.get('food_pref')
                acc_data.sharing_pref = request.POST.get('sharing_pref')
                acc_data.hostel_ids = request.POST.getlist('hostels')
                # ridge = request.POST.get('rigid', None)
                # if ridge:
                #     acc_data.rigid = 1
                # else:
                #     acc_data.rigid = 0
                acc_data.created_by_id = request.user.id
                acc_data.status = 0
                acc_data.save()


                # hostels = request.POST.get("hostels")
                # rooms = request.POST.get("rooms")
                # hostel_room = HostelRoom.objects.get(id = rooms, hostel_id_id = hostels)
                # hostel_room.current_capacity += 1
                # hostel_room.save()

                return redirect('/accomodation/view_request')
                
        elif "assign_request" in request.POST:

            logger.info("Assign request form")

            assign_hostel_form = AssignHostelForm(request.POST)
            
            if assign_hostel_form.is_valid():
                acc_data = Accomodation()
                acc_data.student_id_id = request.POST.get('student_id')
                acc_data.accomodation_request_id_id = request.POST.get('accomdation_id')
                acc_data.room_id_id = request.POST.get('rooms')                
                acc_data.start_date = request.POST.get('start_date')
                acc_data.end_date = request.POST.get('end_date')
                acc_data.created_by_id = request.user.id
                acc_data.save()

                AccomodationRequest.objects.filter(id = request.POST.get('accomdation_id')).update(status = 1)

                room_id = request.POST.get('rooms')
                room_data = HostelRoom.objects.get(id = room_id)
                room_data.current_capacity += 1
                room_data.save()


                return redirect('/accomodation/view_request')

        elif "reject_request" in request.POST:
            logger.info('reject request form')
            reject_accomodation_form = RejectAccomdationForm(request.POST)
            
            if reject_accomodation_form.is_valid():
                acc_data = AccomodationRequest.objects.get(id = request.POST.get('accomdation_id'))
                acc_data.status = 2                
                acc_data.remarks = request.POST.get('remarks')           
                acc_data.save()

                # room_id = request.POST.get('room_id')
                
                # room_data = HostelRoom.objects.get(id = room_id)
                # room_data.current_capacity -= 1
                # room_data.save()

                return redirect('/accomodation/view_request')
    data["form"] = accomodation_form
    data["assign_hostel_form"] = assign_hostel_form
    data["reject_accomodation_form"] = reject_accomodation_form


    accomodation_data = AccomodationRequest.objects.all()

    accomodation_pending_request_data = accomodation_data.filter(status = 0)

    data['accomodation'] = accomodation_data

    pending_accomodation_data = accomodation_data.filter(status = 0)
    data['pending_accomodation'] = pending_accomodation_data

    hostel_data = Hostel.objects.filter(is_active = True)
    data['hostel_data'] = hostel_data
    data['empty_rooms'] = empty_rooms

    data['pending_request'] = accomodation_pending_request_data


    return render(request, 'display_request.html', data)


def load_room(request):
    logger.info(f"hello {request.GET.get('id_hostel')}")
    
    kwargs = {'capacity':F('current_capacity')}

    hostel_name = request.GET.get('hostel_name')
    room_type = request.GET.get('room_type')
    sharing_pref = request.GET.get('sharing_pref')

    rooms = HostelRoom.objects.filter(hostel_id__name=hostel_name, room_type=room_type, capacity=sharing_pref, is_active = True).exclude(**kwargs)
    
    #generate an html template for the specific option
    return render(request, 'list_rooms.html', {'rooms': rooms})


def get_hostel_ids(request):
    rooms_list = list(set(HostelRoom.objects.filter(room_type = request.GET['room_type'], capacity = request.GET['sharing_pref'], is_active=True, hostel_id__is_active = 1).values_list('hostel_id__name')))
    logger.info(f"room list {rooms_list}")
    return JsonResponse(rooms_list,safe=False)
