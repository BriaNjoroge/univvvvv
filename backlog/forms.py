from django import forms
from django.contrib import auth
from django.forms import ModelForm
from .models import backlogfee , backlogteacher
from student.models import Student
from course.models import Course

from exam_appear.models import appearance ,attendance
from results.models import result
from django.shortcuts import get_object_or_404

class backlog_fee_form(forms.Form):
    CHOICES = (('Cash', 'Cash'), ('Cheque', 'Cheque'), ('Online Payment', 'Online Payment'))

    receipt_no = forms.CharField(required=True, label='Receipt Number *', max_length=100, widget=forms.TextInput(
        attrs={'class': "form-control", 'placeholder': 'Receipt Number'}))
    payment_mode = forms.ChoiceField(choices=CHOICES, label='Payment Mode *',
                                     widget=forms.Select(attrs={'class': "form-control"}))
    transaction_id = forms.CharField(required=False, label='Transaction ID', widget=forms.TextInput(
        attrs={'class': "form-control", 'placeholder': 'Transaction ID'}))
    cheque_no = forms.CharField(required=False, label='Cheque Number',
                                widget=forms.TextInput(attrs={'class': "form-control", 'placeholder': 'Cheque Number'}))
    payment_date = forms.DateField(label='Payment Date *',
                                   widget=forms.widgets.DateTimeInput(attrs={"type": "date", 'class': "form-control"}))
    payment_proof = forms.FileField(required=False, label='Payment Proof', widget=forms.FileInput(
        attrs={'class': "form-control", 'placeholder': 'Payment Proof', 'accept': 'application/pdf,image/*'}))

    amount = forms.DecimalField(label='Amount *', widget=forms.NumberInput(attrs={'class': "form-control"}))

    courseid = forms.MultipleChoiceField(label='Select course *', widget=forms.SelectMultiple(
        attrs={'placeholder': 'Select course', 'class': "form-control", 'size': 5}))

    def __init__(self, *args, **kwargs):
        self.student_id = kwargs.pop('student_id', None)
        print(self.student_id) 

        super(backlog_fee_form, self).__init__(*args, **kwargs)
        student = get_object_or_404(Student, id=self.student_id)
        
        courses = Course.objects.filter(programme_id = student.programme)        
        CHOICES = list()
        
        ## uncomment code for failed course
        # appear = appearance.objects.filter(student = student.id)
        # for seat in appear:
        #     print("seat",seat.id)
        #     attend = attendance.objects.filter(seatno = seat.id )
        #     for attended in attend:
        #         print("attend",attended.id)
        #         courses = result.objects.filter(seatno = attended.id , pass_fail = 0)
        #         print(courses)
        #         for j in courses:
        #             print(j.course_id.name)
        #             CHOICES.append((j.course_id.id, j.course_id.name))

        
        ## comment code for failed course
        for course in courses:
            CHOICES.append((course.id, course.name))

        self.fields['courseid'].choices = CHOICES
         
    def clean(self):
        cleaned_data = super(backlog_fee_form, self).clean()
        receipt_no = cleaned_data.get("receipt_no")
        amount = cleaned_data.get("amount")

        # check if amount is above 0
        if amount < 0:
            self.add_error('amount', "The amount should be above 0")

        # return any errors if found
        return self.cleaned_data



# from .models import Fee_Transaction

class backlogfee_update_Form(ModelForm):
    class Meta:
        model = backlogfee

        fields = ['approved', 'remarks']
        labels = {            
            'approved': 'Approved',
            'remarks': 'Remarks',            
        }

    def __init__(self, *args, **kwargs):
        
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({'class': 'form-control'})
        self.fields['remarks'].required = False        
        self.fields['approved'] = forms.ChoiceField(widget=forms.Select(attrs={'class': "form-control"}),
                                                    choices=[(1, "Yes"), (0, "No")])
    
     
class attendance_backlog_form(ModelForm):
    class Meta:
        model = backlogteacher

        fields = ['attendance']
        labels = {                
            'attendance' : 'attendance'            
        }
        
        
        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            for field in iter(self.fields):
                self.fields[field].widget.attrs.update({ 'class': 'form-control' })            

    