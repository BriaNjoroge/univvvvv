from django.conf.urls import url
from django.urls import path, include 
from . import views

urlpatterns = [
    path('view_request', views.backlog_request, name='view_backlog_request'),
    path('list', views.backlog_approved, name='backlog_approved'),    
    path('list/<int:id>/update', views.backlog_update, name='backlog_approved_id'), 
    path('update_marks/course/<int:course_id>', views.update_backlog_marks, name='backlog_update_marks'),   
    path('update_attendance/<int:id>', views.backlog_attendance_list, name='backlog_update_attendance'), 
    path('<int:id>/update', views.backlog_attendance_update, name='backlog_attendance_update'),
    path('backlog_attendance_home', views.backlog_attendance_home, name='backlog_attendance_home'),   
    path('backlog_marks_home', views.backlog_marks_home, name='backlog_marks_home'),   
] 