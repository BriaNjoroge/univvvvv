
from django.contrib import messages
from django.shortcuts import render, redirect, get_object_or_404
from .forms import backlog_fee_form , backlogfee_update_Form , attendance_backlog_form
from .models import backlogfee ,backlogteacher
from exam_appear.models import appearance , attendance
from results.forms import MarksUpdate
from results.models import result
from course.models import Course
from course.models import Course
# Create your views here.


def backlog_attendance_home(request):
    print("hello2")
    data = {}
    data['Courses'] = Course.objects.all()
    data['activetab'] = 'list'
    return render(request, "backlog_attendance_home.html", data)

   
def backlog_marks_home(request):
    print("hello2")
    data = {}
    data['Courses'] = Course.objects.all()
    data['activetab'] = 'list'
    return render(request, "backlog_mark_home.html", data)

def backlog_request(request):
    data ={}
    form = backlog_fee_form(student_id = request.user.Student.id)
    backlogdata = backlogfee.objects.filter(student_id=request.user.Student.id)
    activetab = 'list'

    if request.method == 'POST':

        if 'submit_request' in request.POST:
            
            activetab = 'add'

            backlog_form = backlog_fee_form(request.POST,student_id = request.user.Student.id)
            
            
            if backlog_form.is_valid() :                

                backlogfee_data = backlogfee()

                backlogfee_data.receipt_no = request.POST.get('receipt_no')
                backlogfee_data.payment_mode = request.POST.get('payment_mode')
                backlogfee_data.transaction_id = request.POST.get('transaction_id')
                backlogfee_data.cheque_no = request.POST.get('cheque_no')
                backlogfee_data.payment_date = request.POST.get('payment_date')
                if 'payment_proof' in request.FILES:
                    backlogfee_data.payment_proof = request.FILES['payment_proof']
                else:
                    backlogfee_data.payment_proof = None
                backlogfee_data.amount = request.POST.get('amount')
                backlogfee_data.student_id = request.user.Student.id
                backlogfee_data.courseid = request.POST.getlist('courseid')
                backlogfee_data.approved = 0
                backlogfee_data.save()
                
                activetab = 'list'

                messages.success(request,"backlog applied successfully")

    else:
        form = backlog_fee_form(student_id = request.user.Student.id)
        
    data["form"] = form
    data["data"] = backlogdata
    data["activetab"] = activetab


    return render(request, 'requestbacklog.html', data)

def backlog_approved(request):
    data = {}

    activetab = 'list'

    backlogfee_Request_data = backlogfee.objects.filter(approved=0)

    
    data['backlogfee_Request_data'] = backlogfee_Request_data
    
    data['activetab'] = activetab

    return render(request, 'backlog_list.html', data)

 
def backlog_update(request, id):
    
    backlogfee_Request_data = get_object_or_404(backlogfee, id=id)
    data = {}
    
    form = backlogfee_update_Form()

    if request.method == 'POST':
        form = backlogfee_update_Form(request.POST)
        if form.is_valid():
            #  Fee_Request.objects.get(id = request.POST.get('receipt_no'))

            approved = request.POST.get('approved', None)
            
            if int(approved) == 1:
                
                backlogfee_Request_data.approved = 1
                seat =  get_object_or_404(appearance, student_id=backlogfee_Request_data.student.id)
                
                for course in eval(backlogfee_Request_data.courseid):
                    backlogteacherdata = backlogteacher()
                    backlogteacherdata.seatno_id = seat.id
                    backlogteacherdata.courseid_id = int(course)
                    backlogteacherdata.status = 0
                    backlogteacherdata.save()
                
            elif int(approved) == 0:
                backlogfee_Request_data.approved = 2

            backlogfee_Request_data.remarks = request.POST.get('remarks')    
            
            backlogfee_Request_data.save()
            messages.success(request,"Fee status updated successfully")
            return redirect('/backlog/list')
        # else:
        #     return redirect('/feetransaction/list')

    else:
        form = backlogfee_update_Form()

    data['form'] = form

    return render(request, 'update_backlog.html', data)


def backlog_attendance_update(request,id):
    attendance_data = get_object_or_404(backlogteacher, id=id)
    if request.method == 'POST':

        form = attendance_backlog_form(request.POST)

        if form.is_valid():

            # process form data
            obj = backlogteacher.objects.get(id=id)  # gets new object
            obj.attendance = form.cleaned_data['attendance']
            obj.save()
            messages.success(request,"Marks updated successfully")
            return redirect('backlog_update_attendance')

    return redirect('backlog_update_attendance')

def backlog_attendance_list(request,id):
    data = {}

    activetab = 'list'
    
    attendance_data = backlogteacher.objects.filter(courseid_id = id)
    form = attendance_backlog_form()
    
    data['attendance_data'] = attendance_data
    data['form'] = attendance_backlog_form()
    data['activetab'] = activetab
    

    return render(request, 'backlog_attendance_list.html', data)

def update_backlog_marks(request,course_id):
    data = {}
    cur_cor = Course.objects.get(id = course_id)
    result_data = backlogteacher.objects.filter(courseid_id=course_id)

    marks_form = MarksUpdate(max_marks=cur_cor.max_marks)

    if request.method == 'POST':
        print(request.POST)
         
        marks_form = MarksUpdate(request.POST, max_marks=cur_cor.max_marks)
        
        if marks_form.is_valid():
            obj = backlogteacher.objects.get(courseid_id = course_id , seatno__seatno = request.POST.get("seat_no"))
            obj.marks = request.POST.get("marks")
            if int(request.POST.get("marks")) < int(cur_cor.pass_marks) :
                    obj.pass_fail = 0
            else :
                    obj.pass_fail = 1
            obj.save()
    else:
        marks_form = MarksUpdate(max_marks=cur_cor.max_marks)
    

    data['course'] = cur_cor
    data["result"] = result_data
    data["marks_form"] = marks_form
    return render(request, "update_backlog_marks.html", data)