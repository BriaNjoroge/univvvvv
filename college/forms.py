from django import forms
from college.models import College
 
class AddCollegeForm(forms.Form):
    name = forms.CharField(label='Name of College *',   max_length=100, widget=forms.TextInput(attrs={'placeholder':'College Name','class': "form-control"}))
    
    address = forms.CharField(label='Address *',max_length=50,required=True, widget=forms.TextInput(attrs={'placeholder':'College Address','class': "form-control"}))
    
    contact = forms.CharField(label='Contact *',max_length=20,required=True, widget=forms.NumberInput(attrs={"min":0,'placeholder':'College Contact','class': "form-control"}))
    
    email = forms.CharField(label='Email *',max_length=30, required=True,widget=forms.EmailInput(attrs={'placeholder':'College Email','class': "form-control"}))
    
    website = forms.CharField(label='Website *',max_length=50,required=True, widget=forms.URLInput(attrs={'placeholder':'College Website','class': "form-control"}))
    
    logo = forms.ImageField(label='Logo',required=False, widget=forms.FileInput(attrs={'placeholder':'College Logo','class': "form-control",'accept':'image/*'}))

    def clean(self):
        cleaned_data = super(AddCollegeForm, self).clean()
        contact = cleaned_data.get('contact')
        if College.objects.filter(name=cleaned_data.get('name')).exists():
            self.add_error('name', "This College name already exists.")

        if int(contact) > 9999999999:
            self.add_error('mobile', "Please enter a valid mobile number")
