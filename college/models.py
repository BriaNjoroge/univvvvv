from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
from login.models import University


# Create your models here.
class College(models.Model):
    user = models.OneToOneField(User, related_name="college", on_delete=models.CASCADE)
    name = models.CharField(max_length=100, unique=True)
    logo = models.ImageField(default='college_logo/logo.jpg')
    address = models.CharField(max_length=100, blank=True)
    contact = models.CharField(max_length=20, blank=True)
    email = models.CharField(max_length=30, blank=True)
    website = models.CharField(max_length=50, blank=True)
    is_active = models.BooleanField(default=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)
    university = models.ForeignKey(University, on_delete=models.CASCADE)

    def __str__(self):
        return self.name
