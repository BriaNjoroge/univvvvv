from django.conf.urls import url
from django.urls import path
from . import views

urlpatterns = [
    url('list', views.college_list, name='college_list'),    
    path('programme/<int:id>', views.college_programme_list, name='college_programme_list'),
    path('active_col_pro/<int:id>', views.active_col_pro, name='active_col_pro'),
    path('disable_col_pro/<int:id>', views.disable_col_pro, name='disable_col_pro'),
    path('active_col/<int:id>', views.active_col, name='active_col'), 
    path('disable_col/<int:id>', views.disable_col, name='disable_col'),

]