from django.contrib import messages
from django.shortcuts import render, redirect
from .models import College
from .forms import AddCollegeForm
from login.models import University
from django.db.models import Count
from django.contrib.auth.models import User
from rolepermissions.roles import assign_role
from django.contrib.auth.hashers import make_password
from college.models import College
from programme.models import Programme


# Create your views here.

def college_programme_list(request,id):
    data = {}
    data['formdata'] = Programme.objects.filter(college = id)
    return render(request,'college_programme_list.html',data)

def college_list(request):
    
    data = {}

    data['show'] = 'list'
    collegedata_list = College.objects.all()

    glist = list()
    for i in collegedata_list:
        ldict = {}    
        ldict['data'] = i
        # ldict['progrmme_count'] = College.objects.filter(id = i.id).count()
        ldict['progrmme_count'] =  Programme.objects.filter(college_id=i.id).count()

        glist.append(ldict)
    
    collegedata_list = glist 
    

    if request.method == 'POST':

        form = AddCollegeForm(request.POST)

        if form.is_valid():
            form = AddCollegeForm()

            userdata = User()
            userdata.username = request.POST.get('name').strip().replace(' ','_')
            userdata.password = make_password('admin@123')
            userdata.save()

            collegedata = College()
            collegedata.university_id_id = request.POST.get('university_id')
            collegedata.name = request.POST.get('name')
            collegedata.address = request.POST.get('address')
            collegedata.contact = request.POST.get('contact')
            collegedata.email = request.POST.get('email')
            collegedata.website = request.POST.get('website')
            collegedata.logo = request.FILES.get('logo','college_logo/logo.jpg')

            collegedata.university_id = request.user.university.id
            collegedata.user_id = userdata.id
            
            collegedata.save()

            assign_role(userdata, 'college')
            messages.success(request, "college added succesfully")

            return redirect('college_list')

        else:
            data['show'] = 'add'

    else:
        form = AddCollegeForm()
        

    data['collegedata'] = collegedata_list
    data['form'] = form

    
    return render(request, 'college_list.html', data)

def active_col_pro(request,id):
    data = College.objects.get(id = id)
    data.is_active = False
    data.save()
    return redirect('/college/programme/'+str(data.id))



def disable_col_pro(request,id):
    data = College.objects.get(id = id)
    data.is_active = True
    data.save()


    return redirect('/college/programme/'+str(data.id))


def active_col(request,id):
    data = College.objects.get(id = id)
    data.is_active = False
    data.save()
    messages.info(request,"College has been deactivated")

    return redirect('college_list')


def disable_col(request,id):
    data = College.objects.get(id = id)
    data.is_active = True
    data.save()
    messages.success(request,"College has been activated")

    return redirect('college_list')