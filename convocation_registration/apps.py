from django.apps import AppConfig


class ConvocationRegistrationConfig(AppConfig):
    name = 'convocation_registration'
