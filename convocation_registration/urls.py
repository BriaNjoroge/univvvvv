from django.conf.urls import url
from django.urls import path
from . import views

urlpatterns = [
    path('list', views.convo_register, name='convo_register'),
    path('add', views.convo_request, name='convo_request'),
    path('convo_apr/<int:id>', views.convo_apr, name='convo_apr'),      
    path('approved/<int:id>', views.convo_apr, name='convo_apr'),
    path('rejected/<int:id>', views.convo_rej, name='convo_rej')
]