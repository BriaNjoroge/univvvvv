from django.shortcuts import render , get_object_or_404 ,redirect


from fee_request.models import Fee_Request_convocation , Fee_Request
from fee_request.forms import Fee_Request_convocation_Form
from semester.models import Semester
from student.models import Student
from fee_structure.models import Specialfees

from django.contrib import messages

def convo_request(request):
    data = {}

    form_fee = Fee_Request_convocation_Form()
    student = get_object_or_404(Student, id=request.user.Student.id)
    fee_student = Fee_Request.objects.filter(student_id=request.user.Student.id)
    if request.method == 'POST':

        # if student.current_sem.id < Semester.objects.filter(programme_id=student.programme_id).last().id:
        #     messages.warning(request, 'You are not in your last semester!')
        #
        #     return redirect('convo_request')

        if fee_student.count() > 0:
            if fee_student.last().remaining_amount > 0:
                messages.warning(request, 'You have to finish your tuition fees first!')
                return redirect('convo_request')

        else :
            messages.warning(request, 'You have to apply your tuition fees first!')
            return redirect('convo_request')

        form_fee = Fee_Request_convocation_Form(request.POST)

        if form_fee.is_valid():
            # insert code

            fee_student_convo_count = Fee_Request_convocation.objects.filter(student_id=request.user.Student.id)
            if fee_student_convo_count.count() == 0:
                remain_fee = Specialfees.objects.all().last().graduation
            else:
                remain_fee = fee_student_convo_count.last().remaining_amount

            fee_request_data = Fee_Request_convocation()

            fee_request_data.receipt_no = request.POST.get('receipt_no')
            fee_request_data.payment_mode = request.POST.get('payment_mode')
            fee_request_data.transaction_id = request.POST.get('transaction_id')
            fee_request_data.cheque_no = request.POST.get('cheque_no')
            fee_request_data.payment_date = request.POST.get('payment_date')

            if 'payment_proof' in request.FILES:
                filename = request.FILES['payment_proof']
                if str(filename).lower().split('.')[-1] == 'pdf':

                    fee_request_data.payment_proof = request.FILES['payment_proof']

                else:
                    messages.warning(request, "please upload a pdf")
                    navshow = 'add'
                    return redirect('convo_request')
            else:
                fee_request_data.payment_proof = None


            fee_request_data.amount = request.POST.get('amount')
            fee_request_data.student_id = request.user.Student.id
            fee_request_data.remaining_amount = remain_fee
            if request.POST.get('attend_convo', None) == "on":
                fee_request_data.attend_convo = 1
            else:
                fee_request_data.attend_convo = 0




            fee_request_data.save()

            messages.success(request, f"Graduation request has been added successfully")
            return redirect('convo_request')

        else:
            navshow = 'add'
    else:

        form_fee = Fee_Request_convocation_Form()
    Convo_Request_list = Fee_Request_convocation.objects.filter(student=request.user.Student.id)

    if fee_student.count() > 0:
        eligibility = fee_student.last().remaining_amount
    else:
        eligibility = 1

    data['Convo_Request_list'] = Convo_Request_list

    data['form_fee'] = form_fee
    data['eligibility'] = eligibility


    return render(request, 'student_convo_request_list.html', data)

# Create your views here.
def convo_register(request):
    data = {}

    activetab = 'list'

    convocation_registration_data = Fee_Request_convocation.objects.all()
    
    
    data['convocation_registration_data'] = convocation_registration_data
    data['activetab'] = activetab
    

    return render(request, 'convocation_registration_list.html', data)

    
def convo_apr(request, id):
    data = Fee_Request_convocation.objects.get(id=id)
    print(data)
    data.approved = 1

    data.save()
    messages.success(request,"Graduation request has been approved")

    return redirect('convo_register')

def convo_rej(request, id):
    data = Fee_Request_convocation.objects.get(id=id)
    data.approved = 0

    data.save()
    messages.info(request,"Graduation request has been rejected")
    return redirect('convo_register')
