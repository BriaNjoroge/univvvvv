from django import forms
from django.contrib import auth
from .models import Course


class CourseForm(forms.Form):
    CHOICES = (('Theory', 'Theory'), ('Practical', 'Practical'), ('Project', 'Project'))

    code = forms.CharField(label='Course Code *', max_length=100,
                           widget=forms.TextInput(attrs={'class': "form-control", 'placeholder': 'Course Code'}))

    name = forms.CharField(label='Course Name *', max_length=100,
                           widget=forms.TextInput(attrs={'class': "form-control", 'placeholder': 'Course Name'}))

    course_type = forms.ChoiceField(label='Course Type *', choices=CHOICES,
                                    widget=forms.Select(attrs={'class': "form-control"}))

    course_details = forms.FileField(required=False, label='Course Details', widget=forms.FileInput(
        attrs={'class': "form-control", 'placeholder': 'Course Details', 'accept': 'application/pdf,image/*'}))

    max_marks = forms.CharField(label='Maximum  Marks *', max_length=100, widget=forms.NumberInput(
        attrs={'min': 0, 'max': 200, 'class': "form-control", 'placeholder': 'Max Marks'}))

    pass_marks = forms.CharField(label='Passing Marks *', max_length=100,
                                 widget=forms.NumberInput(attrs={'min': 0, 'max': 100,'class': "form-control", 'placeholder': 'Pass Marks'}))

    def clean(self):
        cleaned_data = super(CourseForm, self).clean()
        if int(cleaned_data.get('max_marks')) < int(cleaned_data.get('pass_marks')):
            self.add_error('max_marks', "Max marks Should be greter than passing marks")
        if Course.objects.filter(code=cleaned_data.get('code')).exists():
            self.add_error('code', "This code already exists.")
