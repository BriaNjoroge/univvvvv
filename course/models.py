from django.db import models
from semester.models import Semester
from django.contrib.auth.models import User
from programme.models import Programme

# Create your models here.
class Course(models.Model):
    name = models.CharField(max_length=100)
    code = models.CharField(max_length=100,unique=True)
    course_type = models.CharField(max_length=100)
    course_details = models.FileField(null=True,upload_to='courses')
    max_marks = models.SmallIntegerField()
    pass_marks = models.SmallIntegerField()
    semester_id = models.ForeignKey(Semester,on_delete=models.CASCADE)
    created_date = models.DateTimeField(auto_now_add = True)
    updated_date = models.DateTimeField(auto_now = True)
    created_by = models.ForeignKey(User,on_delete=models.CASCADE)
    programme = models.ForeignKey(Programme,on_delete=models.CASCADE)


    def __str__(self):
        return self.name

