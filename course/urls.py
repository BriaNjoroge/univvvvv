from django.conf.urls import url
from django.urls import path
from . import views

urlpatterns = [
    path('list/<int:id>/view', views.course_list, name='course_list'),
    path('list/<int:id>/delete', views.course_delete_view, name='course_delete_view'),
]