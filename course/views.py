from django.shortcuts import render, redirect, get_object_or_404
from .models import Course
from .forms import CourseForm
from semester.models import Semester
from django.contrib import messages


# Create your views here.
def course_list(request, id):
    data = {}

    form = CourseForm()
    navshow = 'list'

    if request.method == 'POST':

        form = CourseForm(request.POST)

        if form.is_valid():
            
            coursedata = Course()
            coursedata.name = request.POST.get('name')
            coursedata.code = request.POST.get('code')
            coursedata.course_type = request.POST.get('course_type')
            if 'course_details' in request.FILES:
                coursedata.course_details = request.FILES['course_details']
            else:
                coursedata.course_details = None
            
            coursedata.max_marks = request.POST.get('max_marks')
            coursedata.pass_marks = request.POST.get('pass_marks')            
            coursedata.semester_id_id = id
            programme = Semester.objects.filter(id=id).values('programme_id_id')
            coursedata.programme_id = programme[0]['programme_id_id']
            coursedata.created_by_id = request.user.id
            coursedata.save()
            messages.success(request, "Course has been added successfully!")

            return redirect('course_list',id=id)
        else:
            navshow = 'add'

    else:
        form = CourseForm()

    course_list = Course.objects.filter(semester_id_id=id)

    data['course_list'] = course_list
    data['form'] = form
    data['navshow'] = navshow

    return render(request, 'course_list.html', data)


def course_delete_view(request, id):
    """

    :param id: id of the object
    :return: a list of the fee statements available

    This function deletes a course of a particular programme.
    """
    course = get_object_or_404(Course, id=id)
    idsem = course.semester_id.id
    course.delete()
    messages.info(request, "Course successfully deleted!")
    return  redirect('course_list',id=idsem)
