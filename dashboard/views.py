from django.shortcuts import render

# Create your views here.
def dashboard(request):
    print(request.user.id)
    return render(request, 'dashboard.html')
