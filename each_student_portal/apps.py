from django.apps import AppConfig


class EachStudentPortalConfig(AppConfig):
    name = 'each_student_portal'
