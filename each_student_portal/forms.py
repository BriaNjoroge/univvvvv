import re

from django import forms
from django.contrib.auth.password_validation import validate_password

class ChangePwdForm(forms.Form):
    new_password = forms.CharField( widget=forms.PasswordInput(attrs={'class':'form-control'}), label='New Password', max_length=100, help_text='Password should contains one special character, one uppercase and one lowercase value and minimum length is 8.')
    confirm_new_password = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control'}), label='Confirm New Password', max_length=100)

    def clean(self):
        new_password = self.cleaned_data['new_password']
        confirm_new_password = self.cleaned_data['confirm_new_password']
        if new_password != confirm_new_password:
            raise forms.ValidationError({'new_password': ["password and confirm password does not match",]})
        if len(new_password) < 8:
            raise forms.ValidationError({'new_password': ["Password minimum length was 8",]})

        letters = set(new_password)
        mixed = any(letter.isdigit() for letter in letters) and any(letter.islower() for letter in letters) and any(letter.isupper() for letter in letters)
        if not mixed:
            raise forms.ValidationError({'new_password': ["Invalid password: Mixed case characters not detected",]})


        if not re.findall('[^A-Za-z0-9]', new_password):
            raise forms.ValidationError({'new_password': ["Invalid password: special characters not detected",]})

