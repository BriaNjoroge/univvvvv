from django.urls import path
from . import views

urlpatterns = [
    path('student/<int:id>/profile', views.eachstudentprofile, name='eachstudentprofile'),
    path('student/<int:id>/fee-statement', views.feestatement, name='feestatement'),
    path('student/<int:id>/exam-timetable', views.studentexamtimetable, name='studentexamtimetable'),
    path('student/<int:id>/request-accomodation', views.studentAccommodationRequest, name='studentAccommodationRequest'),
    path('student/reject-accomodation/<int:acc_id>', views.studentAccommodationRejectRequest, name='studentAccommodationRejectRequest'),

    path('get_hostel_ids', views.get_hostel_ids, name='get_hostel_ids'),

]