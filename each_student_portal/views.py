from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User
from django.db.models import Subquery, OuterRef
from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, redirect, reverse

from each_student_portal.forms import ChangePwdForm
from hostel.models import HostelRoom
from student.models import Student
from fee_structure.models import Fee
from django.core import serializers
from exam_timetable.models import exam_timtable
from accomodation.forms import RequestAccomdationForm
from accomodation.models import AccomodationRequest, Accomodation

from django.contrib import messages, auth

from fee_request.models import Fee_Request_accomodation, Fee_Request
from fee_request.forms import Fee_Request_accomodation_Form

# import the logging library
import logging

# Get an instance of a logger
logger = logging.getLogger('each_student_portal')


def eachstudentprofile(request, id):
    """
    :param user request id:
    :return: Student information

    """
    student = get_object_or_404(Student, id=id)
    logger.warning(f"Studentaaa : {student}")

    latest_entry = Subquery(
        Fee_Request.objects.filter(student_id=id).order_by('-approved_date').values('id')[:1])
    remain_amount = Fee_Request.objects.filter(id__in=latest_entry).exclude(remaining_amount=0).values(
        'student__admission', 'student__firstName',
        'student__lastName',
        'student__current_sem__name',
        'student__college', 'remaining_amount',
        'student__programme__name')

    hostel = Accomodation.objects.filter(student_id=id).values('room_id__room_name', 'room_id__hostel_id__name')

    context = {
        'student': student,
        'balance': remain_amount,
        'hostel': hostel
    }

    return render(request, 'eachstudentprofile.html', context)


def feestatement(request, id):
    """
    :param request:
    :return: the fee statement of the student
    """
    student = get_object_or_404(Student, id=id)
    student_programme = student.programme
    # student_batch = student.batch
    student_category = student.category
    if student.nationality == "Indian":
        student_country = "Local"

    else:
        student_country = "Foreigner"
    # query = Fee.objects.filter(programme__name__icontains=student_programme).filter(batch=student_batch).filter(
    #     category__icontains=student_category).filter(country__icontains=student_country).order_by('semister')

    query = Fee.objects.filter(programme__name=student_programme).filter(
        category=student_category).filter(country__icontains=student_country).order_by('semister')

    context = {
        'student': student,
        'fees': query,
    }
    return render(request, 'fee-statement.html', context)


def studentexamtimetable(request, id):
    """

    :param request:
    :return: return the timetable of the students course
    """

    student = get_object_or_404(Student, id=id)
    student_programme = student.programme
    exams = exam_timtable.objects.filter(programme_id=student_programme).order_by('-id')

    context = {
        'exams': exams
    }
    return render(request, 'examtimetable.html', context)


def studentAccommodationRequest(request, id):
    """

    :param request:
    :param id: student id
    :return:  Student to request for accomodation
    """

    student = get_object_or_404(Student, id=id)
    student_id = student.id
    student_gender = student.gender
    activetab = 'list'

    accRequests = AccomodationRequest.objects.filter(student_id_id=student_id)
    accomodation_form = RequestAccomdationForm(gender=student_gender, student_id=student_id)

    fee_form = Fee_Request_accomodation_Form()

    if request.method == 'POST':
        if 'submit_request' in request.POST:
            logger.info("submit request form")
            activetab = 'add'

            accomodation_form = RequestAccomdationForm(request.POST, gender=student_gender, student_id=student_id)
            print(request.POST)

            if accomodation_form.is_valid():
                hostel_list = request.POST.getlist('hostels')
                print("hostel_list")
                print(hostel_list)

                acc_data = AccomodationRequest()
                acc_data.student_id_id = id
                acc_data.room_pref = request.POST.get('room_pref')
                acc_data.food_pref = request.POST.get('food_pref')
                acc_data.sharing_pref = request.POST.get('sharing_pref')
                acc_data.hostel_ids = request.POST.getlist('hostels')

                acc_data.status = 0

                acc_data.save()
                activetab = 'list'

                messages.success(request, "Accomodation applied successfully")

        elif 'pay_fees' in request.POST:
            fee_form = Fee_Request_accomodation_Form(request.POST)

            if fee_form.is_valid():
                fee_request_data = Fee_Request_accomodation()

                fee_request_data.receipt_no = request.POST.get('receipt_no')
                fee_request_data.payment_mode = request.POST.get('payment_mode')
                fee_request_data.transaction_id = request.POST.get('transaction_id')
                fee_request_data.cheque_no = request.POST.get('cheque_no')
                fee_request_data.payment_date = request.POST.get('payment_date')
                if 'payment_proof' in request.FILES:
                    fee_request_data.payment_proof = request.FILES['payment_proof']
                else:
                    fee_request_data.payment_proof = None
                fee_request_data.amount = request.POST.get('amount')
                fee_request_data.student_id = request.user.Student.id
                fee_request_data.remaining_amount = 0

                fee_request_data.save()

                acc_data = AccomodationRequest.objects.get(id=request.POST.get('acc_id'))
                acc_data.student_id_id = id
                acc_data.fee_id = fee_request_data.id
                acc_data.status = 3

                acc_data.save()
                activetab = 'list'

                messages.success(request, "Accomodation Fees Request Submiited successfully")

    else:
        accomodation_form = RequestAccomdationForm()
        logger.debug("not post form")
    context = {
        "form": accomodation_form,
        "accRequests": accRequests,
        "fee_form": fee_form,
        "activetab": activetab
    }

    return render(request, 'requestaccomodation.html', context)


def studentAccommodationRequest1(request, id):
    """
    :param request:
    :param id: student id
    :return:  Student to request for accomodation
    """

    student = get_object_or_404(Student, id=id)
    student_id = student.id
    student_gender = student.gender
    activetab = 'list'

    accRequests = AccomodationRequest.objects.filter(student_id_id=student_id)

    accomodation_form = RequestAccomdationForm(gender=student_gender, student_id=student_id)
    fee_form = Fee_Request_accomodation_Form()

    if request.method == 'POST':
        if 'submit_request' in request.POST:
            logger.info("submit request form")
            activetab = 'add'

            accomodation_form = RequestAccomdationForm(request.POST, gender=student_gender, student_id=student_id)
            print(request.POST)
            fee_form = Fee_Request_accomodation_Form(request.POST)
            if accomodation_form.is_valid() and fee_form.is_valid():
                hostel_list = request.POST.getlist('hostels')
                print("hostel_list")
                print(hostel_list)

                fee_request_data = Fee_Request_accomodation()

                fee_request_data.receipt_no = request.POST.get('receipt_no')
                fee_request_data.payment_mode = request.POST.get('payment_mode')
                fee_request_data.transaction_id = request.POST.get('transaction_id')
                fee_request_data.cheque_no = request.POST.get('cheque_no')
                fee_request_data.payment_date = request.POST.get('payment_date')
                if 'payment_proof' in request.FILES:
                    fee_request_data.payment_proof = request.FILES['payment_proof']
                else:
                    fee_request_data.payment_proof = None
                fee_request_data.amount = request.POST.get('amount')
                fee_request_data.student_id = request.user.Student.id
                fee_request_data.remaining_amount = 0

                acc_data = AccomodationRequest()
                acc_data.student_id_id = id
                acc_data.room_pref = request.POST.get('room_pref')
                acc_data.food_pref = request.POST.get('food_pref')
                acc_data.sharing_pref = request.POST.get('sharing_pref')
                acc_data.hostel_ids = request.POST.getlist('hostels')
                # ridge = request.POST.get('rigid', None)
                # if ridge:
                #     acc_data.rigid = 1
                # else:
                #     acc_data.rigid = 0

                fee_request_data.save()
                acc_data.fee_id = fee_request_data.id
                acc_data.status = 0

                acc_data.save()
                activetab = 'list'

                messages.success(request, "Accomodation applied successfully")
    else:
        accomodation_form = RequestAccomdationForm()
        logger.debug("not post form")
        # hostels = request.POST.get("hostels")
        # rooms = request.POST.get("rooms")
        # hostel_room = HostelRoom.objects.get(id = rooms, hostel_id_id = hostels)
        # hostel_room.current_capacity += 1
        # hostel_room.save()
        fee_form = Fee_Request_accomodation_Form()
    context = {
        "form": accomodation_form,
        "accRequests": accRequests,
        "fee_form": fee_form,
        "activetab": activetab
    }

    return render(request, 'requestaccomodation.html', context)


def get_hostel_ids(request):
    student = get_object_or_404(Student, id=request.user.Student.id)
    student_id = student.id
    student_gender = student.gender

    rooms_list = list(set(
        HostelRoom.objects.filter(room_type=request.GET['room_type'], capacity=request.GET['sharing_pref'],
                                  is_active=True, hostel_id__is_active=1,
                                  hostel_id__hostel_type=student_gender).values_list('hostel_id__name')))
    return JsonResponse(rooms_list, safe=False)


def studentAccommodationRejectRequest(request, acc_id):
    id = request.user.Student.id

    acc_data = AccomodationRequest.objects.get(id=acc_id)
    acc_data.status = 4

    room_data = HostelRoom.objects.get(id=acc_data.temp_room_id.id)
    room_data.current_capacity -= 1
    room_data.remaining_capacity += 1
    room_data.save()

    acc_data.save()

    return HttpResponseRedirect(reverse("studentAccommodationRequest", args=[id]))


