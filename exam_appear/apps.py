from django.apps import AppConfig


class ExamAppearConfig(AppConfig):
    name = 'exam_appear'
