from django import forms
from django.forms import ModelForm
from .models import attendance


class attendance_uni_form(ModelForm):
    class Meta:
        model = attendance

        fields = ['appear']
        labels = {
            'appear': 'appear'
        }

        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            for field in iter(self.fields):
                self.fields[field].widget.attrs.update({'class': 'form-control'})
