from django.db import models
from django.contrib.auth.models import User
from course.models import Course
from student.models import Student
 
# Create your models here.
class appearance(models.Model):
    seatno = models.CharField(max_length=15)
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    
class attendance(models.Model):
    seatno = models.ForeignKey(appearance, on_delete=models.CASCADE)
    courseid = models.ForeignKey(Course, on_delete=models.CASCADE)
    appear = models.BooleanField(null=True)
