from django.conf.urls import url
from django.urls import path

from . import views

urlpatterns = [
    path('list', views.show_attendance, name='show_attendance'), 
    path('<int:id>/update', views.attendance_update, name='exam_attendance_update'),   
     
]