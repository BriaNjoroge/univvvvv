from django.shortcuts import render, redirect
from .models import attendance
from .forms import attendance_uni_form
from django.shortcuts import  get_object_or_404
from .models import appearance
from exam_registration.models import exam_registration
from exam_timetable.models import exam_timtable
from student.models import Student
from course.models import Course
from django.utils import timezone
from semester.models import Semester
from datetime import date

import random

def register_seatno():
    all_exams = exam_timtable.objects.filter(student_registration = 0)
    for i in all_exams:
        print("i.registration_end_date   ",i.registration_end_date, date.today())
        if i.registration_end_date < date.today():
            get_all_stu = exam_registration.objects.filter(student__programme_id = i.programme_id_id,approved = True)        

            for exam_reg_stu in get_all_stu:                

                stu_appearnce = appearance()
                seatno =  str(exam_reg_stu.student.programme.id) + str(exam_reg_stu.student.batch)+ str(exam_reg_stu.student.current_sem) + str(exam_reg_stu.student.admission)
                stu_appearnce.seatno = seatno
                stu_appearnce.student_id = exam_reg_stu.student_id
                stu_appearnce.save()

                sem = Semester.objects.filter(programme_id_id=exam_reg_stu.student.programme.id)
                semester = 1
                for s in sem:
                    if str(exam_reg_stu.student.current_sem) in str(s.name):
                        semester = s.id
                        break
        
                course = Course.objects.filter(programme_id= exam_reg_stu.student.programme.id ,semester_id_id = semester).values('id')
                for j in course:
                    stu_attendance = attendance()
                    stu_attendance.seatno_id = stu_appearnce.id
                    stu_attendance.courseid_id = j['id']
                    stu_attendance.save()
            exam_timtable.objects.filter(id = i.id).update(student_registration = 1)
        
        else:
            pass   


# Create your views here.
# def attendance_register():
#     print("came")
#     all_appearance = appearance.objects.all()

#     for i in all_appearance:                
#         stu_data = list(Student.objects.filter(id = i.student_id).values('programme_id','current_sem'))
#         print(stu_data)
#         sem = Semester.objects.filter(programme_id_id=stu_data[0]['programme_id'])
#         semester = 1
#         for s in sem:
#             if str(stu_data[0]['current_sem']) in str(s.name):
#                 semester = s.id
#                 break
 
#         course = Course.objects.filter(programme_id= stu_data[0]['programme_id'],semester_id_id = semester).values('id')
#         print(course)
#         for j in course:
#             print(j)
#             stu_attendance = attendance()
#             stu_attendance.seatno_id = i.id
#             stu_attendance.courseid_id = j['id']
#             stu_attendance.save()

def show_attendance(request):

    register_seatno()
    # attendance_register()


    data = {}

    activetab = 'list'
    
    attendance_data = attendance.objects.all()
    form = attendance_uni_form()
    
    data['attendance_data'] = attendance_data
    data['form'] = attendance_uni_form()
    data['activetab'] = activetab
    

    return render(request, 'attendance_list.html', data)



def attendance_update(request,id):
    attendance_data = get_object_or_404(attendance, id=id)
    if request.method == 'POST':

        form = attendance_uni_form(request.POST)

        if form.is_valid():

            # process form data
            obj = attendance.objects.get(id=id)  # gets new object
            obj.appear = form.cleaned_data['appear']
            obj.save()

            return redirect('show_attendance')

    return redirect('show_attendance')

