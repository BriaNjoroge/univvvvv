from django.apps import AppConfig


class ExamRegistrationConfig(AppConfig):
    name = 'exam_registration'
