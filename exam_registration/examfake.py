
from exam_registration.models import exam_registration
from student.models import Student
import datetime

from faker import Faker
fake = Faker()
 

def examfaker():
    student = Student.objects.all()
    for i in student:
        fee_request_data = exam_registration()
        fee_request_data.receipt_no = i.firstName
        fee_request_data.payment_mode = "cash"
        fee_request_data.transaction_id = "ADfsdf"
        fee_request_data.cheque_no = "dasd"
        fee_request_data.payment_date = (datetime.datetime.now() - datetime.timedelta(days=3)).date()            
        fee_request_data.payment_proof  = None
        fee_request_data.examcategory = "Regular"
        fee_request_data.student_id = i.id 
        fee_request_data.remaining_amount = 0
        fee_request_data.approved = 1
        fee_request_data.amount = 1000
        fee_request_data.save()