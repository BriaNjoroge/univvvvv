from django import forms
from django.forms import ModelForm
from .models import exam_registration
import datetime


class exam_registration_form(forms.Form):
    CHOICES = (('Cash', 'Cash'), ('Cheque', 'Cheque'), ('Online Payment', 'Online Payment'))
    EXAM_CHOICES = (('Regular', 'Regular'),('Backlog', 'Backlog'))
    receipt_no = forms.CharField(required=True, label='Receipt Number *', max_length=100, widget=forms.TextInput(
        attrs={'class': "form-control", 'placeholder': 'Receipt Number'}))
    payment_mode = forms.ChoiceField(choices=CHOICES, label='Payment Mode *',
                                     widget=forms.Select(attrs={'class': "form-control"}))
    transaction_id = forms.CharField(required=False, label='Transaction ID *', widget=forms.TextInput(
        attrs={'class': "form-control", 'placeholder': 'Transaction ID'}))
    cheque_no = forms.CharField(required=False, label='Cheque Number *',
                                widget=forms.TextInput(attrs={'class': "form-control", 'placeholder': 'Cheque Number'}))
    payment_date = forms.DateField(label='Payment Date *',
                                   widget=forms.widgets.DateTimeInput(attrs={"type": "date", 'class': "form-control"}))
    payment_proof = forms.FileField(required=False, label='Payment Proof', widget=forms.FileInput(
        attrs={'class': "form-control", 'placeholder': 'Payment Proof', 'accept': 'application/pdf,image/*'}))

    amount = forms.DecimalField(label='Amount *', widget=forms.NumberInput(attrs={'min':0,'class': "form-control"}))

    examcategory = forms.ChoiceField(choices=EXAM_CHOICES, label='Exam Category',
                                     widget=forms.Select(attrs={'class': "form-control"}))
    def clean(self):
        cleaned_data = super(exam_registration_form, self).clean()
        receipt_no = cleaned_data.get("receipt_no")
        amount = cleaned_data.get("amount")
        payment_date = cleaned_data.get("payment_date")

        # check if amount is above 0
        if amount < 0:
            self.add_error('amount', "The amount should be above 0")

        if payment_date > datetime.date.today():
            self.add_error('payment_date', "Can't select future date")



        # return any errors if found
        return self.cleaned_data


    # def clean(self):
    #     cleaned_data = super(exam_registration_form, self).clean()
    #     receipt_no = cleaned_data.get("receipt_no")
    #     amount = cleaned_data.get("amount")
    #     approved = cleaned_data.get("approved")
    #     if approved and not amount:
    #        self.add_error('amount', "This field is required.")
    
     
class exam_registration_uni_form(ModelForm):
    class Meta:
        model = exam_registration

        fields = ['approved','remarks']
        labels = {        
            'approved': 'Approved *',
            'remarks' : 'Remarks'            
        }
        
    def __init__(self, *args, **kwargs):
        
        super(exam_registration_uni_form, self).__init__(*args, **kwargs)

        self.fields['remarks'].required = False

        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({ 'class': 'form-control' })            
        # self.fields['pkid'].disable = False

        self.fields['approved'] = forms.ChoiceField(widget=forms.Select(attrs={'class': "form-control"}),
                                                choices=[(1, "Yes"), (0, "No")])


    def clean(self):
        cleaned_data = super(exam_registration_uni_form, self).clean()
        approved = cleaned_data.get("approved")







        # if int(approved) == 0 :
        #     print("error----------> ", approved, int(approved))
        #     self.add_error('remarks', "This field is required.")
        #

