from django.db import models
from student.models import Student
from django.core.validators import MinValueValidator
from decimal import Decimal
# Create your models here.
 

# Create your models  here.
class exam_registration(models.Model):
    pkid = models.AutoField(primary_key=True)
    examcategory = models.CharField(max_length=10)
    receipt_no = models.CharField(max_length=20)
    payment_mode = models.CharField(max_length=30)
    transaction_id = models.CharField(max_length=30,blank=True)
    cheque_no = models.CharField(max_length=30, default=None)
    payment_date = models.DateTimeField()
    payment_proof = models.FileField(upload_to='fees_proof',null=True)
    amount =  models.DecimalField(decimal_places=2, default=0,max_digits=12, validators=[MinValueValidator(Decimal('0.00'))],blank=True, verbose_name='Other Charges')
    remaining_amount = models.FloatField(null=True)    
    approved = models.BooleanField(null=True)
    remarks = models.CharField(max_length=50, default=None, null=True,blank=True)
    student = models.ForeignKey(Student, on_delete=models.CASCADE)


     