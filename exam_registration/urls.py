from django.conf.urls import url
from django.urls import path
from . import views

urlpatterns = [
    
    path('student/list', views.stu_exam_register, name='stu_exam_register'),   
    path('list', views.exam_register, name='exam_register'),
    path('student/card', views.exam_card, name='exam_card'),   
    path('<int:id>/update', views.exam_update, name='exam_register_update'), 
]