from django.shortcuts import render, redirect
from .models import exam_registration
from exam_appear.models import appearance ,attendance
from .forms import exam_registration_uni_form , exam_registration_form
from django.shortcuts import  get_object_or_404
from django.contrib import messages

def stu_exam_register(request):
    data = {}

    form = exam_registration_form()

    navshow = 'list'
     
    if request.method == 'POST':
        
        form = exam_registration_form(request.POST)

        if form.is_valid():
                        
            fee_request_data = exam_registration()
            fee_request_data.receipt_no = request.POST.get('receipt_no')
            fee_request_data.payment_mode = request.POST.get('payment_mode')            
            fee_request_data.transaction_id = request.POST.get('transaction_id')
            fee_request_data.cheque_no = request.POST.get('cheque_no')
            fee_request_data.payment_date = request.POST.get('payment_date')
            if 'payment_proof' in request.FILES:

                filename = request.FILES['payment_proof']
                if str(filename).lower().split('.')[-1] == 'pdf':
                    if int(len(request.FILES['payment_proof'].read())) < (3 * 1024 * 1024):
                        fee_request_data.payment_proof = request.FILES['payment_proof']
                    else:
                        messages.warning(request, "please upload a pdf within 3 mb")
                        navshow = 'add'
                        return redirect('stu_exam_register')
 
                else:
                    messages.warning(request, "please upload a pdf")
                    navshow = 'add'
                    return redirect('stu_exam_register')

            else:
                fee_request_data.payment_proof  = None

            fee_request_data.examcategory = request.POST.get('examcategory')
            

            fee_request_data.student_id = request.user.Student.id
            fee_request_data.remaining_amount = 0
            fee_request_data.amount = request.POST.get('amount')
            if exam_registration.objects.filter(student_id=request.user.Student.id).exclude(approved=1).exists():
                messages.warning(request, "Your previous request was accepted")

                return redirect('stu_exam_register')
            
            fee_request_data.save()
            messages.success(request,"Fee request applied successfully")
            
            return redirect('stu_exam_register')
        else:
            navshow = 'add'

    else:
        form = exam_registration_form()

    Fee_Request_list = exam_registration.objects.filter(student_id = request.user.Student.id).order_by('-pkid')
    data['Fee_Request_list'] = Fee_Request_list
    data['form'] = form

    
    data['navshow'] = navshow

    return render(request, 'exam_Request_list.html',data)

 # 

# Create your views here.
def exam_register(request):

    data = {}

    activetab = 'list'
  
    exam_registration_data = exam_registration.objects.all()
    form = exam_registration_uni_form()

    data['exam_registration_data'] = exam_registration_data
    data['form'] = form
    data['activetab'] = activetab
    
    return render(request, 'exam_registration_list.html', data)

def exam_card(request):
    data = {}

    activetab = 'list'
  
    exam_registration_data = exam_registration.objects.filter(student_id = request.user.Student.id ,approved = 1).count()
    print(request.user.Student.id)
    if exam_registration_data > 0:
        seat = appearance.objects.filter(student_id = request.user.Student.id)
        
        if seat.count() < 1:
            messages.warning(request, "Seat number is not assigned yet")
        else:
            
            course_data = attendance.objects.filter(seatno_id = seat[0].id)
            
            data['data'] = course_data
    else:
        messages.warning(request, "Either you didn't apply for exam or your application isn't approve")
    print(data)
    data['activetab'] = activetab
    
    return render(request, 'exam_card_list.html', data)

def exam_update(request,id):
    exam_registration_data = get_object_or_404(exam_registration, pkid=id)

    if request.method == 'POST':

        form = exam_registration_uni_form(request.POST)

        if form.is_valid():

            # process form data
            obj = exam_registration.objects.get(pkid=id)  # gets new object
            obj.approved = form.cleaned_data['approved']
            obj.remarks = form.cleaned_data['remarks']
            obj.save()

            return redirect('exam_register')

    return redirect('exam_register')

