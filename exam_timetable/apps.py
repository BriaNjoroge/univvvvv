from django.apps import AppConfig


class ExamTimetableConfig(AppConfig):
    name = 'exam_timetable'
