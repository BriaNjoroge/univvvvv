from django import forms
from django.contrib import auth
from .models import exam_timtable
from programme.models import Programme


class exam_timtable_form(forms.Form):
    registration_start_date = forms.DateField(label='Registration start date *', widget=forms.widgets.DateTimeInput(
        attrs={"type": "date", 'class': "form-control"}))
    registration_end_date = forms.DateField(label='Registration end date *', widget=forms.widgets.DateTimeInput(
        attrs={"type": "date", 'class': "form-control"}))
    pdf = forms.FileField(required=False, label='Timetable  Document', widget=forms.FileInput(
        attrs={'class': "form-control", 'placeholder': 'Timetable Document', 'accept': 'application/pdf,image/*'}))
    programme_id = forms.ModelChoiceField(Programme.objects.all(), label='Programme *', widget=forms.Select(
        attrs={'class': "form-control", 'placeholder': 'Programme ID'}))

    def clean(self):
        cleaned_data = super(exam_timtable_form, self).clean()
        registration_start_date = cleaned_data.get("registration_start_date")
        registration_end_date = cleaned_data.get("registration_end_date")
        programme_id = cleaned_data.get("programme_id")

        if registration_end_date < registration_start_date:
            self.add_error('registration_end_date', "Registration end date must be greater than start date")
        if registration_end_date == registration_start_date:
            self.add_error('registration_end_date', "Registration end date should not be the same date start date")

        if exam_timtable.objects.filter(programme_id=programme_id).exists():
            self.add_error('programme_id', "This timetable for this programme already exists.")
class update_timetable_form(forms.Form):
    registration_start_date = forms.DateField(label='Registration start date *', widget=forms.widgets.DateTimeInput(
        attrs={"type": "date", 'class': "form-control"}))
    registration_end_date = forms.DateField(label='Registration end date *', widget=forms.widgets.DateTimeInput(
        attrs={"type": "date", 'class': "form-control"}))
    # pdf = forms.FileField(required=False, label='Timetable  Document', widget=forms.FileInput(
    #     attrs={'class': "form-control", 'placeholder': 'Timetable Document', 'accept': 'application/pdf,image/*'}))
    # programme_id = forms.ModelChoiceField(Programme.objects.all(), label='Programme ID *', widget=forms.Select(
    #     attrs={'class': "form-control", 'placeholder': 'Programme ID'}))

    def clean(self):
        cleaned_data = super(update_timetable_form, self).clean()
        registration_start_date = cleaned_data.get("registration_start_date")
        registration_end_date = cleaned_data.get("registration_end_date")
        # programme_id = cleaned_data.get("programme_id")

        if registration_end_date < registration_start_date:
            self.add_error('registration_end_date', "Registration end date must be greater than start date")
        if registration_end_date == registration_start_date:
            self.add_error('registration_end_date', "Registration end date should not be the same date start date")
        #
        # if exam_timtable.objects.filter(programme_id=programme_id).exists():
        #     self.add_error('programme_id', "This timetable for this programme already exists.")