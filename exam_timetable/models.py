from django.db import models
from django.contrib.auth.models import User
from programme.models import Programme
# Create your models here.
class exam_timtable(models.Model):    
    registration_start_date = models.DateField()
    registration_end_date = models.DateField()
    pdf = models.FileField(upload_to='exam_timetable')
    programme_id = models.ForeignKey(Programme, on_delete=models.CASCADE)
    student_registration = models.BooleanField(default=0)
