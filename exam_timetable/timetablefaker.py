from exam_timetable.models import exam_timtable
from programme.models import Programme
import datetime

from faker import Faker
fake = Faker()


def timetablefaker():
    programme = Programme.objects.all()
    for i in programme:
        obj = exam_timtable()
        obj.registration_start_date = (datetime.datetime.now() - datetime.timedelta(days=7)).date()        
        obj.registration_end_date =(datetime.datetime.now() - datetime.timedelta(days=3)).date()        
        obj.pdf = "exam_timtable/pre.png"
        obj.programme_id_id = i.id
        obj.student_registration = 0
        obj.save()
