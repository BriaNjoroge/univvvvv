from django.conf.urls import url
from django.urls import path
from . import views

urlpatterns = [
    path('list', views.exam_timtable_view, name='exam_timetable_list'),
    path('list/<int:id>/update', views.updateexamtimetable, name='updatetimetable'),
    path('list/<int:id>/delete', views.deleteexamtimetable, name='deleteexamtimetable'),
    path('clear_all_timetables', views.clear_all_timetables, name='clear_all_timetables'),

]