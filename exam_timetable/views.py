from django.contrib import messages
from django.shortcuts import render, redirect, HttpResponseRedirect, get_object_or_404
from .models import exam_timtable
from .forms import exam_timtable_form,update_timetable_form
from exam_registration.models import exam_registration
from django.utils import timezone


# Create your views here.
def exam_timtable_view(request):
    data = {}
    form = exam_timtable_form()
    navshow = 'list'
    if request.method == 'POST':
        form = exam_timtable_form(request.POST)

        if form.is_valid():

            exam_timtable_data = exam_timtable()
            exam_timtable_data.registration_start_date = request.POST.get('registration_start_date')
            exam_timtable_data.registration_end_date = request.POST.get('registration_end_date')
            exam_timtable_data.programme_id_id = request.POST.get('programme_id')

            if 'pdf' in request.FILES:
                exam_timtable_data.pdf = request.FILES['pdf']

            else:
                exam_timtable_data.pdf = None

            exam_timtable_data.save()
            messages.success(request, "Exam timetable added successfully")

            return redirect('exam_timetable_list')
        else:
            navshow = 'add'

    else:
        form = exam_timtable_form()

    exam_timtable_list = exam_timtable.objects.all()

    data['exam_timtable_list'] = exam_timtable_list
    data['form'] = form
    data['navshow'] = navshow

    return render(request, 'exam_timetable_list.html', data)


def updateexamtimetable(request, id):

    exam_data = get_object_or_404(exam_timtable, id=id)


    form = update_timetable_form(initial={'registration_start_date': exam_data.registration_start_date,
                                       'registration_end_date': exam_data.registration_end_date})

    if request.method == 'POST':

        form = update_timetable_form(request.POST)

        if form.is_valid():

            # process form data
            obj = exam_timtable.objects.filter(id=id)  # gets new object

            # obj.photo = request.FILES['photo']
            registration_start_date = form.cleaned_data['registration_start_date']
            registration_end_date = form.cleaned_data['registration_end_date']

            obj.update(registration_start_date=registration_start_date,registration_end_date=registration_end_date)
            messages.success(request, 'Timetable record updated successfully!')
            return redirect('exam_timetable_list')


    return render(request, 'update_exam.html',{'form': form, 'exam_data': exam_data})

def deleteexamtimetable(request, id):
    """

    :param id: id of the object
    :return: a list of the fee statements available

    This function deletes a Fee Statement of a particular programme.
    """
    timetable = get_object_or_404(exam_timtable, id=id)

    if request.user.is_authenticated:
        timetable.delete()
        messages.success(request, "Exam Timetable successfully deleted!")
        return redirect("exam_timetable_list")

    return render(request, 'exam_timetable_list.html')


def clear_all_timetables(request):
    """

    :param request:
    :return: Clear all requests, and roo vacancy for students to apply again
    """
    exam_timtable.objects.all().delete()
    messages.info(request,"All timetables have been cleared")
    return redirect('exam_timetable_list')