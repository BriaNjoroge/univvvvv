from django import forms
from college.models import College
from .models import Faculty, Faculty_Course_College
from course.models import Course
# from programme.models import College_programme,Programme
from programme.models import Programme
from semester.models import Semester

SAL = (('Dr.', 'Dr.'),
       ('Prof.', 'Prof.'),
       ('Mr.', 'Mr.',),
       ('Mrs.', 'Mrs.'),
       ('Miss', 'Miss'))


class FacultyRegisterForm(forms.Form):
    salutation = forms.ChoiceField(choices=SAL, widget=forms.Select(attrs={'class': "form-control"}))
    first_name = forms.CharField(label='First Name *', max_length=20,
                                 widget=forms.TextInput(attrs={'placeholder': 'First Name', 'class': "form-control"}))

    middle_name = forms.CharField(required=False, label='Middle Name', max_length=20,
                                  widget=forms.TextInput(attrs={'placeholder': 'Middle Name', 'class': "form-control"}))

    last_name = forms.CharField(label='Last Name *', max_length=20,
                                widget=forms.TextInput(attrs={'placeholder': 'Last Name', 'class': "form-control"}))

    phone = forms.CharField(required=False, label='Contact ', max_length=20,
                            widget=forms.NumberInput(attrs={'min':0,'max':9999999999,'placeholder': 'Contact', 'class': "form-control"}))

    email = forms.CharField(label='Email *',
                            widget=forms.EmailInput(attrs={'placeholder': 'Email', 'class': "form-control"}))

    address = forms.CharField(required=False, label='Address', max_length=100,
                              widget=forms.TextInput(attrs={'placeholder': 'Address', 'class': "form-control"}))

    qualification = forms.CharField(required=False, label='Highest Qualification', max_length=80,
                                    widget=forms.TextInput(
                                        attrs={'placeholder': 'Highest Qualification', 'class': "form-control"}))

    experience = forms.CharField(required=False, label='Years of Experience', widget=forms.NumberInput(
        attrs={'placeholder': 'Years of Experience', 'class': "form-control"}))
    department = forms.CharField(required=False, label='Department', widget=forms.TextInput(
        attrs={'placeholder': 'Department', 'class': "form-control"}))

    prev_institute = forms.CharField(label='Previous Institute', required=False, max_length=100, widget=forms.TextInput(
        attrs={'placeholder': 'Previous Institute', 'class': "form-control"}))

    documents = forms.FileField(label='Resume', required=False,
                                widget=forms.FileInput(attrs={'placeholder': 'Resume', 'class': "form-control",'accept':'application/pdf,image/*'}))

    photo = forms.ImageField(required=False,
                             widget=forms.FileInput(attrs={'placeholder': 'Photo', 'class': "form-control",'accept':'image/*'}))

    college_id = forms.ModelChoiceField(College.objects.all(), label="College *",
                                        widget=forms.Select(attrs={"placeholder": 'College', 'class': 'form-control'}))

    def clean(self):
        cleaned_data = super(FacultyRegisterForm, self).clean()

        phone = cleaned_data.get('phone')


        # if phone > 9999999999:
        #     self.add_error('mobile', "Please enter a valid mobile number")


class FacultyAssignForm(forms.Form):
    # college_id = College.objects.filter(user_id = request.user.id).values_list('id',flat=True)[0]
    # faculties = Faculty.objects.filter(college_id_id = college_id)

    faculty_id = forms.ModelChoiceField(Faculty.objects.none(), label='Faculty',
                                        widget=forms.Select(attrs={'placeholder': 'Faculty', 'class': "form-control"}))
    courses_id = forms.ModelMultipleChoiceField(Course.objects.none(), label='Course', widget=forms.SelectMultiple(
        attrs={'placeholder': 'Course', 'class': "form-control"}))

    def __init__(self, college_id, *args, **kwargs):
        super(FacultyAssignForm, self).__init__(*args, **kwargs)
        self.fields['faculty_id'].queryset = Faculty.objects.filter(college_id_id=college_id)

        programmes_rec = Programme.objects.filter(college_id=college_id).values_list('programme',flat=True)
        sems_rec = Semester.objects.filter(programme_id_id__in=programmes_rec).values_list('id', flat=True)
        courses = Course.objects.filter(semester_id_id__in=sems_rec)

        self.fields['courses_id'].queryset = courses

    def clean(self):
        cleaned_data = super(FacultyAssignForm, self).clean()

        if Faculty_Course_College.objects.filter(faculty_id_id=cleaned_data.get('faculty_id'),
                                                 course_id_id__in=cleaned_data.get('courses_id')).exists():
            self.add_error('faculty_id', "This Assigning already exists.")
