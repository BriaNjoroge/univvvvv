from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
from course.models import Course
from college.models import College
import datetime


class Faculty(models.Model):
    college_id = models.ForeignKey(College, on_delete=models.CASCADE)
    salutation = models.CharField(max_length=20)

    photo = models.ImageField(default='faculty_picture/def.png', upload_to='faculty_picture')
    first_name = models.CharField(max_length=20)
    middle_name = models.CharField(max_length=20, blank=True)
    last_name = models.CharField(max_length=20)
    phone = models.CharField(max_length=20, blank=True)
    department = models.CharField(max_length=100, blank=True)
    email = models.EmailField()
    address = models.CharField(blank=True, max_length=100)
    highest_qualification = models.CharField(max_length=80, blank=True)
    years_of_experience = models.CharField(max_length=20,blank=True)
    previous_institute = models.CharField(blank=True, max_length=100)
    resume_doc = models.FileField(upload_to='faculty_resume', null=True)

    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name='Faculty_reg')
    user = models.OneToOneField(User, related_name="Faculty", on_delete=models.CASCADE)


    def __str__(self):
        return self.first_name + " " + self.last_name


class Faculty_Course_College(models.Model):
    faculty_id = models.ForeignKey(Faculty, on_delete=models.CASCADE)
    course_id = models.ForeignKey(Course, on_delete=models.CASCADE)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name='Faculty_assign')
