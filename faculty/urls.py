from django.conf.urls import url
from django.urls import path, include
from . import views

urlpatterns = [
    path('faculty_registration', views.faculty_registration, name='faculty_registration'),

    path('faculty_assign', views.faculty_assign, name='faculty_assign'),
    path('display_faculty_details/<int:fid>', views.display_faculty_details, name='display_faculty_details'),

    path('faculty/<int:id>/delete', views.faculty_delete_view,
         name='faculty_delete_view'),
    path('faculty/<int:id>/view', views.viewperfaculty, name='viewperfaculty'),


]
