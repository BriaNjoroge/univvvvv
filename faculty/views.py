from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User
from django.shortcuts import render, redirect, get_object_or_404
from rolepermissions.roles import assign_role

from .models import Faculty, Faculty_Course_College
from .forms import FacultyRegisterForm, FacultyAssignForm
import datetime
from college.models import College
from programme.models import Programme
from course.models import Course
from semester.models import Semester
from django.contrib import messages


def faculty_registration(request):
    data = {}

    form = FacultyRegisterForm()
    activetab = 'list'
    if request.method == 'POST':
        # if has_permission(request.user, "university"):
        activetab = 'add'
        form = FacultyRegisterForm(request.POST)
        # print(request.POST)
        if form.is_valid():
            # insert code
            userdata = User()

            if User.objects.filter(username= request.POST.get('email')).exists():
                messages.error(request, f"This email already exists.")
                return redirect('/faculty/faculty_registration')

            userdata.username = request.POST.get('email')
            userdata.email = request.POST.get('email')
            userdata.password = make_password(request.POST.get('email'))

            userdata.save()

            fac = Faculty()
            fac.college_id_id = request.POST.get("college_id")
            fac.salutation = request.POST.get("salutation")
            fac.first_name = request.POST.get("first_name")
            fac.middle_name = request.POST.get("middle_name")
            fac.last_name = request.POST.get("last_name")
            fac.phone = request.POST.get("phone")
            fac.email = request.POST.get("email")
            fac.address = request.POST.get('address')
            fac.department = request.POST.get('department')
            fac.highest_qualification = request.POST.get("qualification")
            fac.years_of_experience = request.POST.get('experience')
            fac.previous_institute = request.POST.get('prev_institute')
            # fac.resume_doc = request.POST.get("documents")

            if 'documents' in request.FILES:

                filename = request.FILES['documents']
                if str(filename).lower().split('.')[-1] == 'pdf':
                    if int(len(request.FILES['documents'].read())) < (3 * 1024 * 1024):
                        fac.resume_doc = request.FILES['documents']
                    else:
                        messages.warning(request, "please upload a pdf within 3 mb")
                        navshow = 'add'
                        return redirect('faculty_registration')

                else:
                    messages.warning(request, "please upload a pdf")
                    navshow = 'add'
                    return redirect('faculty_registration')

            # if request.POST['photo']:
            # print("hello")
            fac.photo = request.FILES.get('photo', 'faculty_picture/def.png')

            fac.created_by_id = request.user.id
            fac.user_id = userdata.id
            fac.save()

            assign_role(userdata, 'teacher')

            messages.success(request, f"Lecturer has been added successfully")
            return redirect('/faculty/faculty_registration')

        else:
            print("\ninvalid form\n")
    else:
        form = FacultyRegisterForm()

    fac_data = Faculty.objects.order_by('-id')
    data['form'] = form
    data['faculties'] = fac_data
    data['activetab'] = activetab

    return render(request, 'faculty_register.html', data)


def faculty_assign(request):
    data = {}

    college_id = College.objects.filter(user_id=request.user.id).values_list('id', flat=True)[0]

    form = FacultyAssignForm(college_id)
    activetab = 'list'

    if request.method == 'POST':
        # print(request.POST)
        activetab = 'assign'
        form = FacultyAssignForm(college_id, request.POST)
        if form.is_valid():

            course_ids = request.POST.getlist('courses_id')
            # print(course_ids)

            for course_id in course_ids:
                # print(course_id)

                fcc = Faculty_Course_College()
                fcc.faculty_id_id = request.POST.get('faculty_id')
                fcc.course_id_id = course_id
                fcc.created_by_id = request.user.id
                fcc.save()
                messages.success("Faculty has been successfully assigned a course")

            form = FacultyAssignForm(college_id)
            return redirect('/faculty/faculty_assign', )
    else:
        form = FacultyAssignForm(college_id)

    fac_data = Faculty.objects.filter(
        college_id_id=College.objects.filter(user_id=request.user.id).values_list('id', flat=True)[0])

    glist = list()
    for i in fac_data:
        ldict = {}
        ldict['data'] = i
        ldict['course_count'] = Faculty_Course_College.objects.filter(faculty_id_id=i.id).count()
        glist.append(ldict)

    fac_data = glist
    data['form'] = form
    data['faculties'] = fac_data
    data['activetab'] = activetab

    return render(request, 'faculty_assign.html', data)


def display_faculty_details(request, fid):
    data = {}

    fac_data = Faculty.objects.filter(id=fid)

    data["faculties"] = fac_data

    fac_courses = Course.objects.filter(
        id__in=Faculty_Course_College.objects.filter(faculty_id_id=fid).values_list('course_id_id', flat=True))

    data["courses"] = fac_courses
    data["activetab"] = "Personal"

    return render(request, 'display_faculty_details.html', data)


def faculty_delete_view(request, id=None):
    """

    :param id: id of the object
    :return: a list of the fee statements available

    This function deletes the selected Faculty.
    """

    fee_statement = get_object_or_404(Faculty, id=id)

    if request.user.is_authenticated:
        fee_statement.delete()
        messages.success(request, "Lecturer successfully deleted!")
        return redirect("faculty_registration")

    return render(request, 'faculty_register.html')


def viewperfaculty(request, id):
    """

    :param request:
    :param id: of the faculty record
    :return: the info of the Lecturer plus the edit records
    """

    lecturer = get_object_or_404(Faculty, id=id)




    lecturer_form = FacultyRegisterForm(initial=lecturer.__dict__)

    if request.method == 'POST':
        lecturer_form = FacultyRegisterForm(request.POST)

    if lecturer_form.is_valid():
    # process form data
        obj = Faculty.objects.get(id=id)  # gets new object
        obj.salutation = lecturer_form.cleaned_data['salutation']
        obj.first_name = lecturer_form.cleaned_data['first_name']
        obj.middle_name = lecturer_form.cleaned_data['middle_name']
        obj.last_name = lecturer_form.cleaned_data['last_name']
        obj.phone = lecturer_form.cleaned_data['phone']
        obj.email = lecturer_form.cleaned_data['email']
        obj.address = lecturer_form.cleaned_data['address']
        obj.department = lecturer_form.cleaned_data['department']
        obj.highest_qualification = lecturer_form.cleaned_data['qualification']
        obj.years_of_experience = lecturer_form.cleaned_data['experience']
        obj.previous_institute = lecturer_form.cleaned_data['prev_institute']
        obj.photo = request.FILES.get('photo', 'faculty_picture/def.png')
        obj.college_id_id = request.POST.get("college_id")

        obj.save()

        messages.success(request, 'Lecturer details updated successfully!')
        return redirect('faculty_registration')
    else:
            lecturer_form = FacultyRegisterForm(initial=lecturer.__dict__)

    context = {
        'lecturer': lecturer,
        'form': lecturer_form
    }

    return render(request, 'perlecturer.html', context)
