from django.apps import AppConfig


class FeeRequestConfig(AppConfig):
    name = 'fee_request'
