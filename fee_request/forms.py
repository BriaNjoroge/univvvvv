from django import forms
from django.contrib import auth

from semester.models import Semester
from .models import Fee_Request
from fee_structure.models import Specialfees
import os
from urllib import request
import datetime

class Fee_Request_Form(forms.Form):
    CHOICES = (('Cash', 'Cash'), ('Cheque', 'Cheque'), ('Online Payment', 'Online Payment'))
    

    receipt_no = forms.CharField(required=True, label='Receipt Number *', max_length=100, widget=forms.TextInput(
        attrs={'class': "form-control", 'placeholder': 'Receipt Number'}))
    payment_mode = forms.ChoiceField(choices=CHOICES, label='Payment Mode *',
                                     widget=forms.Select(attrs={'class': "form-control"}))
    
    transaction_id = forms.CharField(required=False, label='Transaction ID *', widget=forms.TextInput(
        attrs={'class': "form-control", 'placeholder': 'Transaction ID'}))
    cheque_no = forms.CharField(required=False, label='Cheque Number *',
                                widget=forms.TextInput(attrs={'class': "form-control", 'placeholder': 'Cheque Number'}))
    payment_date = forms.DateField(label='Payment Date *',widget=forms.widgets.DateTimeInput(attrs={"type": "date", 'class': "form-control"}))
    payment_proof = forms.FileField(required=False, label='Payment Proof', widget=forms.FileInput(
        attrs={'class': "form-control", 'placeholder': 'Payment Proof', 'accept': 'application/pdf'}))

    amount = forms.DecimalField(label='Amount *', widget=forms.NumberInput(attrs={'min':0,'max':500000,'class': "form-control"}))

    declare_semester = forms.ChoiceField(widget=forms.Select(attrs={'class': "form-control"}))

    def __init__(self, *args, **kwargs):
        prog = kwargs.pop('prog')
        super(Fee_Request_Form,self).__init__(*args, **kwargs)
        all_sems = Semester.objects.all().filter(programme_id=prog).values_list('id','name')

        CHOICES = list()
        for sem in all_sems:
            CHOICES.append((sem[0], sem[1]))

        self.fields['declare_semester'].choices = CHOICES

    def clean(self):
        cleaned_data = super(Fee_Request_Form, self).clean()
        receipt_no = cleaned_data.get("receipt_no")
        amount = cleaned_data.get("amount")
        payment_date = cleaned_data.get("payment_date")

        # check if amount is above 0
        if amount < 0:
            self.add_error('amount', "The amount should be above 0")

        # if Fee_Request.objects.filter(receipt_no=receipt_no).exists():
        #     self.add_error('receipt_no', "This receipt no already exists.")

        if payment_date > datetime.date.today():
            self.add_error('payment_date', "Can't select future date")


        # return any errors if found
        return self.cleaned_data

 
class Fee_Request_migration_Form(forms.Form):
    CHOICES = (('Cash', 'Cash'), ('Cheque', 'Cheque'), ('Online Payment', 'Online Payment'))

    receipt_no = forms.CharField(required=True, label='Receipt Number *', max_length=100, widget=forms.TextInput(
        attrs={'class': "form-control", 'placeholder': 'Receipt Number'}))
    payment_mode = forms.ChoiceField(choices=CHOICES, label='Payment Mode *',
                                     widget=forms.Select(attrs={'class': "form-control"}))
    transaction_id = forms.CharField(required=False, label='Transaction ID', widget=forms.TextInput(
        attrs={'class': "form-control", 'placeholder': 'Transaction ID'}))
    cheque_no = forms.CharField(required=False, label='Cheque Number',
                                widget=forms.TextInput(attrs={'class': "form-control", 'placeholder': 'Cheque Number'}))
    payment_date = forms.DateField(label='Payment Date *',
                                   widget=forms.widgets.DateTimeInput(attrs={"type": "date", 'class': "form-control"}))
    payment_proof = forms.FileField(required=False, label='Payment Proof', widget=forms.FileInput(
        attrs={'class': "form-control", 'placeholder': 'Payment Proof', 'accept': 'application/pdf,image/*'}))

    amount = forms.DecimalField(label='Amount *', widget=forms.NumberInput(attrs={'class': "form-control"}))

    def clean(self):
        cleaned_data = super(Fee_Request_migration_Form, self).clean()
        receipt_no = cleaned_data.get("receipt_no")
        amount = cleaned_data.get("amount")

        mig = Specialfees.objects.last().migration

        # check if amount is above 0
        if amount !=mig:
            self.add_error('amount', f"The amount requested should be {mig}")

        # return any errors if found
        return self.cleaned_data


class Fee_Request_convocation_Form(forms.Form):
    CHOICES = (('Cash', 'Cash'), ('Cheque', 'Cheque'), ('Online Payment', 'Online Payment'))

    receipt_no = forms.CharField(required=True, label='Receipt Number *', max_length=100, widget=forms.TextInput(
        attrs={'class': "form-control", 'placeholder': 'Receipt Number'}))
    payment_mode = forms.ChoiceField(choices=CHOICES, label='Payment Mode *',
                                     widget=forms.Select(attrs={'class': "form-control"}))

    transaction_id = forms.CharField(required=False, label='Transaction ID', widget=forms.TextInput(
        attrs={'class': "form-control", 'placeholder': 'Transaction ID'}))
    cheque_no = forms.CharField(required=False, label='Cheque Number',
                                widget=forms.TextInput(attrs={'class': "form-control", 'placeholder': 'Cheque Number'}))
    payment_date = forms.DateField(label='Payment Date *',widget=forms.widgets.DateTimeInput(attrs={"type": "date", 'class': "form-control"}))
    payment_proof = forms.FileField(required=False, label='Payment Proof', widget=forms.FileInput(
        attrs={'class': "form-control", 'placeholder': 'Payment Proof', 'accept': 'application/pdf,image/*'}))

    amount = forms.DecimalField(widget=forms.NumberInput(attrs={'class': "form-control"}))

    attend_convo = forms.BooleanField(widget=forms.CheckboxInput(attrs={'class': "form-control"}))

    def clean(self):
        cleaned_data = super(Fee_Request_convocation_Form, self).clean()
        receipt_no = cleaned_data.get("receipt_no")
        amount = cleaned_data.get("amount")

        grad = Specialfees.objects.last().graduation

        # check if amount is above 0
        if amount != grad:
            self.add_error('amount', f"The amount requested should be {grad}")
        # return any errors if found
        return self.cleaned_data


class Fee_Request_accomodation_Form(forms.Form):
    CHOICES = (('Cash', 'Cash'), ('Cheque', 'Cheque'), ('Online Payment', 'Online Payment'))

    acc_id = forms.CharField(required=True, label='Accomodation ID *', max_length=100, widget=forms.TextInput(
        attrs={'class': "form-control", 'placeholder': 'Receipt Number'}))

    receipt_no = forms.CharField(required=True, label='Receipt Number *', max_length=100, widget=forms.TextInput(
        attrs={'class': "form-control", 'placeholder': 'Receipt Number'}))
    payment_mode = forms.ChoiceField(choices=CHOICES, label='Payment Mode *',
                                     widget=forms.Select(attrs={'class': "form-control"}))

    transaction_id = forms.CharField(required=False, label='Transaction ID *', widget=forms.TextInput(
        attrs={'class': "form-control", 'placeholder': 'Transaction ID'}))
    cheque_no = forms.CharField(required=False, label='Cheque Number *',
                                widget=forms.TextInput(attrs={'class': "form-control", 'placeholder': 'Cheque Number'}))
    payment_date = forms.DateField(label='Payment Date *',widget=forms.widgets.DateTimeInput(attrs={"type": "date", 'class': "form-control"}))
    payment_proof = forms.FileField(required=False, label='Payment Proof', widget=forms.FileInput(
        attrs={'class': "form-control", 'placeholder': 'Payment Proof', 'accept': 'application/pdf,image/*'}))

    amount = forms.DecimalField(label='Amount *', widget=forms.NumberInput(attrs={'class': "form-control"}))

    def __init__(self, *args, **kwargs):
        super(Fee_Request_accomodation_Form, self).__init__(*args, **kwargs)
        self.fields['acc_id'].widget.attrs['readonly'] = True


    def clean(self):
        cleaned_data = super(Fee_Request_accomodation_Form, self).clean()
        receipt_no = cleaned_data.get("receipt_no")
        amount = cleaned_data.get("amount")

        payment_date = cleaned_data.get("payment_date")

        if payment_date > datetime.date.today():
            self.add_error('payment_date', "Can't select future date")

        # check if amount is above 0
        if amount < 0:
            self.add_error('amount', "The amount should be above 0")
        # return any errors if found
        return self.cleaned_data
