from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MinValueValidator
from decimal import Decimal

from semester.models import Semester
from student.models import Student

from django.core.validators import FileExtensionValidator
# class Post(models.Model):
#     PDF = models.FileField(null=True, blank=True)])

# Create your models here.
class Fee_Request(models.Model):
    receipt_no = models.CharField(max_length=100)
    payment_mode = models.CharField(max_length=30)
    transaction_id = models.CharField(max_length=30,blank=True)
    cheque_no = models.CharField(max_length=30, default=None)
    payment_date = models.DateTimeField()
    payment_proof = models.FileField(upload_to='fees_proof',null=True)
    requested_amount =  models.DecimalField(decimal_places=2, default=0,max_digits=12, validators=[MinValueValidator(Decimal('0.00'))],verbose_name='Other Charges')
    approved_amount =  models.DecimalField(decimal_places=2, default=0,max_digits=12, validators=[MinValueValidator(Decimal('0.00'))],blank=True)
    remaining_amount = models.FloatField(null=True)
    approved = models.SmallIntegerField(null=True)
    # approved flag -> 0 : pending, 1 : accepted, 2 : rejected
    
    remarks = models.CharField(max_length=50, default=None, null=True)
    declare_semester = models.ForeignKey(Semester,on_delete=models.CASCADE,)
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    approved_date = models.DateTimeField(auto_now=True)
  

class Fee_Request_migration(models.Model):
    receipt_no = models.CharField(max_length=100)
    payment_mode = models.CharField(max_length=30)
    transaction_id = models.CharField(max_length=30,blank=True)
    cheque_no = models.CharField(max_length=30, default=None)
    payment_date = models.DateTimeField()
    payment_proof = models.FileField(upload_to='fees_proof',null=True)
    amount =  models.DecimalField(decimal_places=2, default=0,max_digits=12, validators=[MinValueValidator(Decimal('0.00'))],blank=True, verbose_name='Other Charges')
    remaining_amount = models.FloatField(null=True)
    approved = models.BooleanField(null=True)
    remarks = models.CharField(max_length=50, default=None, null=True)
    student = models.ForeignKey(Student, on_delete=models.CASCADE)

class Fee_Request_accomodation(models.Model):
    receipt_no = models.CharField(max_length=100)
    payment_mode = models.CharField(max_length=30)
    transaction_id = models.CharField(max_length=30,blank=True)
    cheque_no = models.CharField(max_length=30, default=None)
    payment_date = models.DateTimeField()
    payment_proof = models.FileField(upload_to='fees_proof',null=True)
    approved = models.BooleanField(null=True)
    amount =  models.DecimalField(decimal_places=2, default=0,max_digits=12, validators=[MinValueValidator(Decimal('0.00'))],blank=True, verbose_name='Other Charges')
    student = models.ForeignKey(Student, on_delete=models.CASCADE)


class Fee_Request_convocation(models.Model):
    receipt_no = models.CharField(max_length=100)
    payment_mode = models.CharField(max_length=30)
    transaction_id = models.CharField(max_length=30,blank=True)
    cheque_no = models.CharField(max_length=30, default=None)
    payment_date = models.DateTimeField()
    payment_proof = models.FileField(upload_to='fees_proof',null=True)
    amount =  models.DecimalField(decimal_places=2, default=0,max_digits=12, validators=[MinValueValidator(Decimal('0.00'))],blank=True, verbose_name='Other Charges')
    remaining_amount = models.FloatField(null=True)
    approved = models.BooleanField(null=True)
    remarks = models.CharField(max_length=50, default=None, null=True)
    register_date = models.DateTimeField(auto_now_add=True)
    attend_convo = models.BooleanField(default=True)
    student = models.ForeignKey(Student, on_delete=models.CASCADE)