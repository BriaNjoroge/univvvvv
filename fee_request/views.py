from django.shortcuts import render, redirect, HttpResponseRedirect
from .models import Fee_Request
from .forms import Fee_Request_Form

import os

from django.shortcuts import render, get_object_or_404, redirect

from student.models import Student
from fee_structure.models import Fee

from semester.models import Semester

from django.contrib import messages


# Create your views here.
def request_fee(request):
    data = {}

    form = Fee_Request_Form(prog=request.user.Student.programme.id)

    navshow = 'list'
    remain_fee = None
    student = get_object_or_404(Student, id=request.user.Student.id)
    if request.method == 'POST':
        fee_student = Fee_Request.objects.filter(student_id=request.user.Student.id)
        if fee_student.count() == 0:

            student_programme = student.programme_id
            student_batch = student.batch
            student_category = student.category
            if student.nationality == "Indian":
                student_country = "Local"

            else:
                student_country = "Foreigner"
            student_sem = student.current_sem.id
            print(student_programme, student_sem, student_category, student_country)
            try:
                query = Fee.objects.filter(programme_id=student_programme).filter(
                    category__icontains=student_category).filter(country__icontains=student_country).values_list(
                    'total_amount', flat=True).get(semister_id=student_sem)

                remain_fee = query
            except:
                remain_fee = 100000
        else:
            remain_fee = fee_student.latest('approved_date').remaining_amount
        form = Fee_Request_Form(request.POST, prog=request.user.Student.programme.id)

        if form.is_valid():

            fee_request_data = Fee_Request()
            fee_request_data.receipt_no = request.POST.get('receipt_no')
            fee_request_data.payment_mode = request.POST.get('payment_mode')
            fee_request_data.transaction_id = request.POST.get('transaction_id')
            fee_request_data.cheque_no = request.POST.get('cheque_no')
            fee_request_data.payment_date = request.POST.get('payment_date')
            if 'payment_proof' in request.FILES:

                filename = request.FILES['payment_proof']
                if str(filename).lower().split('.')[-1] == 'pdf':
                    if int(len(request.FILES['payment_proof'].read())) < (3 * 1024 * 1024):
                        fee_request_data.payment_proof = request.FILES['payment_proof']
                    else:
                        messages.warning(request, "please upload a pdf within 3 mb")
                        navshow = 'add'
                        return redirect('request_fee')

                else:
                    messages.warning(request, "please upload a pdf")
                    navshow = 'add'
                    return redirect('request_fee')

            else:
                fee_request_data.payment_proof = None

            fee_request_data.category = request.POST.get('category')

            if request.POST.get('declare_semester') != '':
                fee_request_data.declare_semester_id = request.POST.get('declare_semester')
            fee_request_data.student_id = request.user.Student.id
            fee_request_data.approved = 0
            fee_request_data.requested_amount = request.POST.get('amount')
            fee_request_data.remaining_amount = remain_fee
            fee_request_data.save()
            messages.success(request, "Fee request applied successfully")

            return redirect('request_fee')
        else:
            navshow = 'add'

    else:
        form = Fee_Request_Form(prog=request.user.Student.programme.id)

    Fee_Request_list = Fee_Request.objects.filter(student=request.user.Student.id).order_by('-id')
    data['Fee_Request_list'] = Fee_Request_list
    data['form'] = form

    data['navshow'] = navshow

    return render(request, 'Fee_Request_list.html', data)