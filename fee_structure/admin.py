from django.contrib import admin
from .models import Fee, Specialfees


# Register your models here.
@admin.register(Fee)
class FeeAdmin(admin.ModelAdmin):
    list_display = ('programme', 'semister',)
    list_filter = ('programme',)
    search_fields = ('programme',)


@admin.register(Specialfees)
class SpecialfeesAdmin(admin.ModelAdmin):

    def has_add_permission(self, *args, **kwargs):
        return not Specialfees.objects.exists()