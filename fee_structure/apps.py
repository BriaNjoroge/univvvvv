from django.apps import AppConfig


class FeeStructureConfig(AppConfig):
    name = 'fee_structure'
