COUNTRY_CHOICES = (
        ('Local', 'Local'),
        ('Foreigner', 'Foreigner'),
    )
CATEGORY_CHOICES = (
        ('Government Sponsored', 'Government Sponsored'),
        ('Self Sponsored', 'Self Sponsored'),
    )

YEAR_CHOICES = [(i, i) for i in range(1, 7)]
SEMISTER_CHOICES = [(i, i) for i in range(1, 8)]
