from django.forms import ModelForm
from .models import Fee
from .models import Specialfees


# Create the form class.
class FeeForm(ModelForm):
    class Meta:
        model = Fee
        fields = '__all__'
        exclude = ('created_by', 'total_amount',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        f2 = ["tution", "student_Union", "library_Fees", "caution_Money", "registration", "laboratory", "examination",
              "activity", "student_id", "ict_charges", "others"]
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({'class': 'form-control'})

        for field in f2:
            self.fields[field].widget.attrs['min'] = 0

    def clean(self):
        cleaned_data = super(FeeForm, self).clean()
        programme = cleaned_data.get("programme")
        category = cleaned_data.get("category")
        country = cleaned_data.get("country")
        semister = cleaned_data.get("semister")

        if Fee.objects.filter(programme=programme, category=category, country=country, semister=semister).exists():
            self.add_error('programme', "This record already exists.")


class SpecialfeesForm(ModelForm):
    class Meta:
        model = Specialfees
        fields = '__all__'
        exclude = ('created_by', 'updated_date',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({'class': 'form-control'})

    def clean(self):
        cleaned_data = super(SpecialfeesForm, self).clean()

        graduation = cleaned_data.get("graduation")
        migration = cleaned_data.get("migration")
        exam = cleaned_data.get("exam")
        accomodation = cleaned_data.get("accomodation")

        if graduation < 0:
            self.add_error('graduation', "The amount should be above 0")

        if migration < 0:
            self.add_error('migration', "The amount should be above 0")

        if exam < 0:
            self.add_error('exam', "The amount should be above 0")

        if accomodation < 0:
            self.add_error('accomodation', "The amount should be above 0")
