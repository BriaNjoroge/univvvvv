from django.db import models
from programme.models import Programme
from django.contrib.auth.models import User

from semester.models import Semester
from .choices import COUNTRY_CHOICES, CATEGORY_CHOICES, YEAR_CHOICES, SEMISTER_CHOICES
from django.core.validators import MinValueValidator
from datetime import datetime
from simple_history.models import HistoricalRecords



# Create your models here.
class Fee(models.Model):
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, default=1)
    programme = models.ForeignKey(Programme, on_delete=models.CASCADE, verbose_name='Associated Programme *')
    category = models.CharField(max_length=128, choices=CATEGORY_CHOICES, verbose_name='Programme Category *')
    country = models.CharField(max_length=128, choices=COUNTRY_CHOICES, verbose_name='Foreigner/Local *')
    semister = models.ForeignKey(Semester,on_delete=models.CASCADE,verbose_name='Semester *', )
    tution = models.DecimalField(verbose_name='Tuition Money Charges *', max_digits=12, decimal_places=2)

    student_Union = models.DecimalField(decimal_places=2, default=0, max_digits=12,
                                        blank=True,
                                        verbose_name='Student Union Charges')
    library_Fees = models.DecimalField(decimal_places=2, default=0, max_digits=12,
                                       blank=True,
                                       verbose_name='Library Fee Charges')
    caution_Money = models.DecimalField(decimal_places=2, default=0, max_digits=12,
                                        blank=True,
                                        verbose_name='Caution Money Charges')
    registration = models.DecimalField(decimal_places=2, default=0, max_digits=12,
                                       blank=True,
                                       verbose_name='Student Registration Charges')
    laboratory = models.DecimalField(decimal_places=2, default=0, max_digits=12,
                                     blank=True,
                                     verbose_name='Lab Charges')
    examination = models.DecimalField(decimal_places=2, default=0, max_digits=12,
                                      blank=True,
                                      verbose_name='Examination Charges')
    activity = models.DecimalField(decimal_places=2, default=0, max_digits=12,
                                   blank=True,
                                   verbose_name='Activity Charges')
    student_id = models.DecimalField(decimal_places=2, default=0, max_digits=12,
                                     blank=True,
                                     verbose_name='Student Identity Card Charges')
    ict_charges = models.DecimalField(decimal_places=2, default=0, max_digits=12,
                                      blank=True,
                                      verbose_name='ICT Charges')
    others = models.DecimalField(decimal_places=2, default=0, max_digits=12,
                                 blank=True,
                                 verbose_name='Other Charges')
    total_amount = models.DecimalField(decimal_places=2, default=0, max_digits=12)

    # @property
    # def total_amount(self):
    #     return self.tution+self.ict_charges + self.laboratory + self.registration +self.caution_Money

    def __str__(self):
        return f" {self.programme} Semester {self.semister}"

    def save(self, *args, **kwargs):
        self.total_amount = self.tution + self.student_Union + self.library_Fees + self.caution_Money + self.registration + self.laboratory + self.examination + self.activity + self.student_id + self.ict_charges + self.others
        super(Fee, self).save(*args, **kwargs)


class Specialfees(models.Model):
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, default=1)

    graduation = models.DecimalField(decimal_places=2, default=0, max_digits=12,
                                     blank=True,
                                     verbose_name='Graduation Charges')
    migration = models.DecimalField(decimal_places=2, default=0, max_digits=12,
                                    blank=True,
                                    verbose_name='Migration Charges')
    exam = models.DecimalField(decimal_places=2, default=0, max_digits=12,
                               blank=True, verbose_name='Exam Charges')
    accomodation = models.DecimalField(decimal_places=2, default=0, max_digits=12,
                                       blank=True,
                                       verbose_name='Accomodation Charges')
    updated_date = models.DateTimeField(default=datetime.now)
