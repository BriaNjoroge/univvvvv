from django import template
from babel.numbers import format_decimal,format_currency

register = template.Library()


@register.simple_tag
def currency(a):
    # dec = round(a,2)
    # dec = a
    #
    # num = str(a)
    # print("{:.2f}".format(float(num.replace('\U00002013', '-'))))
    # dec = ("{:.2f}".format(float(num.replace('\U00002013', '-'))))

    # return format_decimal(dec, locale='hi_IN')
    return format_currency(a, 'INR',locale='en_IN')
