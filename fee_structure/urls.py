from django.urls import path
from . import views

urlpatterns = [
    path('', views.fee_structure, name='fee_structure'),
    path('fee/<int:id>/delete', views.fee_statement_delete_view, name='fee_statement_delete_view'),
    path('<int:id>/update', views.fee_statement_update_view, name='fee_statement_update_view'),
    path('special-fee/<int:id>/update', views.special_fee_statement_update_view, name='special_fee_statement_update_view'),
    path('special-fee/<int:id>/delete', views.special_fee_statement_delete_view, name='special_fee_statement_delete_view'),

]