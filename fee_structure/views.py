from django.shortcuts import render, redirect
from .forms import FeeForm, SpecialfeesForm
from .models import Fee, Specialfees
from django.contrib import messages
from django.shortcuts import get_object_or_404


def fee_structure(request):
    """

    This function accepts a form which you insert a fees statement for a programme
    """
    fees = Fee.objects.all().order_by('-id')
    form = FeeForm()

    special_fees_form = SpecialfeesForm()
    specialfees = Specialfees.objects.all().order_by('-updated_date__minute')
    activetab = 'list'

    if request.method == 'POST':

        if 'normal_fees' in request.POST:
            activetab = 'add'
            form = FeeForm(request.POST)
            print('normal')

            if form.is_valid():
                # process form data
                obj = Fee()  # gets new object
                if request.user.is_authenticated:
                    obj.created_by_id = request.user.id
                # finally save the object in db
                else:
                    obj.created_by_id = 1

                obj.programme = form.cleaned_data['programme']
                obj.category = form.cleaned_data['category']
                obj.country = form.cleaned_data['country']
                obj.semister = form.cleaned_data['semister']
                obj.student_Union = form.cleaned_data['student_Union']
                obj.library_Fees = form.cleaned_data['library_Fees']
                obj.caution_Money = form.cleaned_data['caution_Money']
                obj.registration = form.cleaned_data['registration']
                obj.tution = form.cleaned_data['tution']
                obj.laboratory = form.cleaned_data['laboratory']
                obj.student_id = form.cleaned_data['student_id']
                obj.ict_charges = form.cleaned_data['ict_charges']
                obj.others = form.cleaned_data['others']

                obj.save()
                messages.success(request, f"{form.cleaned_data['programme']} fee structure has been added successfully")
                return redirect('fee_structure')

        elif 'special_fess' in request.POST:
            activetab = 'add_special'

            special_fees_form = SpecialfeesForm(request.POST)
            print('special_fees')
            if special_fees_form.is_valid():
                obj = Specialfees()  # gets new object
                if request.user.is_authenticated:
                    obj.created_by_id = request.user.id
                # finally save the object in db
                else:
                    obj.created_by_id = 1

                obj.graduation = special_fees_form.cleaned_data['graduation']
                obj.migration = special_fees_form.cleaned_data['migration']
                obj.exam = special_fees_form.cleaned_data['exam']
                obj.accomodation = special_fees_form.cleaned_data['accomodation']

                obj.save()

                messages.success(request, "The special fees have been added successfully")

                return redirect('fee_structure')

    else:
        form = FeeForm()
        special_fees_form = SpecialfeesForm()

    return render(request, 'fee_structure_list.html', {'form': form,
                                                       'special_fees_form': special_fees_form,
                                                       'fees': fees,
                                                       'specialfees': specialfees,
                                                       'activetab': activetab
                                                       })


def fee_statement_delete_view(request, id=None):
    """

    :param id: id of the object
    :return: a list of the fee statements available

    This function deletes a Fee Statement of a particular programme.
    """
    fee_statement = get_object_or_404(Fee, id=id)

    if request.user.is_authenticated:
        fee_statement.delete()
        messages.success(request, "Fee statement successfully deleted!")
        return redirect("fee_structure")

    return render(request, 'fee_structure_list.html')


def special_fee_statement_delete_view(request, id=None):
    """

    :param id: id of the object
    :return: a list of the fee statements available

    This function deletes a Fee Statement of a particular programme.
    """
    activetab = 'special'
    fee_statement = get_object_or_404(Specialfees, id=id)

    if request.user.is_authenticated:
        fee_statement.delete()
        messages.success(request, "Fee statement successfully deleted!")
        return redirect("fee_structure")

    return render(request, 'fee_structure_list.html', {'activetab': activetab})


def fee_statement_update_view(request, id):
    fee = get_object_or_404(Fee, id=id)

    initial = {
        'programme': fee.programme,
        'category': fee.category,
        'country': fee.country,
        'semister': fee.semister,
        'tution': fee.tution,
    }

    form = FeeForm(initial=initial)

    if request.method == 'POST':

        form = FeeForm(request.POST)

        if form.is_valid():
            # process form data
            obj = Fee.objects.get(id=id)  # gets new object
            obj.programme = form.cleaned_data['programme']
            obj.category = form.cleaned_data['category']
            obj.country = form.cleaned_data['country']
            obj.semister = form.cleaned_data['semister']
            obj.student_Union = form.cleaned_data['student_Union']
            obj.library_Fees = form.cleaned_data['library_Fees']
            obj.caution_Money = form.cleaned_data['caution_Money']
            obj.registration = form.cleaned_data['registration']
            obj.tution = form.cleaned_data['tution']
            obj.laboratory = form.cleaned_data['laboratory']
            obj.student_id = form.cleaned_data['student_id']
            obj.ict_charges = form.cleaned_data['ict_charges']
            obj.others = form.cleaned_data['others']
            obj.save()

            messages.info(request, 'Fee Statement updated successfully!')
            return redirect('fee_structure')

    return render(request, 'update_fee_statement.html', {'form': form, 'student': fee})


def special_fee_statement_update_view(request, id):
    fee = get_object_or_404(Specialfees, id=id)

    special_fees_form = SpecialfeesForm(initial=fee.__dict__)

    if request.method == 'POST':

        special_fees_form = SpecialfeesForm(request.POST)

        if special_fees_form.is_valid():
            # process form data
            obj = Specialfees.objects.get(id=id)  # gets new object
            obj.graduation = special_fees_form.cleaned_data['graduation']
            obj.migration = special_fees_form.cleaned_data['migration']
            obj.exam = special_fees_form.cleaned_data['exam']
            obj.accomodation = special_fees_form.cleaned_data['accomodation']

            obj.save()
            # obj.save()

            messages.info(request, 'Fee Statement updated successfully!')
            return redirect('fee_structure')



    return render(request, 'update_fee_statement.html', {'form': special_fees_form, 'student': fee})
