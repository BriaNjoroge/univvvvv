from django.apps import AppConfig


class FeeTransactionConfig(AppConfig):
    name = 'fee_transaction'
