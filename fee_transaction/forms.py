from django import forms
from django.forms import ModelForm
from fee_request.models import Fee_Request
from semester.models import Semester


# from .models import Fee_Transaction

class Fee_Transaction_Form(ModelForm):
    class Meta:
        model = Fee_Request

        fields = ['approved', 'approved_amount', 'approved', 'remarks', 'declare_semester']
        labels = {
            'approved_amount': 'Amount',
            'approved': 'Approved',
            'remarks': 'Remarks',
            'declare_semester': 'declare_semester'
        }

    def __init__(self, *args, **kwargs):
        prog = kwargs.pop('prog')
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({'class': 'form-control'})
        self.fields['approved_amount'].required = False
        self.fields['remarks'].required = False
        self.fields['declare_semester'].required = False

        self.fields['approved'] = forms.ChoiceField(widget=forms.Select(attrs={'class': "form-control"}),
                                                    choices=[(1, "Yes"), (0, "No")])

        all_sems = Semester.objects.all().filter(programme_id=prog).values_list('id', 'name')
        print("progaaaaaa", all_sems)

        CHOICES = list()
        for sem in all_sems:
            CHOICES.append((sem[0], sem[1]))

        self.fields['declare_semester'].choices = CHOICES

    def clean(self):
        cleaned_data = super(Fee_Transaction_Form, self).clean()
        receipt_no = cleaned_data.get("receipt_no")
        amount = cleaned_data.get("approved_amount")
        approved = cleaned_data.get("approved")
        if int(approved) and not int(amount):
            print("error", amount, approved)
            self.add_error('approved_amount', "This field is required.")

        if int(amount) < 0:
            self.add_error('approved_amount', "The amount should be above 0")

        # if Fee_Transaction.objects.filter(receipt_no_id=receipt_no).exists():
        #     self.add_error('receipt_no', "This receipt id already exists.")
