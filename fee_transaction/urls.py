from django.conf.urls import url
from django.urls import path
from . import views

urlpatterns = [
    path('list', views.Fee_approved, name='Fee_approved'),    
    path('list/<int:id>/update', views.Fee_update, name='Fee_approved_id'),    
]