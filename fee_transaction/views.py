from django.contrib import messages
from django.shortcuts import render, redirect
from fee_request.models import Fee_Request
from .forms import Fee_Transaction_Form
from django.shortcuts import get_object_or_404
from django.db.models import Count, Subquery, OuterRef

from exam_registration.models import exam_registration
from student.models import Student

from fee_structure.models import Fee


# Create your views here.

def Fee_approved(request):
    data = {}

    activetab = 'list'

    # remain_amount = {}
    # stu = Fee_Request.objects.values('student_id').annotate(Count('student_id')).values('student_id')
    # for i in stu:
    #     remain_amount[i['student_id']] = Fee_Request.objects.filter(student_id = i['student_id']).last().remaining_amount
    #
    # student_remain_fee = Fee_Request.objects.filter()

    Fee_Request_data = Fee_Request.objects.filter(approved=0)

    latest_entry = Subquery(
        Fee_Request.objects.filter(student_id=OuterRef('student_id')).order_by('-approved_date').values('id')[:1])
    remain_amount = Fee_Request.objects.filter(id__in=latest_entry).exclude(remaining_amount=0).values(
        'student__admission', 'student__firstName',
        'student__lastName',
        'student__current_sem__name',
        'student__college', 'remaining_amount',
        'student__programme__name')

    data['Fee_Request'] = Fee_Request_data
    data['remain_fee'] = remain_amount
    data['activetab'] = activetab

    return render(request, 'Fee_Transaction_list.html', data)


def Fee_update(request, id):
    print("fee update")

    Fee_Request_data = get_object_or_404(Fee_Request, id=id)
    data = {}

    form = Fee_Transaction_Form(prog=Fee_Request_data.student.programme.id)

    if request.method == 'POST':
        form = Fee_Transaction_Form(request.POST, prog=Fee_Request_data.student.programme.id)
        if form.is_valid():
            #  Fee_Request.objects.get(id = request.POST.get('receipt_no'))

            approved = request.POST.get('approved', None)
            print(approved)
            print(type(approved))

            if int(approved) == 1:
                Fee_Request_data.approved_amount = request.POST.get('approved_amount')
                # Fee_Request_data.remarks = request.POST.get('remarks')
                Fee_Request_data.approved = 1

                new_remain_amount = Fee_Request_data.remaining_amount - float(request.POST.get('approved_amount'))
                print(float(request.POST.get('approved_amount')))

                # if Fee_Request_data.category == "Tution":
                Fee_Request_data.declare_semester_id = request.POST.get('declare_semester')
                stu_data = Student.objects.get(id=Fee_Request_data.student_id)
                print(stu_data.current_sem_id, request.POST.get('declare_semester'))
                print(new_remain_amount)

                if stu_data.current_sem_id != int(request.POST.get('declare_semester')):
                    if stu_data.nationality == "Indian":
                        student_country = "Local"

                    else:
                        student_country = "Foreigner"
                    print("sem fee")
                    try:

                        query = Fee.objects.filter(programme_id=stu_data.programme.id).filter(
                            category__icontains=stu_data.category).filter(
                            country__icontains=student_country).values_list('total_amount', flat=True).get(
                            semister_id=request.POST.get('declare_semester'))
                    except:
                        messages.warning(request, "Add fee statement first")
                        return redirect('/feetransaction/list')

                    remain_fee = query

                    new_remain_amount += float(remain_fee)
                stu_data.current_sem_id = request.POST.get('declare_semester')
                print(new_remain_amount)
                stu_data.save()

                if new_remain_amount < 1 and new_remain_amount > -1:
                    new_remain_amount = 0

                Fee_Request_data.remaining_amount = new_remain_amount
                studet_remain_fee = Fee_Request.objects.filter(student_id=Fee_Request_data.student_id,
                                                               approved=0).update(remaining_amount=new_remain_amount)

            elif int(approved) == 0:
                Fee_Request_data.approved = 2

            Fee_Request_data.remarks = request.POST.get('remarks')

            Fee_Request_data.save()
            messages.success(request, "Fee status updated successfully")
            return redirect('/feetransaction/list')
        # else:
        #     return redirect('/feetransaction/list')

    else:
        form = Fee_Transaction_Form(prog=Fee_Request_data.student.programme.id)

    data['form'] = form

    return render(request, 'update_fee.html', data)
