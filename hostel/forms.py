from django import forms
from .models import Hostel,HostelRoom

class AddHostelForm(forms.Form):

    HOSTEL_TYPE = (("Male", "Male"),
    ("Female", "Female"))

    name = forms.CharField(label="Hostel Name *", max_length=30, widget=forms.TextInput(attrs={'placeholder':'Hostel Name','class': "form-control"}))
    
    address = forms.CharField(label="Hostel Address *", max_length=100, widget=forms.Textarea(attrs={'placeholder':'Hostel Address', 'rows':3, 'class': "form-control"}))
    
    contact = forms.CharField(label="Contact Number *", max_length=13, widget=forms.NumberInput(attrs={'min':0,'placeholder':'Hostel Contact Number','class': "form-control"}))
    
    warden_name = forms.CharField(label="Warden Name *", max_length=30, widget=forms.TextInput(attrs={'placeholder':'Warden Name','class': "form-control"}))
    
    no_of_rooms = forms.CharField(label="Number of Rooms *", max_length=50, widget=forms.NumberInput(attrs={'min':0,'placeholder':'Number of Rooms','class': "form-control"}))
    
    MY_CHOICES = (
        ('AC Room', 'AC Room'),
        ('Computer Room', 'Computer Room'),
        ('Reading Room', 'Reading Room'),
        ('Playing Area', 'Playing Area'),
        ('Water Cooler', 'Water Cooler'),
        ('All day water', 'All day water'),
    )

    facilities = forms.MultipleChoiceField(label='Facilities *', widget=forms.CheckboxSelectMultiple(attrs={'class': "form-control"}),choices=MY_CHOICES)

    hostel_type = forms.ChoiceField(label='Hostel For *', choices=HOSTEL_TYPE, widget=forms.Select(attrs={'placeholder':'Hostel Type','class': "form-control"}))

    def __init__(self, *args, **kwargs):
        self.insertMode = kwargs.pop('insert_mode', True)
        
        super(AddHostelForm, self).__init__(*args, **kwargs)

        if not self.insertMode:
            print("hello")
            self.fields['name'].widget.attrs['readonly'] = True
            self.fields['no_of_rooms'].widget.attrs['readonly'] = True

    def clean(self):
        cleaned_data = super(AddHostelForm, self).clean()
        name = cleaned_data.get("name")
        contact = cleaned_data.get("contact")

        if int(contact) > 9999999999:
            self.add_error('contact', "Please enter a valid mobile number")

        if self.insertMode:
            if Hostel.objects.filter(name=name).exists():
                self.add_error('name', "This Hostel name already exists.")
            

class AddRoomForm(forms.Form):

    ROOM_CHOICES = (('AC', 'AC'), ('NON AC', 'NON AC'))
    
    CAPACITY_CHOICES = (('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5'))

    room_type = forms.ChoiceField(label='Room Type *', choices=ROOM_CHOICES , widget=forms.Select(attrs={'placeholder':'Room Type','class': "form-control"}))
    
    capacity = forms.ChoiceField(label='Capacity *', choices=CAPACITY_CHOICES , widget=forms.Select(attrs={'placeholder':'Capacity','class': "form-control"}))
    
    fee_amount = forms.CharField(label='Fees Amount *', max_length=30, widget=forms.NumberInput(attrs={'min':0,'placeholder':'Fee Amount','class': "form-control"}))
  
    hostel_room = forms.ModelMultipleChoiceField(label='Rooms *', widget=forms.CheckboxSelectMultiple(attrs={'class': "form-control"}), queryset=HostelRoom.objects.none() , required=False)

    def __init__(self, *args, **kwargs):
        id = kwargs.pop('id')
        super(forms.Form, self).__init__(*args, **kwargs)
        print("------------------------------------" + str(id))
        self.fields['hostel_room'].queryset = HostelRoom.objects.filter(hostel_id_id = id, status = 0)


class UpdateRoomForm(forms.Form):

    ROOM_CHOICES = (('AC', 'AC'), ('NON AC', 'NON AC'))
    
    CAPACITY_CHOICES = (('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5'))

    room_name = forms.CharField(max_length=30, widget=forms.TextInput(attrs={'placeholder':'Hostel Name','class': "form-control"}))

    room_type = forms.ChoiceField(label='Room Type *', choices=ROOM_CHOICES , widget=forms.Select(attrs={'placeholder':'Room Type','class': "form-control"}))
    
    capacity = forms.ChoiceField(label='Capacity *', choices=CAPACITY_CHOICES , widget=forms.Select(attrs={'placeholder':'Capacity','class': "form-control"}))
    
    fee_amount = forms.CharField(label='Fees Amount *', max_length=30, widget=forms.NumberInput(attrs={'min':0,'placeholder':'Fee Amount','class': "form-control"}))
  
    def __init__(self, *args, **kwargs):
        super(forms.Form, self).__init__(*args, **kwargs)
        self.fields['room_name'].widget.attrs['readonly'] = True
            
            
        