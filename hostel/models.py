from django.db import models
from django.contrib.auth.models import User
from django.conf import settings


# Create your models here.
class Hostel(models.Model):
    name = models.CharField(max_length=50, unique=True)
    address = models.TextField()
    contact = models.CharField(max_length=13)
    warden_name = models.CharField(max_length=30)
    facilities = models.TextField(default="")
    hostel_type = models.CharField(default="Male", max_length=5)
    is_active = models.BooleanField(default=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)
    no_of_room = models.SmallIntegerField()

    def __str__(self):
        return self.name


class HostelRoom(models.Model):
    hostel_id = models.ForeignKey(Hostel, on_delete=models.CASCADE)
    room_type = models.CharField(max_length=10, null=True)
    capacity = models.SmallIntegerField(default=0)
    current_capacity = models.SmallIntegerField(default=0)
    room_name = models.CharField(max_length=100)
    fee_amount = models.DecimalField(max_digits=7, decimal_places=2, default=0)
    is_active = models.BooleanField(default=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)
    status = models.SmallIntegerField(default=0)
    remaining_capacity = models.SmallIntegerField(default=0)

    def save(self, *args, **kwargs):
        self.remaining_capacity = int(self.capacity) - int(self.current_capacity)
        super(HostelRoom, self).save(*args, **kwargs)

    def __str__(self):
        return self.room_name
