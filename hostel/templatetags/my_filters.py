from django import template

register = template.Library()

def list_to_str(facilities):
    # print("hello", facilities)
    if facilities.strip():
        list1 = eval(facilities)
        if isinstance(list1, list):
            return ", ".join(list1)
        
    facilities

register.filter('list_to_str', list_to_str)