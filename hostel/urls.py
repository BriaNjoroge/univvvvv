from django.conf.urls import url
from django.urls import path, include
from . import views

urlpatterns = [
    path('view_hostel', views.view_hostel, name='view_hostel'),
    path('disable_hostel/<int:id>', views.disable_hostel, name='disable_hostel'),
    path('activate_hostel/<int:id>', views.activate_hostel, name='activate_hostel'),

    path('<int:id>', views.view_room, name='view_room'),
    path('<int:hostel_id>/disable_room/<int:room_id>', views.disable_room, name='disable_room'),
    path('<int:hostel_id>/activate_room/<int:room_id>', views.activate_room, name='activate_room'),

    path('<int:hostel_id>/clear_room', views.clear_room, name='clear_room'),
    path('clear_all_room', views.clear_all_room, name='clear_all_room'),
]
