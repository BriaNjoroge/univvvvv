from django.contrib import messages
from django.db.models import F
from django.shortcuts import render, redirect, reverse

from accomodation.models import Accomodation, AccomodationRequest
from .forms import AddHostelForm, AddRoomForm, UpdateRoomForm
from .models import Hostel, HostelRoom

# Create your views here.
def view_hostel(request):
    data = {}

    hostel_form = AddHostelForm()
    hostel_form1 = AddHostelForm(prefix='f2', insert_mode=False)

    if request.method == 'POST':

        if "add_hostel" in request.POST:

            hostel_form = AddHostelForm(request.POST)
            
            if hostel_form.is_valid():

                hostel_data = Hostel()
                hostel_data.name = request.POST.get('name')
                hostel_data.address = request.POST.get('address')
                hostel_data.contact = request.POST.get('contact')
                hostel_data.warden_name = request.POST.get('warden_name')
                hostel_data.facilities = request.POST.getlist('facilities')
                hostel_data.no_of_room = request.POST.get('no_of_rooms')
                hostel_data.hostel_type = request.POST.get('hostel_type')
                hostel_data.created_by_id = request.user.id
                hostel_data.save()

                no_of_rooms = int(request.POST.get('no_of_rooms'))

                for i in range(1,no_of_rooms+1):
                    room_data = HostelRoom()
                    room_data.hostel_id_id = hostel_data.id
                    room_data.room_name = "Room-"+ str(i)
                    room_data.created_by_id = request.user.id
                    room_data.save()

                return redirect('/hostel/view_hostel')


        elif "update_hostel" in request.POST:
            print(request.POST)

            hostel_form1 = AddHostelForm(request.POST, prefix='f2', insert_mode=False)
            
            if hostel_form1.is_valid():
                print("hello")
                hostel_data = Hostel.objects.get(id = request.POST.get('hostel_id1'))
                
                # hostel_data.name = request.POST.get('f2-name')
                hostel_data.address = request.POST.get('f2-address')
                hostel_data.contact = request.POST.get('f2-contact')
                hostel_data.warden_name = request.POST.get('f2-warden_name')
                hostel_data.facilities = request.POST.getlist('f2-facilities')
                hostel_data.hostel_type = request.POST.get('f2-hostel_type')

                hostel_data.save()

                return redirect('/hostel/view_hostel')

    data["form"] = hostel_form
    data["form1"] = hostel_form1
    
    hostel_data = Hostel.objects.all()
    data['all_data'] = hostel_data
    
    hostel_data = hostel_data.filter(is_active=True)

    # glist = list()
    # for i in hostel_data:
    #     ldict = {}    
    #     ldict['data'] = i
    #     ldict['room_count'] = HostelRoom.objects.filter(hostel_id_id = i.id, is_active=True).count()
    #     glist.append(ldict)
    
    data['active_data'] = hostel_data
    
    return render(request, 'add_hostel1.html', data)


def view_room(request, id):
    data = {}

    room_form = AddRoomForm(id=id)
    room_form1 = UpdateRoomForm(prefix="f2")

    if request.method == 'POST':
        
        if "add_room" in request.POST:
            print(request.POST)
            room_form = AddRoomForm(request.POST, id=id)

            rooms_list = list(request.POST.get('hostel_room'))

            print(rooms_list)
            # room_form.fields['hostel_room'].choices = rooms_list
            # print(rooms_list)
            if room_form.is_valid():

                rooms_list = request.POST.getlist('hostel_room')
                for i in rooms_list:
                    room_data = HostelRoom.objects.get(id = i)
                    room_data.room_type = request.POST.get('room_type')
                    room_data.capacity = request.POST.get('capacity')
                    room_data.fee_amount = request.POST.get('fee_amount')
                    room_data.status = 1
                    room_data.save()

                return redirect('/hostel/'+str(id))

        elif "update_room" in request.POST:
            room_form1 = UpdateRoomForm(request.POST, prefix="f2")
            print(request.POST)
            if room_form1.is_valid():
                print("hello")
                room_data = HostelRoom.objects.get(id = request.POST.get('room_id'))
                room_data.room_type = request.POST.get('f2-room_type')
                room_data.capacity = request.POST.get('f2-capacity')
                room_data.fee_amount = request.POST.get('f2-fee_amount')
                room_data.status = 1
                room_data.save()

                return redirect('/hostel/'+str(id))
    
    data["form"] = room_form
    data["form1"] = room_form1

    all_room_data = HostelRoom.objects.filter(hostel_id_id = id)
    data['all_rooms'] = all_room_data
    
    room_data = all_room_data.filter(is_active = True)
    data['active_rooms'] = room_data
    
    
    return render(request, 'view_room.html', data)


def activate_hostel(request, id):

    Hostel.objects.filter(id = id).update(is_active=True)
    name =  Hostel.objects.get(id = id).name

    messages.success(request, f"Hostel {name} has been activated")

    return redirect(reverse('view_hostel'))


def disable_hostel(request, id):

    Hostel.objects.filter(id = id).update(is_active=False)

    # College_programme.objects.filter(programme_id_id=id).update(is_active=True)
    name =  Hostel.objects.get(id = id).name


    messages.info(request, f"Hostel {name} has been deactivated")

    return redirect(reverse('view_hostel'))


def activate_room(request, hostel_id, room_id):

    HostelRoom.objects.filter(id = room_id).update(is_active=True)

    return redirect(reverse('view_room', args=[hostel_id]))


def disable_room(request, hostel_id, room_id):

    HostelRoom.objects.filter(id = room_id).update(is_active=False)
    return redirect(reverse('view_room', args=[hostel_id]))


def clear_room(request, hostel_id):
    HostelRoom.objects.filter(hostel_id_id=hostel_id).update(current_capacity=0,remaining_capacity=F('capacity'))

    Accomodation.objects.filter(room_id__hostel_id_id=hostel_id).update(is_active=0)

    try:
        name = HostelRoom.objects.filter(hostel_id_id=hostel_id).values('hostel_id__name')[0]['hostel_id__name']
        messages.info(request,f"Hostel {name} rooms have been cleared")

    except:
        messages.info(request,"The hostel rooms have been cleared")


    return redirect(reverse('view_hostel'))


def clear_all_room(request):
    """

    :param request:
    :return: Clear all requests, and roo vacancy for students to apply again
    """
    AccomodationRequest.objects.all().delete()
    HostelRoom.objects.all().update(current_capacity=0,remaining_capacity=F('capacity'))
    Accomodation.objects.all().update(is_active=0)
    messages.info(request,"All hostels have been cleared")
    return redirect(reverse('view_hostel'))