from django.db import models
from django.contrib.auth.models import User

# from college.models import College
ROLE_CHOICES = (
      (1, 'university'),
      (2, 'college'),
      (3, 'student'),
  )

# Create your models here.
class University(models.Model):
  user = models.OneToOneField(User, related_name="university", on_delete=models.CASCADE)
  name = models.CharField(max_length=100, unique=True)
  logo = models.ImageField(default='university_logo/logo.png')
  created_date = models.DateTimeField(auto_now_add = True)
  updated_date = models.DateTimeField(auto_now = True)