from django.shortcuts import render
from django.urls import reverse

from each_student_portal.forms import ChangePwdForm
from .forms import LoginForm
from django.shortcuts import redirect
from django.contrib import auth, messages
from django.contrib.auth.hashers import check_password, make_password
from django.contrib.auth.models import User

# Create your views here. 
def login(request):

    if request.method == 'POST':

        form = LoginForm(request.POST)

        if form.is_valid():
            form = LoginForm()
            username=str(request.POST.get('user_name'))
            username = username.strip()

            userdata = auth.authenticate(username=username, password=request.POST.get('password'))

            if userdata is not None:
                auth.login(request, userdata)
                username1 = str(request.POST.get('user_name'))
                # print(userdata.groups.all[0])

                return redirect('dashboard')
            else:
                messages.error(request, "Incorrect username or password")
                return redirect('login')

    else:
        form = LoginForm()

    return render(request, 'login.html', {'form': form})

def logoutfun(request):
    auth.logout(request)
    return redirect('login')


def change_password(request):
    print("USER: ", request.user.id)

    if request.method == 'POST':

        form = ChangePwdForm(request.POST)

        if form.is_valid():
            new_password = request.POST.get('new_password', '')
            User.objects.filter(id = request.user.id).update(password = make_password(new_password))

            # company.objects.filter(id = request.user.company_data.id).update(is_change_pwd = True)

            messages.success(request, 'Password successfully changed.')
            auth.logout(request)
            return redirect(reverse('login'))

    else:
        form = ChangePwdForm()


    return render(request, 'change_password.html',{'form':form})