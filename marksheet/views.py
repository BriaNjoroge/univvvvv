from django.shortcuts import render ,get_object_or_404
from results.models import result
from reassessment.models import reassessmentteacher
from backlog.models import backlogteacher
from exam_appear.models import appearance
from student.models import Student
# Create your views here.
from django.contrib import messages



def marks(request):
        """
        :param request:
        :return: the fee statement of the student
        """ 
        # try:
        studentdata = get_object_or_404(Student,id=request.user.Student.id)
        seatnodata = get_object_or_404(appearance, student_id=request.user.Student.id)
        print(seatnodata)
        results = result.objects.filter(seatno__seatno_id = seatnodata.id)

        reassessment  = reassessmentteacher.objects.filter(seatno_id = seatnodata.id)
        print(reassessment)
        backlog = backlogteacher.objects.filter(seatno_id = seatnodata.id)
         
        prepare_result = {}
        for resultdata in results:
            try:
                if resultdata.marks != 'AB':
                    prepare_result[resultdata.course_id.semester_id.name] = prepare_result[resultdata.course_id.semester_id.name] + [[resultdata.course_id.name,float(resultdata.marks),float(resultdata.course_id.pass_marks)]]
                else :
                    prepare_result[resultdata.course_id.semester_id.name] = prepare_result[resultdata.course_id.semester_id.name] + [[resultdata.course_id.name,resultdata.marks,resultdata.course_id.pass_marks]]
            except:
                prepare_result[resultdata.course_id.semester_id.name] = []
                if resultdata.marks != 'AB':
                    prepare_result[resultdata.course_id.semester_id.name] = prepare_result[resultdata.course_id.semester_id.name] + [[resultdata.course_id.name,float(resultdata.marks),float(resultdata.course_id.pass_marks)]]
                else :
                    prepare_result[resultdata.course_id.semester_id.name] = prepare_result[resultdata.course_id.semester_id.name] + [[resultdata.course_id.name,resultdata.marks,resultdata.course_id.pass_marks]]
                
        print(prepare_result)
        total_marks = {}
        for key , values in prepare_result.items() :
            for mark in values:
                if mark[1] != 'AB':
                    try:
                        total_marks[key] = total_marks[key] + float(mark[1])
                    except:
                        total_marks[key] = float(mark[1])
        print(total_marks)


        prepare_result_reassessment = {}
        for resultdata in reassessment:
            try:
                if resultdata.marks != 'AB':
                    prepare_result_reassessment[resultdata.courseid.semester_id.name] = prepare_result_reassessment[resultdata.courseid.semester_id.name] + [[resultdata.courseid.name,float(resultdata.marks),float(resultdata.courseid.pass_marks)]]
                else :
                    prepare_result_reassessment[resultdata.courseid.semester_id.name] = prepare_result_reassessment[resultdata.courseid.semester_id.name] + [[resultdata.courseid.name,resultdata.marks,resultdata.courseid.pass_marks]]
            except:
                prepare_result_reassessment[resultdata.courseid.semester_id.name] = []
                if resultdata.marks != 'AB':
                    prepare_result_reassessment[resultdata.courseid.semester_id.name] = prepare_result_reassessment[resultdata.courseid.semester_id.name] + [[resultdata.courseid.name,float(resultdata.marks),float(resultdata.courseid.pass_marks)]]
                else :
                    prepare_result_reassessment[resultdata.courseid.semester_id.name] = prepare_result_reassessment[resultdata.courseid.semester_id.name] + [[resultdata.courseid.name,resultdata.marks,resultdata.courseid.pass_marks]]
                
        print(prepare_result_reassessment)
        total_marks_reassessment = {}
        for key , values in prepare_result_reassessment.items() :
            for mark in values:
                if mark[1] != 'AB':
                    try:
                        total_marks_reassessment[key] = total_marks_reassessment[key] + float(mark[1])
                    except:
                        total_marks_reassessment[key] = float(mark[1])
        print(total_marks_reassessment)

        prepare_result_backlog = {}
        for resultdata in backlog:
            if resultdata.marks != '':
                try:
                    if resultdata.marks != 'AB':
                        prepare_result_backlog[resultdata.courseid.semester_id.name] = prepare_result_backlog[resultdata.courseid.semester_id.name] + [[resultdata.courseid.name,float(resultdata.marks),float(resultdata.courseid.pass_marks)]]
                    else :
                        prepare_result_backlog[resultdata.courseid.semester_id.name] = prepare_result_backlog[resultdata.courseid.semester_id.name] + [[resultdata.courseid.name,resultdata.marks,resultdata.courseid.pass_marks]]
                except:
                    prepare_result_backlog[resultdata.courseid.semester_id.name] = []
                    if resultdata.marks != 'AB':
                        prepare_result_backlog[resultdata.courseid.semester_id.name] = prepare_result_backlog[resultdata.courseid.semester_id.name] + [[resultdata.courseid.name,float(resultdata.marks),float(resultdata.courseid.pass_marks)]]
                    else :
                        prepare_result_backlog[resultdata.courseid.semester_id.name] = prepare_result_backlog[resultdata.courseid.semester_id.name] + [[resultdata.courseid.name,resultdata.marks,resultdata.courseid.pass_marks]]
                
        print("backlog",prepare_result_backlog)
        total_marks_backlog = {}
        for key , values in prepare_result_backlog.items() :
            for mark in values:
                
                if mark[1] != 'AB':
                    try:
                        total_marks_backlog[key] = total_marks_backlog[key] + float(mark[1])
                    except:
                        total_marks_backlog[key] = float(mark[1])
        print("backlog",total_marks_backlog)

        


    # except:
    #     messages.warning(request,"You haven't applied for exams.")
    #     prepare_result = {}
    #     total_marks = {}
    #     studentdata = {}
    #     total_marks_reassessment = {}
    #     prepare_result_reassessment = {}
        
        context = {
            'results': prepare_result,
            'total_marks': total_marks,
            "student" : studentdata,
            "total_marks_reassessment" :total_marks_reassessment,
            "prepare_result_reassessment" : prepare_result_reassessment,
            "total_marks_backlog" :total_marks_backlog,
            "prepare_result_backlog" : prepare_result_backlog,        

        }
        return render(request, 'stu-marks.html', context)
 
