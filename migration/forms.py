from django import forms
from .models import RECEIPT_MODE


class MigrationRequestForm(forms.Form):
    phone = forms.IntegerField(label='Contact *',widget=forms.NumberInput(
        attrs={"min":0,'placeholder': 'Contact', 'class': "form-control"}))



    documents = forms.FileField(label='Document of proof', required=False, widget=forms.FileInput(
        attrs={'placeholder': 'Document of proof', 'class': "form-control",'accept':'application/pdf,image/*'}))

    receipt_mode = forms.ChoiceField(label='Receipt mode *', choices=RECEIPT_MODE, widget=forms.Select(
        attrs={'placeholder': 'Receipt mode', 'class': "form-control"}))

    delivery_address = forms.CharField(label='Delivery Address *', required=True, max_length=100, widget=forms.TextInput(
        attrs={'placeholder': 'Delivery Address', 'class': "form-control"}))

    receiving_person_name = forms.CharField(label='Name of person to receive *', required=True, max_length=50,
                                            widget=forms.TextInput(attrs={'placeholder': 'Name of person to receive',
                                                                          'class': "form-control"}))

    def clean(self):
        cleaned_data = super(MigrationRequestForm, self).clean()
        phone = cleaned_data.get('phone')


        if phone > 9999999999:
            self.add_error('phone', "Please enter a valid mobile number")
        if phone < 0:
            self.add_error('phone', "Please enter a valid mobile number")

    #     if College.objects.filter(name=cleaned_data.get('name')).exists():
    #         self.add_error('name', "This College name already exists.")

    #     if College.objects.filter(email=cleaned_data.get('email')).exists():
    #         self.add_error('name', "This Email address is already used.")
