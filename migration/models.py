from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from django.contrib.auth.models import User
from student.models import Student
from django.conf import settings
from fee_request.models import Fee_Request_migration

STATUS = (
    ("Processing", "Processing"),
    ("Approved", "Approved"),
    ("Issued", "Issued"),
    ("Rejected", "Rejected"),
)

RECEIPT_MODE = (
    ("In person", "In person"),
    ("On behalf", "On behalf"),
    ("Post", "Post")
)

 
# Create your models here.
class Migration(models.Model):
    student_id = models.ForeignKey(Student, on_delete=models.CASCADE)
    phone = models.PositiveIntegerField(default=0,
                                validators=[MaxValueValidator(9999999999), MinValueValidator(0)])
    # email = models.EmailField()
    documents = models.FileField(upload_to='migration_documents')
    status = models.CharField(choices=STATUS, default="Processing", max_length=30)
    receipt_mode = models.CharField(choices=RECEIPT_MODE, default="In person", max_length=30)
    delivery_address = models.CharField(blank=True, max_length=100)
    receiving_person_name = models.CharField(blank=True, max_length=100)
    issue_date = models.DateField(null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)
    Fee = models.ForeignKey(Fee_Request_migration , on_delete = models.CASCADE)

