from django.conf.urls import url
from django.urls import path, include 
from . import views

urlpatterns = [
    path('university-migration/students', views.university_migration, name='university_migration'),
    path('student_migration', views.student_migration, name='student_migration'),
    path('pro_apr/<int:id>', views.pro_apr, name='pro_apr'),    
    path('apr_iss/<int:id>', views.apr_iss, name='apr_iss'),    
    path('approved/<int:id>', views.pro_apr, name='pro_apr'),
    path('issued/<int:id>', views.apr_iss, name='apr_iss'),
    
    path('rejected/<int:id>', views.pro_rej, name='pro_rej')
    # path('semester/<int:id>', views.display_semester, name='display_semester'),
    # path('active_pro/<int:id>', views.active_pro, name='active_pro'), 
    # path('disable_pro/<int:id>', views.disable_pro, name='disable_pro'),       
]