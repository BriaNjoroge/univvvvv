from django.shortcuts import render, redirect ,get_object_or_404
from django.contrib.auth.models import User

from .models import Migration
from .forms import MigrationRequestForm

from fee_request.models import Fee_Request_migration , Fee_Request
from fee_request.forms import Fee_Request_migration_Form
from semester.models import Semester
from student.models import Student
from fee_structure.models import Specialfees


import datetime
from django.contrib import messages


def student_migration(request):

    data = {}

    form = MigrationRequestForm()
    form_fee = Fee_Request_migration_Form()
    student = get_object_or_404(Student, id=request.user.Student.id)
    fee_student = Fee_Request.objects.filter(student_id=request.user.Student.id)
    if request.method == 'POST':

        # if student.current_sem.id < Semester.objects.filter(programme_id=student.programme_id).last().id:
        #     messages.warning(request, 'You are not in your last semester!')
        #     return redirect('student_migration')
        if fee_student.count() > 0:
            if fee_student.last().remaining_amount > 0:
                messages.warning(request, 'You have to finish your tuition fees first!')
                return redirect('student_migration')
        else :
            messages.warning(request, 'You have to apply your tuition fees first!')
            return redirect('student_migration')


        form_fee = Fee_Request_migration_Form(request.POST)
        form = MigrationRequestForm(request.POST)
        if form.is_valid() and form_fee.is_valid():
            # insert code

            fee_student_migrate_count = Fee_Request_migration.objects.filter(student_id=request.user.Student.id)
            if fee_student_migrate_count.count() == 0:
                remain_fee = Specialfees.objects.all().last().migration
            else:
                remain_fee = fee_student_migrate_count.last().remaining_amount


            fee_request_data = Fee_Request_migration()

            fee_request_data.receipt_no = request.POST.get('receipt_no')
            fee_request_data.payment_mode = request.POST.get('payment_mode')
            fee_request_data.transaction_id = request.POST.get('transaction_id')
            fee_request_data.cheque_no = request.POST.get('cheque_no')
            fee_request_data.payment_date = request.POST.get('payment_date')



            if 'payment_proof' in request.FILES:
                filename = request.FILES['payment_proof']
                if str(filename).lower().split('.')[-1] == 'pdf':

                    fee_request_data.payment_proof = request.FILES['payment_proof']

                else:
                    messages.warning(request, "please upload a pdf")
                    navshow = 'add'
                    return redirect('student_migration')
            else:
                fee_request_data.payment_proof = None


            fee_request_data.amount = request.POST.get('amount')
            fee_request_data.student_id = request.user.Student.id
            fee_request_data.remaining_amount = remain_fee




            migr = Migration()
            migr.student_id_id = request.user.Student.id
            # migr.request_date = request.POST.get("request_date")
            migr.phone = request.POST.get("phone")
            migr.documents = request.POST.get("documents")
            migr.receipt_mode = request.POST.get("receipt_mode")
            migr.delivery_address = request.POST.get("delivery_address")
            migr.receiving_person_name = request.POST.get("receiving_person_name")
            # print('------------------\n'+ migr.receiving_person_name +'\n------------------------------\n')
            fee_request_data.save()
            migr.Fee_id = fee_request_data.id 
            migr.save()
            

            messages.success(request, f"Migration has been added successfully")
            return redirect('student_migration')

        else:
            navshow = 'add'
    else:
        form = MigrationRequestForm()
        form_fee = Fee_Request_migration_Form()

    if fee_student.count() > 0:
        eligibility = fee_student.last().remaining_amount
    else:
        eligibility = 1


    migr_data = Migration.objects.filter(student_id=request.user.Student.id).order_by('-created_date')
    data['form'] = form
    data['form_fee'] = form_fee
    data['migration_requests'] = migr_data
    data['eligibility'] = eligibility

    return render(request, 'student_migration_list.html', data)


def university_migration(request):
    data = {}
    # migr_data = Migration.objects.filter(student_id=request.user)
    migr_data = Migration.objects.all()
    data['migration_requests'] = migr_data

    return render(request, 'migration_list.html', data)


def apr_iss(request, id):
    data = Migration.objects.get(id=id)
    data.status = "Issued"
    data.issue_date = datetime.date.today()
    data.save()
    messages.success(request,"Migration has been issued")
    return redirect('university_migration')


def pro_apr(request, id):
    data = Migration.objects.get(id=id)
    data.status = "Approved"

    data.save()
    messages.success(request,"Migration has been approved")

    return redirect('university_migration')

def pro_rej(request, id):
    data = Migration.objects.get(id=id)
    data.status = "Rejected"

    data.save()
    messages.success(request,"Migration has been rejected")

    return redirect('university_migration')
