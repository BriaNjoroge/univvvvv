from django.contrib import admin
from .models import Programme
from django.utils.html import format_html

@admin.register(Programme)
class ProgrammeAdmin(admin.ModelAdmin):
    pass