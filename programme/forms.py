from django import forms
from login.models import University
from college.models import College
from django.forms import ModelForm
from programme.models import Programme


class AddProgrmmeForm(forms.Form):
    CHOICES = (('1', '1 years'), ('2', '2 years'), ('3', '3 years'), ('4', '4 years'), ('5', '5 years'))

    college = forms.ModelChoiceField(label='College *', queryset=College.objects.all(),
                                     widget=forms.Select(attrs={'class': "form-control"}),
                                     empty_label="-")

    name = forms.CharField(label='Programme Name *', max_length=100,
                           widget=forms.TextInput(attrs={'placeholder': 'Programme Name', 'class': "form-control"}))

    duration = forms.ChoiceField(label='Programme Duration *', choices=CHOICES, widget=forms.Select(
        attrs={'placeholder': 'Programme Duration', 'class': "form-control"}))

    passing_percentage = forms.CharField(label='Programme Passing Percentage *', widget=forms.NumberInput(
        attrs={"min":30,"max":100,'placeholder': 'Programme Passing Percentage', 'class': "form-control"}))
    seats = forms.CharField(label='Seats ', required=False,
                            widget=forms.NumberInput(attrs={"min":1,'placeholder': 'Seats allocated', 'class': "form-control"}))

    def clean(self):
        cleaned_data = super(AddProgrmmeForm, self).clean()
        seats = int(cleaned_data.get("seats"))
        passing_percentage = cleaned_data.get("passing_percentage")
        if seats<1:
            self.add_error('seats', "This Programme should have more than 1 seat")

        if Programme.objects.filter(name=cleaned_data.get('name')).exists():
            self.add_error('name', "This Programme already exists.")

        if float(passing_percentage) < 0:
            self.add_error('passing_percentage', "The percentage should be above 0")

class ReassignForm(forms.Form):
    CHOICES = (('1', '1 years'), ('2', '2 years'), ('3', '3 years'), ('4', '4 years'), ('5', '5 years'))

    college = forms.ModelChoiceField(label='College *', queryset=College.objects.all(),
                                     widget=forms.Select(attrs={'class': "form-control"}),
                                     empty_label="-")

    name = forms.ModelChoiceField(label='Programme Name  *', queryset=Programme.objects.all(),
                           widget=forms.Select(attrs={'class': "form-control"}),
                           empty_label="-")

    duration = forms.ChoiceField(label='Programme Duration *', choices=CHOICES, widget=forms.Select(
        attrs={'placeholder': 'Programme Duration', 'class': "form-control"}))

    passing_percentage = forms.CharField(label='Programme Passing Percentage *', widget=forms.NumberInput(
        attrs={'placeholder': 'Programme Passing Percentage', 'class': "form-control"}))

    seats = forms.CharField(label='Seats ', required=False,
                            widget=forms.NumberInput(attrs={'placeholder': 'Seats allocated', 'class': "form-control"}))

    def clean(self):
        cleaned_data = super(ReassignForm, self).clean()
        name = cleaned_data.get("name")
        passing_percentage = cleaned_data.get("passing_percentage")

        # if Programme.objects.filter(name=cleaned_data.get('name')).exists():
        #     self.add_error('name', "This Programme already exists.")

        if float(passing_percentage) < 0:
            self.add_error('passing_percentage', "The percentage should be above 0")
