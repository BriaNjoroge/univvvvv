from django.db import models
from college.models import College
from django.contrib.auth.models import User
from django.conf import settings
from simple_history.models import HistoricalRecords

# Create your models here.
class Programme(models.Model):
    name = models.CharField(max_length=100, unique=True)
    duration = models.CharField(max_length=100)
    passing_percentage = models.DecimalField(max_digits=8,decimal_places=2)
    is_active = models.BooleanField(default = True)
    seats = models.SmallIntegerField(default=0)
    college = models.ForeignKey(College, on_delete=models.CASCADE)
    created_date = models.DateTimeField(auto_now_add = True)
    updated_date = models.DateTimeField(auto_now = True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)



    def __str__(self):
        return self.name

        
