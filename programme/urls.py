from django.conf.urls import url
from django.urls import path, include 
from . import views

urlpatterns = [
    path('add', views.add_programmes, name='add_programmes'),
    path('semester/<int:id>', views.display_semester, name='display_semester'),
    path('active_pro/<int:id>', views.active_pro, name='active_pro'), 
    path('disable_pro/<int:id>', views.disable_pro, name='disable_pro'),
]