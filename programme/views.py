from django.contrib import messages
from django.shortcuts import render, redirect

from course.models import Course
from .forms import AddProgrmmeForm,ReassignForm
from .models import Programme
from college.models import College
from semester.models import Semester
from rolepermissions.decorators import has_role_decorator
from django.contrib.auth.decorators import login_required

# @login_required
# @has_role_decorator('student')
def add_programmes(request):
    print(request.user.id)
    data = {}

    form = AddProgrmmeForm()
    spform =ReassignForm()
    activetab = 'list'


    if request.method == 'POST':
        if "normal_prog" in request.POST:
            college_id = request.POST.get('college_id')

            if college_id is None:
                activetab = 'add'
                form = AddProgrmmeForm(request.POST)
                if form.is_valid():
                    pro_data = Programme()
                    pro_data.name = request.POST.get('name')
                    pro_data.college_id = request.POST.get('college')
                    if request.POST.get('seats')=='':
                        pro_data.seats = 0

                    else:
                        pro_data.seats = request.POST.get('seats')
                    pro_data.duration = request.POST.get('duration')
                    pro_data.passing_percentage = request.POST.get('passing_percentage')
                    pro_data.created_by_id = request.user.id
                    pro_data.save()

                    no_of_sem = int(request.POST.get('duration')) * 2

                    for sem in range(1, no_of_sem + 1):
                        semdata = Semester()
                        semdata.name = 'Sem '+ str(sem)
                        semdata.programme_id_id = pro_data.id
                        semdata.created_by_id = request.user.id
                        semdata.save()

                    form = AddProgrmmeForm()
                    return redirect('/programmes/add')

        elif 'reassign_prog' in request.POST:
            college_id = request.POST.get('college_id')

            if college_id is None:
                activetab = 'reassign'
                spform = ReassignForm(request.POST)
                if spform.is_valid():
                    pro_data = Programme()
                    pro_data.name = request.POST.get('name')
                    pro_data.college_id = request.POST.get('college')
                    if request.POST.get('seats') == '':
                        pro_data.seats = 0

                    else:
                        pro_data.seats = request.POST.get('seats')
                    pro_data.duration = request.POST.get('duration')
                    pro_data.passing_percentage = request.POST.get('passing_percentage')
                    pro_data.created_by_id = request.user.id
                    pro_data.save()

                    no_of_sem = int(request.POST.get('duration')) * 2

                    for sem in range(1, no_of_sem + 1):
                        semdata = Semester()
                        semdata.name = 'Sem ' + str(sem)
                        semdata.programme_id_id = pro_data.id
                        semdata.created_by_id = request.user.id
                        semdata.save()

                    spform = ReassignForm()
                    return redirect('/programmes/add')



    else:
        form = AddProgrmmeForm()
        spform = ReassignForm()

    programme_data = Programme.objects.all()

    data['form'] = form
    data['spform'] = spform
    data['programme'] = programme_data
    data['activetab'] = activetab


    return render(request, 'add_programmes.html', data)


def display_semester(request, id):

    data = {}

    sem_data = Semester.objects.filter(programme_id_id = id)

    data["sem_data"] = sem_data


    return render(request, 'display_semester.html', data)


def active_pro(request,id):
    data = Programme.objects.get(id = id)
    data.is_active = False
    Programme.objects.filter(id=id).update(is_active=False)

    data.save()
    messages.success(request,"Programme has been deactivated")

    return redirect('/programmes/add')


def disable_pro(request,id):
    data = Programme.objects.get(id = id)


    Programme.objects.filter(id=id).update(is_active=True)

    data.is_active = True
    data.save()
    messages.success(request,"Programme has been activated")
    return redirect('/programmes/add')