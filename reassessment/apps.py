from django.apps import AppConfig


class ReassessmentConfig(AppConfig):
    name = 'reassessment'
