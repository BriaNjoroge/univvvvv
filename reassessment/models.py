from django.db import models
from course.models import Course
from student.models import Student
from django.core.validators import MinValueValidator
from decimal import Decimal
from exam_appear.models import appearance
# Create your models here.
   
class reassessmentfee(models.Model):
    receipt_no = models.CharField(max_length=100)
    payment_mode = models.CharField(max_length=30)
    transaction_id = models.CharField(max_length=30,blank=True)
    cheque_no = models.CharField(max_length=30, default=None)
    payment_date = models.DateTimeField()
    payment_proof = models.FileField(upload_to='fees_proof',null=True)
    amount =  models.DecimalField(decimal_places=2, default=0,max_digits=12, validators=[MinValueValidator(Decimal('0.00'))],blank=True, verbose_name='Other Charges')    
    approved = models.BooleanField(null=True)
    remarks = models.CharField(max_length=50, default=None, null=True)
    courseid = models.CharField(max_length=50, default="")
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
  
class reassessmentteacher(models.Model): 
    seatno = models.ForeignKey(appearance, on_delete=models.CASCADE)
    courseid = models.ForeignKey(Course, on_delete=models.CASCADE)
    attendance = models.BooleanField(null=True)    
    marks = models.CharField(max_length=3)
    pass_fail = models.BooleanField(null=True)
    status = models.BooleanField(null=True)
