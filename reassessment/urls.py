from django.conf.urls import url
from django.urls import path, include 
from . import views

urlpatterns = [
    path('view_request', views.reassessment_request, name='view_reassessment_request'),
    path('list', views.reassessment_approved, name='reassessment_approved'),    
    path('list/<int:id>/update', views.reassessment_update, name='reassessment_approved_id'), 

    path('home', views.reassessment_home, name='reassessment_home'),
    path('update_marks/course/<int:course_id>', views.update_reassessment_marks, name='reassessment_update_marks'),   
    path('update_attendance', views.reassessment_attendance_list, name='reassessment_update_attendance'), 
    path('<int:id>/update', views.reassessment_attendance_update, name='reassessment_attendance_update'),   
]  