
from django.contrib import messages
from django.shortcuts import render, redirect, get_object_or_404
from .forms import reassessment_fee_form , reassessmentfee_update_Form , attendance_reassessment_form
from .models import reassessmentfee ,reassessmentteacher
from exam_appear.models import appearance , attendance
from results.forms import MarksUpdate
from results.models import result
from course.models import Course
# Create your views here.
  


def reassessment_home(request):
    
    data = {}
    data['Courses'] = Course.objects.all()
    data['activetab'] = 'list'
    return render(request, "reassessment_home.html", data)

def reassessment_request(request):
    data ={}
    form = reassessment_fee_form(student_id = request.user.Student.id)
    reassessmentdata = reassessmentfee.objects.filter(student_id=request.user.Student.id)
    if request.method == 'POST':

        if 'submit_request' in request.POST:
            
            activetab = 'add'

            reassessment_form = reassessment_fee_form(request.POST,student_id = request.user.Student.id)
            
            
            if reassessment_form.is_valid() :                

                reassessmentfee_data = reassessmentfee()

                reassessmentfee_data.receipt_no = request.POST.get('receipt_no')
                reassessmentfee_data.payment_mode = request.POST.get('payment_mode')
                reassessmentfee_data.transaction_id = request.POST.get('transaction_id')
                reassessmentfee_data.cheque_no = request.POST.get('cheque_no')
                reassessmentfee_data.payment_date = request.POST.get('payment_date')
                if 'payment_proof' in request.FILES:
                    reassessmentfee_data.payment_proof = request.FILES['payment_proof']
                else:
                    reassessmentfee_data.payment_proof = None
                reassessmentfee_data.amount = request.POST.get('amount')
                reassessmentfee_data.student_id = request.user.Student.id
                reassessmentfee_data.courseid = request.POST.getlist('courseid')
                reassessmentfee_data.approved = 0
                reassessmentfee_data.save()
                
                activetab = 'list'

                messages.success(request,"reassessment applied successfully")

    else:
        form = reassessment_fee_form(student_id = request.user.Student.id)
        
    data["form"] = form
    data["data"] = reassessmentdata
    

    return render(request, 'requestreassessment.html', data)

def reassessment_approved(request):
    data = {}

    activetab = 'list'

    reassessmentfee_Request_data = reassessmentfee.objects.filter(approved=0)

    
    data['reassessmentfee_Request_data'] = reassessmentfee_Request_data
    
    data['activetab'] = activetab

    return render(request, 'reassessment_list.html', data)

 
def reassessment_update(request, id):
    
    reassessmentfee_Request_data = get_object_or_404(reassessmentfee, id=id)
    data = {}
    
    form = reassessmentfee_update_Form()

    if request.method == 'POST':
        form = reassessmentfee_update_Form(request.POST)
        if form.is_valid():
            #  Fee_Request.objects.get(id = request.POST.get('receipt_no'))

            approved = request.POST.get('approved', None)
            
            if int(approved) == 1:
                
                reassessmentfee_Request_data.approved = 1
                seat =  get_object_or_404(appearance, student_id=reassessmentfee_Request_data.student.id)
                
                for course in eval(reassessmentfee_Request_data.courseid):
                    reassessmentteacherdata = reassessmentteacher()
                    reassessmentteacherdata.seatno_id = seat.id
                    reassessmentteacherdata.courseid_id = int(course)
                    reassessmentteacherdata.status = 0
                    reassessmentteacherdata.save()
                
            elif int(approved) == 0:
                reassessmentfee_Request_data.approved = 2

            reassessmentfee_Request_data.remarks = request.POST.get('remarks')    
            
            reassessmentfee_Request_data.save()
            messages.success(request,"Fee status updated successfully")
            return redirect('/reassessment/list')
        # else:
        #     return redirect('/feetransaction/list')

    else:
        form = reassessmentfee_update_Form()

    data['form'] = form

    return render(request, 'update_reassessment.html', data)


def reassessment_attendance_update(request,id):
    attendance_data = get_object_or_404(reassessmentteacher, id=id)
    if request.method == 'POST':

        form = attendance_reassessment_form(request.POST)

        if form.is_valid():

            # process form data
            obj = reassessmentteacher.objects.get(id=id)  # gets new object
            obj.attendance = form.cleaned_data['attendance']
            obj.save()
            messages.success(request,"Marks updated successfully")
            return redirect('reassessment_update_attendance')

    return redirect('reassessment_update_attendance')

def reassessment_attendance_list(request):
    data = {}

    activetab = 'list'
    
    attendance_data = reassessmentteacher.objects.all()
    form = attendance_reassessment_form()
    
    data['attendance_data'] = attendance_data
    data['form'] = attendance_reassessment_form()
    data['activetab'] = activetab
    

    return render(request, 'reassessment_attendance_list.html', data)

def update_reassessment_marks(request,course_id):
    data = {}
    cur_cor = Course.objects.get(id = course_id)
    result_data = reassessmentteacher.objects.filter(courseid_id=course_id)

    marks_form = MarksUpdate(max_marks=cur_cor.max_marks)

    if request.method == 'POST':
        print(request.POST)
         
        marks_form = MarksUpdate(request.POST, max_marks=cur_cor.max_marks)
        
        if marks_form.is_valid():
            obj = reassessmentteacher.objects.get(courseid_id = course_id , seatno__seatno = request.POST.get("seat_no"))
            obj.marks = request.POST.get("marks")
            if int(request.POST.get("marks")) < int(cur_cor.pass_marks) :
                    obj.pass_fail = 0
            else :
                    obj.pass_fail = 1
            obj.save()
    else:
        marks_form = MarksUpdate(max_marks=cur_cor.max_marks)
    

    data['course'] = cur_cor
    data["result"] = result_data
    data["marks_form"] = marks_form
    return render(request, "update_reassessment_marks.html", data)