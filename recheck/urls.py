from django.conf.urls import url
from django.urls import path, include 
from . import views

urlpatterns = [
    path('view_request', views.recheck_request, name='view_recheck_request'),
    path('list', views.recheck_approved, name='recheck_approved'),    
    path('list/<int:id>/update', views.recheck_update, name='recheck_approved_id'), 
    path('home', views.recheck_home, name='recheck_home'),
    path('update_marks/course/<int:course_id>', views.update_marks, name='recheck_update_marks'),   
]  