
from django.contrib import messages
from django.shortcuts import render, redirect, get_object_or_404
from .forms import recheck_fee_form , recheckfee_update_Form
from .models import recheckfee ,recheckteacher
from exam_appear.models import appearance , attendance
from results.forms import MarksUpdate
from results.models import result
from course.models import Course
# Create your views here.
  


def recheck_home(request):
    
    data = {}
    data['Courses'] = Course.objects.all()
    data['activetab'] = 'list'
    return render(request, "recheck_home.html", data)

def recheck_request(request):
    data ={}
    form = recheck_fee_form(student_id = request.user.Student.id)
    recheckdata = recheckfee.objects.filter(student_id=request.user.Student.id)
    activetab = 'list'

    if request.method == 'POST':

        if 'submit_request' in request.POST:
            
            activetab = 'add'

            recheck_form = recheck_fee_form(request.POST,student_id = request.user.Student.id)
            
            
            if recheck_form.is_valid() :                

                recheckfee_data = recheckfee()

                recheckfee_data.receipt_no = request.POST.get('receipt_no')
                recheckfee_data.payment_mode = request.POST.get('payment_mode')
                recheckfee_data.transaction_id = request.POST.get('transaction_id')
                recheckfee_data.cheque_no = request.POST.get('cheque_no')
                recheckfee_data.payment_date = request.POST.get('payment_date')
                if 'payment_proof' in request.FILES:
                    recheckfee_data.payment_proof = request.FILES['payment_proof']
                else:
                    recheckfee_data.payment_proof = None
                recheckfee_data.amount = request.POST.get('amount')
                recheckfee_data.student_id = request.user.Student.id
                recheckfee_data.courseid = request.POST.getlist('courseid')
                recheckfee_data.approved = 0
                recheckfee_data.save()
                
                activetab = 'list'

                messages.success(request,"Recheck applied successfully")

    else:
        form = recheck_fee_form(student_id = request.user.Student.id)
        
    data["form"] = form
    data["data"] = recheckdata
    data["activetab"] = activetab


    return render(request, 'requestrecheck.html', data)

def recheck_approved(request):
    data = {}

    activetab = 'list'

    recheckfee_Request_data = recheckfee.objects.filter(approved=0)

    
    data['recheckfee_Request_data'] = recheckfee_Request_data
    
    data['activetab'] = activetab

    return render(request, 'recheck_list.html', data)

 
def recheck_update(request, id):
    
    recheckfee_Request_data = get_object_or_404(recheckfee, id=id)
    data = {}
    
    form = recheckfee_update_Form()

    if request.method == 'POST':
        form = recheckfee_update_Form(request.POST)
        if form.is_valid():
            #  Fee_Request.objects.get(id = request.POST.get('receipt_no'))

            approved = request.POST.get('approved', None)
            
            if int(approved) == 1:
                
                recheckfee_Request_data.approved = 1
                seat =  get_object_or_404(appearance, student_id=recheckfee_Request_data.student.id)
                
                for course in eval(recheckfee_Request_data.courseid):
                    recheckteacherdata = recheckteacher()
                    recheckteacherdata.seatno_id = seat.id
                    recheckteacherdata.courseid_id = int(course)
                    recheckteacherdata.status = 0
                    recheckteacherdata.save()
                
            elif int(approved) == 0:
                recheckfee_Request_data.approved = 2

            recheckfee_Request_data.remarks = request.POST.get('remarks')    
            
            recheckfee_Request_data.save()
            messages.success(request,"Fee status updated successfully")
            return redirect('/recheck/list')
        # else:
        #     return redirect('/feetransaction/list')

    else:
        form = recheckfee_update_Form()

    data['form'] = form

    return render(request, 'update_recheck.html', data)

def update_marks(request,course_id):
    data = {}
    cur_cor = Course.objects.get(id = course_id)
    result_data = recheckteacher.objects.filter(courseid_id=course_id)

    marks_form = MarksUpdate(max_marks=cur_cor.max_marks)

    if request.method == 'POST':
        print(request.POST)
         
        marks_form = MarksUpdate(request.POST, max_marks=cur_cor.max_marks)
        
        if marks_form.is_valid():
            obj = result.objects.get(course_id_id = course_id , seatno__seatno__seatno = request.POST.get("seat_no"))
            obj.marks = request.POST.get("marks")
            if int(request.POST.get("marks")) < int(cur_cor.pass_marks) :
                    obj.pass_fail = 0
            else :
                    obj.pass_fail = 1
            obj.save()
    else:
        marks_form = MarksUpdate(max_marks=cur_cor.max_marks)
    

    data['course'] = cur_cor
    data["result"] = result_data
    data["marks_form"] = marks_form
    return render(request, "update_marks.html", data)