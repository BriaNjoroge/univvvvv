from django.shortcuts import render
from .forms import UniRegForm
from django.shortcuts import redirect
from login.models import University
from django.contrib.auth.hashers import make_password
from rolepermissions.roles import assign_role
from django.contrib.auth.models import User

# Create your views here.

def uni_register(request):
    if request.method == 'POST':

        form = UniRegForm(request.POST)

        if form.is_valid():
            
            form = UniRegForm()


            userdata = User()
            userdata.username = request.POST.get('username')
            userdata.password = make_password(request.POST.get('password'))
            userdata.save()

            unidata = University()
            unidata.name = request.POST.get('name')
            unidata.user_id = userdata.id
            unidata.save()

            assign_role(userdata, 'university')

            return redirect('login')
            
    else:
        form = UniRegForm()

    return render(request, 'university_register.html', {'form': form})