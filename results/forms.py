from django import forms
from exam_appear.models import attendance,appearance
from course.models import Course


class MarksInput(forms.Form):
    
    def __init__(self, *args, **kwargs):
        self.field_count = kwargs.pop('field_count', 0)
        self.max_marks = kwargs.pop('max_marks', 0)
        super(MarksInput, self).__init__(*args, **kwargs)
        
        for i in range(self.field_count):
            self.fields['marks'+str(i)] = forms.CharField(label="Marks obtained *", max_length=3, widget=forms.TextInput(attrs={'placeholder':'Marks','class': "form-control", 'name':'marks'}))


    def clean(self):
        cleaned_data = super(MarksInput, self).clean()
        
        for i in range(self.field_count):
            marks = cleaned_data.get('marks'+str(i)).strip()
            print(marks)
            
            if marks == "":
                self.add_error('marks'+str(i), "Please insert marks")
            elif marks.replace('.', '').isdigit():
                if eval(marks) < 0 or eval(marks) > self.max_marks:
                    self.add_error('marks'+str(i), "Insert marks between 0 and "+str(self.max_marks))
            elif marks != "AB":
                self.add_error('marks'+str(i), "Please enter proper marks")
            

class MarksUpdate(forms.Form):
    seat_no = forms.CharField(label="Seat Number", widget=forms.TextInput(attrs={'placeholder':'Seat No','class': "form-control"}))
    marks = forms.CharField(label="Marks obtained *", max_length=3, widget=forms.TextInput(attrs={'placeholder':'Marks','class': "form-control"}))

    def __init__(self, *args, **kwargs):
        self.max_marks = kwargs.pop('max_marks', 0)
        
        super(forms.Form, self).__init__(*args, **kwargs)
        self.fields['seat_no'].widget.attrs['readonly'] = True
    
    def clean(self):
        cleaned_data = super(MarksUpdate, self).clean()
        
        marks = cleaned_data.get('marks').strip()
        print(marks)
        
        if marks == "":
            self.add_error('marks', "Please insert marks")
        elif marks.replace('.', '').isdigit():
            if eval(marks) < 0 or eval(marks) > self.max_marks:
                self.add_error('marks', "Insert marks between 0 and "+str(self.max_marks))
        elif marks != "AB":
            self.add_error('marks', "Please enter proper marks")