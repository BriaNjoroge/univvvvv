from django.db import models
from django.conf import settings
from course.models import Course
from exam_appear.models import attendance

# Create your models here.
class result(models.Model):
    seatno = models.ForeignKey(attendance,on_delete=models.CASCADE)
    course_id = models.ForeignKey(Course, on_delete=models.CASCADE)
    marks = models.CharField(max_length=3)
    pass_fail = models.BooleanField()
    created_date = models.DateTimeField(auto_now_add = True)
    updated_date = models.DateTimeField(auto_now = True)
    # created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)