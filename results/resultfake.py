
from results.models import result
from exam_appear.models import attendance, appearance
from exam_registration.models import exam_registration
from student.models import Student
import datetime
import random
from course.models import Course

from faker import Faker
fake = Faker()

def input_marks():
    
    all_appeared = attendance.objects.all()
    
    for idx, appeared_student in enumerate(all_appeared):
        obj = result()
        obj.seatno = appeared_student
        obj.course_id_id = appeared_student.courseid.id
        cur_cor = Course.objects.get(id = appeared_student.courseid.id)
        mark = random.randrange(30,100,5)
        obj.marks = mark
        if mark < int(cur_cor.pass_marks) :
            obj.pass_fail = 0
        else :
            obj.pass_fail = 1
        # obj.created_by_id = request.user.id
        obj.save()

        
 