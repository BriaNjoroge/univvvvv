from django.conf.urls import url
from django.urls import path
from . import views

urlpatterns = [
    path('home', views.result_home, name='result_home'),
    path('input-marks/course/<int:course_id>', views.input_marks, name='input_marks'),
    path('display_marks/course/<int:course_id>', views.display_marks, name='display_marks'),
]