from django.shortcuts import render, redirect, reverse
from course.models import Course
from exam_appear.models import attendance, appearance
from exam_registration.models import exam_registration
from student.models import Student
from .models import result
from .forms import MarksInput, MarksUpdate


def result_home(request):
    print("hello2")
    data = {}
    data['Courses'] = Course.objects.all()
    data['activetab'] = 'list'
    return render(request, "result_home.html", data)


def input_marks(request,course_id):
    data = {}
    cur_cor = Course.objects.get(id = course_id)
    all_appeared = attendance.objects.filter(courseid = course_id)
    
    all_students = Student.objects.filter(current_sem = Course.objects.filter(id=course_id).values('semester_id'))
    # cnt = all_students.count()
    form = MarksInput(field_count=all_appeared.count(), max_marks=cur_cor.max_marks)

    if request.method == 'POST':
        form = MarksInput(request.POST, field_count=all_appeared.count(), max_marks=cur_cor.max_marks)
        
        if form.is_valid():
            print("hello")

            for idx, appeared_student in enumerate(all_appeared):
                obj = result()
                obj.seatno = appeared_student
                obj.course_id_id = course_id
                obj.marks = request.POST.get("marks"+str(idx))
                if int(request.POST.get("marks"+str(idx))) < int(cur_cor.pass_marks) :
                    obj.pass_fail = 0
                else :
                    obj.pass_fail = 1
                # obj.created_by_id = request.user.id
                obj.save()

            return redirect(reverse('result_home'))
    else:
        form = MarksInput(field_count=all_appeared.count(), max_marks=cur_cor.max_marks)
        
    if len(all_appeared) > 0 and result.objects.filter(seatno_id=all_appeared[0].id):
        data['records_exists'] = True
        print("records all ready exists")

    data['forms1'] = zip(form, all_appeared)
    data['course'] = cur_cor
    print(data)
    return render(request, 'input_marks.html', data)

 
def display_marks(request,course_id):
    data = {}
    cur_cor = Course.objects.get(id = course_id)
    result_data = result.objects.filter(course_id=course_id)

    marks_form = MarksUpdate(max_marks=cur_cor.max_marks)

    if request.method == 'POST':
        print(request.POST)
         
        marks_form = MarksUpdate(request.POST, max_marks=cur_cor.max_marks)
        
        if marks_form.is_valid():
            obj = result.objects.get(id = request.POST.get('result_id'))
            obj.marks = request.POST.get("marks")
            
            if int(request.POST.get("marks")) < int(cur_cor.pass_marks) :
                    obj.pass_fail = 0
            else :
                    obj.pass_fail = 1
            obj.save()
    else:
        marks_form = MarksUpdate(max_marks=cur_cor.max_marks)
    

    data['course'] = cur_cor
    data["result"] = result_data
    data["marks_form"] = marks_form
    return render(request, "display_marks.html", data)