from django.db import models
from programme.models import Programme
from django.contrib.auth.models import User
from django.conf import settings


# Create your models here.
class Semester(models.Model):
    name = models.CharField(max_length=30)
    programme_id = models.ForeignKey(Programme, on_delete=models.CASCADE)
    no_of_course = models.SmallIntegerField(default=1)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name
