from django.contrib import admin
from .models import Student
from django.utils.html import format_html

@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    list_display = ('thumbnail_tag','firstName','lastName','admission','created_by')
    # readonly_fields = ('admission',)
    search_fields = ('admission','firstName','firstName',)
    list_filter = ('programme',)

    def thumbnail_tag(self, obj):
        if obj.thumbnail:
            return format_html(
                '<img src="%s" style="width:50px;height:50px;""/>' % obj.thumbnail.url
            )
        return "-"

    thumbnail_tag.short_description = "Thumbnail"


