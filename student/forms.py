import datetime
from datetime import date

from dateutil.relativedelta import relativedelta
from django import forms

from semester.models import Semester
from .models import Student
from .choices import NATIONALITIES, RELIGION, GENDER_CHOICES, COUNTRIES, MARITAL_STATUS, STUDY_MODE, STUDENT_CATEGORY
from college.models import College
from programme.models import Programme
from fee_structure.choices import SEMISTER_CHOICES
from .choices import current_year, year_choices


class StudentForm(forms.Form):
    # Personal Information
    admission = forms.CharField(max_length=50,
                                widget=forms.TextInput(
                                    attrs={'placeholder': 'Student Registration Number',
                                           'class': "form-control"}))
    photo = forms.FileField(widget=forms.FileInput(
        attrs={'class': "form-control", 'placeholder': 'Student Photo', 'accept': 'image/*'}))
    firstName = forms.CharField(max_length=50,
                                widget=forms.TextInput(attrs={'placeholder': 'First Name', 'class': "form-control"}))

    middleName = forms.CharField(max_length=50, required=False,
                                 widget=forms.TextInput(attrs={'placeholder': 'Middle Name', 'class': "form-control"}))

    lastName = forms.CharField(max_length=50,
                               widget=forms.TextInput(attrs={'placeholder': 'Last Name', 'class': "form-control"}))

    gender = forms.ChoiceField(choices=GENDER_CHOICES, widget=forms.Select(attrs={'class': "form-control"}))

    email = forms.CharField(max_length=30,
                            widget=forms.EmailInput(attrs={'placeholder': 'Student Email', 'class': "form-control"}))

    dob = forms.DateField(widget=forms.widgets.DateTimeInput(attrs={"type": "date", 'class': "form-control"}))

    maritalStatus = forms.ChoiceField(choices=MARITAL_STATUS, widget=forms.Select(attrs={'class': "form-control"}))

    religion = forms.ChoiceField(choices=RELIGION, widget=forms.Select(attrs={'class': "form-control"}))

    nationality = forms.ChoiceField(choices=NATIONALITIES, widget=forms.Select(attrs={'class': "form-control"}))

    mobile = forms.IntegerField(widget=forms.NumberInput(
        attrs={"min":0,'placeholder': 'Student mobile number', 'class': "form-control"}))

    aadhar = forms.IntegerField(widget=forms.NumberInput(
        attrs={"min":0,'placeholder': 'Student Aadhar Number', 'class': "form-control"}))

    caste = forms.CharField(max_length=50, required=False,
                            widget=forms.TextInput(attrs={'placeholder': 'Last Name', 'class': "form-control"}))

    subcaste = forms.CharField(max_length=50, required=False,
                               widget=forms.TextInput(attrs={'placeholder': 'Last Name', 'class': "form-control"}))

    # Guardian
    guardian = forms.CharField(max_length=30,
                               widget=forms.TextInput(attrs={'placeholder': 'Guardian Name', 'class': "form-control"}))

    guardianRelationship = forms.CharField(max_length=30, widget=forms.TextInput(
        attrs={'placeholder': 'Relationship with Guardian', 'class': "form-control"}))

    guardianproffession = forms.CharField(max_length=30, widget=forms.TextInput(
        attrs={'placeholder': 'Proffession of the Guardian', 'class': "form-control"}))

    guardiancontact = forms.IntegerField(widget=forms.NumberInput(
        attrs={"min":0,'placeholder': 'Contact Number of the Guardian', 'class': "form-control"}))

    # address
    country = forms.ChoiceField(choices=COUNTRIES, widget=forms.Select(attrs={'class': "form-control"}))
    city = forms.CharField(max_length=30, widget=forms.TextInput(
        attrs={'placeholder': 'Student City of Origin', 'class': "form-control"}))

    address = forms.CharField(max_length=30,
                              widget=forms.TextInput(attrs={'placeholder': 'Student Address', 'class': "form-control"}))

    zipcode = forms.CharField(max_length=30,
                              widget=forms.TextInput(attrs={'placeholder': 'Area Zipcode', 'class': "form-control"}))

    # Previous Education

    previousuniversity = forms.CharField(max_length=30,
                                         widget=forms.TextInput(
                                             attrs={'placeholder': 'Previous University Attended',
                                                    'class': "form-control"}))
    previouscollege = forms.CharField(max_length=30,
                                      widget=forms.TextInput(
                                          attrs={'placeholder': 'Previous College Attended', 'class': "form-control"}))
    latestacademic = forms.CharField(max_length=30,
                                     widget=forms.TextInput(
                                         attrs={'placeholder': 'Latest Academic Achievement', 'class': "form-control"}))
    lastdegree = forms.FileField(widget=forms.FileInput(
        attrs={'class': "form-control", 'placeholder': 'Latest certificate acquired',
               'accept': 'application/pdf,image/*'}))

    # Current Registration
    enrollmentdate = forms.DateField(
        widget=forms.widgets.DateTimeInput(attrs={"type": "date", 'class': "form-control"}))
    studymode = forms.ChoiceField(choices=STUDY_MODE, widget=forms.Select(attrs={'class': "form-control"}))

    # student current course
    college = forms.ModelChoiceField(queryset=College.objects.all(), empty_label="-",
                                     widget=forms.Select(attrs={'class': "form-control"}))
    programme = forms.ChoiceField(widget=forms.Select(attrs={'class': "form-control"}))

    batch = forms.ChoiceField(choices=year_choices, required=False,
                              widget=forms.Select(attrs={'class': "form-control"}))

    category = forms.ChoiceField(choices=STUDENT_CATEGORY, widget=forms.Select(attrs={'class': "form-control"}))

    is_active = forms.BooleanField(required=False,
                                   widget=forms.CheckboxInput(attrs={'class': "form-control fancy-checkbox"}))
    current_sem = forms.ChoiceField(widget=forms.Select(attrs={'class': "form-control"}))

    def clean(self):
        cleaned_data = super(StudentForm, self).clean()

        dob = cleaned_data.get('dob')
        mobile = cleaned_data.get('mobile')
        aadhar = cleaned_data.get('aadhar')
        guardiancontact = cleaned_data.get('guardiancontact')

        if aadhar > 999999999999:
            self.add_error('aadhar', "Please enter a valid aadhar number")
        if aadhar < 0:
            self.add_error('aadhar', "Please enter a valid aadhar number")

        if mobile > 9999999999:
            self.add_error('mobile', "Please enter a valid mobile number")
        if mobile < 0:
            self.add_error('mobile', "Please enter a valid mobile number")

        if guardiancontact > 9999999999:
            self.add_error('guardiancontact', "Please enter a valid mobile number")
        if guardiancontact < 0:
            self.add_error('guardiancontact', "Please enter a valid mobile number")

        if dob > datetime.date.today() + relativedelta(years=-20):
            self.add_error('dob', "Should be above 20 years")

        if Student.objects.filter(admission=cleaned_data.get('admission')).exists():
            self.add_error('admission', "This Admission Number already exists.")

        if Student.objects.filter(email=cleaned_data.get('email')).exists():
            self.add_error('email', "This Email already exists.")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].label = Student._meta.get_field(field).verbose_name.title()

        all_sems = set(Semester.objects.all().values_list('id', flat=True))
        all_progs = set(Programme.objects.all().values_list('id', flat=True))

        CHOICES = list()
        for sem in all_sems:
            CHOICES.append((sem, sem))

        CHOICES2 = list()
        for progs in all_progs:
            CHOICES2.append((progs, progs))

        self.fields['current_sem'].choices = CHOICES
        self.fields['programme'].choices = CHOICES2


class StudentEditForm(forms.Form):
    # Personal Information

    firstName = forms.CharField(max_length=50,
                                widget=forms.TextInput(attrs={'placeholder': 'First Name', 'class': "form-control"}))
    middleName = forms.CharField(max_length=50, required=False,
                                 widget=forms.TextInput(attrs={'placeholder': 'Middle Name', 'class': "form-control"}))
    lastName = forms.CharField(max_length=50,
                               widget=forms.TextInput(attrs={'placeholder': 'Last Name', 'class': "form-control"}))

    gender = forms.ChoiceField(choices=GENDER_CHOICES, widget=forms.Select(attrs={'class': "form-control"}))

    dob = forms.DateField(widget=forms.widgets.DateTimeInput(attrs={"type": "date", 'class': "form-control"}))

    maritalStatus = forms.ChoiceField(choices=MARITAL_STATUS, widget=forms.Select(attrs={'class': "form-control"}))

    religion = forms.ChoiceField(choices=RELIGION, widget=forms.Select(attrs={'class': "form-control"}))

    nationality = forms.ChoiceField(choices=NATIONALITIES, widget=forms.Select(attrs={'class': "form-control"}))

    mobile = forms.IntegerField(widget=forms.NumberInput(
        attrs={"min":0,'placeholder': 'Student mobile number', 'class': "form-control"}))

    aadhar = forms.IntegerField(widget=forms.NumberInput(
        attrs={"min":0,'placeholder': 'Student Aadhar Number', 'class': "form-control"}))

    caste = forms.CharField(max_length=50, required=False,
                            widget=forms.TextInput(attrs={'placeholder': 'Last Name', 'class': "form-control"}))

    subcaste = forms.CharField(max_length=50, required=False,
                               widget=forms.TextInput(attrs={'placeholder': 'Last Name', 'class': "form-control"}))

    # Guardian
    guardian = forms.CharField(max_length=30,
                               widget=forms.TextInput(attrs={'placeholder': 'Guardian Name', 'class': "form-control"}))

    guardianRelationship = forms.CharField(max_length=30, widget=forms.TextInput(
        attrs={'placeholder': 'Relationship with Guardian', 'class': "form-control"}))

    guardianproffession = forms.CharField(max_length=30, widget=forms.TextInput(
        attrs={'placeholder': 'Proffession of the Guardian', 'class': "form-control"}))

    guardiancontact = forms.IntegerField(widget=forms.NumberInput(
        attrs={"min":0,'placeholder': 'Contact Number of the Guardian', 'class': "form-control"}))

    # address
    country = forms.ChoiceField(choices=COUNTRIES, widget=forms.Select(attrs={'class': "form-control"}))

    city = forms.CharField(max_length=30,
                           widget=forms.TextInput(
                               attrs={'placeholder': 'Student City of Origin',
                                      'class': "form-control"}))
    address = forms.CharField(max_length=100,
                              widget=forms.TextInput(
                                  attrs={'placeholder': 'Student Address',
                                         'class': "form-control"}))

    zipcode = forms.CharField(max_length=30,
                              widget=forms.TextInput(
                                  attrs={'placeholder': 'Area Zipcode',
                                         'class': "form-control"}))

    # Previous Education

    previousuniversity = forms.CharField(max_length=30,
                                         widget=forms.TextInput(
                                             attrs={'placeholder': 'Previous University Attended',
                                                    'class': "form-control"}))
    previouscollege = forms.CharField(max_length=30,
                                      widget=forms.TextInput(
                                          attrs={'placeholder': 'Previous College Attended', 'class': "form-control"}))
    latestacademic = forms.CharField(max_length=30,
                                     widget=forms.TextInput(
                                         attrs={'placeholder': 'Latest Academic Achievement', 'class': "form-control"}))

    # Current Registration
    # enrollmentdate = forms.DateField(
    #     widget=forms.widgets.DateTimeInput(attrs={"type": "date", 'class': "form-control"}))
    studymode = forms.ChoiceField(choices=STUDY_MODE, widget=forms.Select(attrs={'class': "form-control"}))

    # student current course
    college = forms.ModelChoiceField(queryset=College.objects.all(), empty_label="-",
                                     widget=forms.Select(attrs={'class': "form-control"}))
    programme = forms.ChoiceField(widget=forms.Select(attrs={'class': "form-control"}))

    batch = forms.ChoiceField(choices=year_choices, required=False,
                              widget=forms.Select(attrs={'class': "form-control"}))

    category = forms.ChoiceField(choices=STUDENT_CATEGORY, widget=forms.Select(attrs={'class': "form-control"}))

    is_active = forms.BooleanField(required=False,
                                   widget=forms.CheckboxInput(attrs={'class': "form-control fancy-checkbox "}))
    current_sem = forms.ChoiceField(widget=forms.Select(attrs={'class': "form-control"}))

    def clean(self):
        cleaned_data = super(StudentEditForm, self).clean()
        dob = cleaned_data.get('dob')
        mobile = cleaned_data.get('mobile')
        aadhar = cleaned_data.get('aadhar')
        guardiancontact = cleaned_data.get('guardiancontact')

        if aadhar > 999999999999:
            self.add_error('aadhar', "Please enter a valid aadhar number")
        if aadhar < 0:
            self.add_error('aadhar', "Please enter a valid aadhar number")

        if mobile > 9999999999:
            self.add_error('mobile', "Please enter a valid mobile number")
        if mobile < 0:
            self.add_error('mobile', "Please enter a valid mobile number")

        if guardiancontact > 9999999999:
            self.add_error('guardiancontact', "Please enter a valid mobile number")
        if guardiancontact < 0:
            self.add_error('guardiancontact', "Please enter a valid mobile number")

        if dob > datetime.date.today() + relativedelta(years=-20):
            self.add_error('dob', "Should be above 20 years")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            # print(field)
            self.fields[field].label = Student._meta.get_field(field).verbose_name.title()

        all_sems = set(Semester.objects.all().values_list('id', flat=True))
        all_progs = set(Programme.objects.all().values_list('id', flat=True))

        CHOICES = list()
        for sem in all_sems:
            CHOICES.append((sem, sem))

        CHOICES2 = list()
        for progs in all_progs:
            CHOICES2.append((progs, progs))

        self.fields['current_sem'].choices = CHOICES
        self.fields['programme'].choices = CHOICES2
