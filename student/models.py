from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from datetime import datetime
from programme.models import Programme
from course.models import Course
from college.models import College
from django.contrib.auth.models import User
from io import BytesIO
import os
from .choices import current_year,year_choices

from django.core.files.base import ContentFile
from django.contrib.auth.hashers import make_password

from semester.models import Semester

THUMB_SIZE = (200, 200)

from PIL import Image


class Student(models.Model):
    """
    This model is for creating the Student module
    """

    # Created by
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, default=1)
    created_at = models.DateTimeField(default=datetime.now)
    # student personal info
    photo = models.ImageField(upload_to='photo/',verbose_name='Photo *')
    thumbnail = models.ImageField(
        upload_to="photo/thumbnails/", null=True, editable=False
    )
    admission = models.CharField(max_length=128, verbose_name='Registration number of the student *', unique=True)
    firstName = models.CharField(max_length=128, verbose_name='First Name *')
    middleName = models.CharField(max_length=128, blank=True, verbose_name='Middle Name')
    lastName = models.CharField(max_length=128, verbose_name='Last Name *')
    gender = models.CharField(max_length=128, verbose_name='Gender *')
    maritalStatus = models.CharField(max_length=128, verbose_name='Marital Status *')
    dob = models.DateField(blank=False, verbose_name='Date of Birth *')
    email = models.EmailField(unique=True, verbose_name='Email Address *')
    mobile = models.IntegerField(verbose_name='Mobile Number*')
    nationality = models.CharField(max_length=128, verbose_name='Nationality *')
    # aadhar = models.CharField(max_length=128, blank=True, verbose_name='Aadhar Card')
    aadhar =models.PositiveIntegerField(default=0,verbose_name='Aadhar Card',validators=[MaxValueValidator(999999999999),MinValueValidator(0)])
    religion = models.CharField(max_length=128, blank=True, verbose_name='Religion')
    caste = models.CharField(max_length=128, blank=True, verbose_name='Caste')
    subcaste = models.CharField(max_length=128, blank=True, verbose_name='Sub Caste')

    # student academics
    enrollmentdate = models.DateField(default=datetime.today, verbose_name='Enrollment Date *')
    previousuniversity = models.CharField(max_length=128, blank=True, verbose_name='Previous University attended')
    previouscollege = models.CharField(max_length=128, blank=True, verbose_name='Previous college attended')
    latestacademic = models.CharField(max_length=128, blank=True, verbose_name='Latest academic achievement')
    studymode = models.CharField(max_length=128, verbose_name='Study Mode *')
    lastdegree = models.FileField(upload_to='degree/', blank=True, verbose_name='Previous Academic Certificate *')

    # student gurdian
    guardian = models.CharField(max_length=128, verbose_name='Name of Guardian *')
    guardianRelationship = models.CharField(max_length=128, verbose_name='Relationship with Guardian *')
    guardianproffession = models.CharField(max_length=128, verbose_name='Profession of Guardian *')
    guardiancontact = models.PositiveIntegerField(default=0, verbose_name='phone contact of Guardian *',
                                validators=[MaxValueValidator(9999999999), MinValueValidator(0)])

    # student address

    country = models.CharField(max_length=128, verbose_name='Country of Origin *')
    city = models.CharField(max_length=128, verbose_name='City *')
    zipcode = models.CharField(max_length=128, blank=True, verbose_name='Zipcode *')
    address = models.CharField(max_length=128, blank=True, verbose_name='Current address ')

    # student current course
    college = models.ForeignKey(College, on_delete=models.CASCADE, related_name='college',
                                verbose_name='College to be admitted *')
    programme = models.ForeignKey(Programme, on_delete=models.CASCADE, related_name='programme',
                                  verbose_name='Programme to be admitted *')
    batch =  models.IntegerField( default=current_year,verbose_name="Batch *")
    category = models.CharField(default="Self Sponsored",max_length=128, verbose_name='Student category *')
    is_active = models.BooleanField(default=True)
    current_sem = models.ForeignKey(Semester,on_delete=models.CASCADE,verbose_name='Semester *',default=1)
    user = models.OneToOneField(User, related_name="Student", on_delete=models.CASCADE)

    def full_name(self):
        return f'{self.firstName} {self.lastName}'

    def __str__(self):
        return f'{self.firstName} {self.lastName}'

    def save(self, *args, **kwargs):
        """
        This function does 1 thing:
        1. Generating a automatic thumbnail for the photo
        """



        if not self.make_thumbnail():
            # set to a default thumbnail
            raise Exception('Could not create thumbnail - is the file type valid?')

        super(Student, self).save(*args, **kwargs)

    def make_thumbnail(self):

        image = Image.open(self.photo)
        image.thumbnail(THUMB_SIZE, Image.ANTIALIAS)

        thumb_name, thumb_extension = os.path.splitext(self.photo.name)
        thumb_extension = thumb_extension.lower()

        thumb_filename = thumb_name + '_thumb' + thumb_extension

        if thumb_extension in ['.jpg', '.jpeg']:
            FTYPE = 'JPEG'
        elif thumb_extension == '.gif':
            FTYPE = 'GIF'
        elif thumb_extension == '.png':
            FTYPE = 'PNG'
        else:
            return False  # Unrecognized file type

        # Save thumbnail to in-memory file as StringIO
        temp_thumb = BytesIO()
        image.save(temp_thumb, FTYPE)
        temp_thumb.seek(0)

        # set save=False, otherwise it will run in an infinite loop
        self.thumbnail.save(thumb_filename, ContentFile(temp_thumb.read()), save=False)
        temp_thumb.close()

        return True
