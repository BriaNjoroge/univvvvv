from semester.models import Semester
from student.models import Student
from datetime import datetime, date
from college.models import College
from django.contrib.auth.models import User
from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password
from rolepermissions.roles import assign_role
from faker import Faker
fake = Faker()
def studentfaker():
    for i in range(50):

        student = Student()  # gets new studentect
        student.photo = 'photo/IMG_20200102_130808646.jpg'
        student.admission = f'adm-{i+20}'
        student.firstName = 'FName ' + str(i)
        student.middleName = 'MName ' + str(i)
        student.lastName = 'LName ' + str(i)

        if i < 25:
            student.gender = 'Male'
        else:
            student.gender = 'Female'

        student.maritalStatus = 'Single'
        student.dob = date.today()
        student.email = f"abc123{i}@gmail{i}.com"
        student.mobile = "1234552344"

        student.nationality = 'Indian'
        student.aadhar = f'aadhar{i}'
        student.religion = 'Hindu'
        student.caste = 'caste'
        student.subcaste = 'subcaste'
        student.enrollmentdate = date.today()
        student.previousuniversity = 'previousuniversity'
        student.previouscollege = 'previouscollege'
        student.latestacademic = f'Bachelors'
        student.studymode = 'Full Time'
        student.lastdegree = 'photo/IMG_20200102_130808646.jpg'
        student.guardian = f'guardian{i}'
        student.guardianRelationship = 'guardianRelationship'
        student.guardianproffession = 'guardianproffession'
        student.guardiancontact = 'guardiancontact'
        student.country = 'India'
        student.city = 'city'
        student.zipcode = 'zipcode'
        student.address = 'address'

        if i % 2 == 0:
            student.programme_id = 1
            student.current_sem = Semester.objects.filter(programme_id_id=1)[0]
            student.college_id = 1
        else:
            student.programme_id = 2
            student.current_sem = Semester.objects.filter(programme_id_id=2)[0]
            student.college_id = 2

        userdata = User()

        userdata.username = student.admission
        userdata.password = make_password(student.admission)
        userdata.email = student.email
        userdata.save()

        assign_role(userdata, 'student')
        student.user_id = userdata.id

        student.batch = '2019'
        student.category = 'Government Sponsored'

        student.save()
