from django.urls import path
from . import views

urlpatterns = [
    path('', views.student_register, name='viewstudents'),
    path('student/<int:id>/view', views.viewperstudent, name='viewperstudent'),
    path('student/<int:id>/update', views.updateperstudent, name='updateperstudent'),
    path('student/<int:id>/update', views.updateperstudent, name='updateperstudent'),
    path('student/semester', views.semesterAjax, name='semesterAjax'),
    path('student/programme', views.programmeAjax, name='programmeAjax'),
    ]
