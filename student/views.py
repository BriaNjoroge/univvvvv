from django.shortcuts import render, get_object_or_404
from django.shortcuts import redirect
from django.http import JsonResponse
from datetime import date

# Student Registration Form
from programme.models import Programme
from semester.models import Semester
from student.models import Student
from student.forms import StudentForm, StudentEditForm
from django.contrib import messages
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password
from rolepermissions.roles import assign_role

from fee_request.models import Fee_Request

from fee_structure.models import Fee

# Create your views here.
def view_students(request):
    students = Student.objects.all()

    data = {
        'students': students
    }

    return render(request, 'student_list.html', data)


def student_register(request):
    students = Student.objects.order_by('-admission')

    navshow = 'list'

    if request.method == 'POST':
        navshow = 'add'

        form = StudentForm(request.POST, request.FILES)

        if form.is_valid():
            # process form data
            obj = Student()  # gets new object
            obj.photo = request.FILES['photo']

            if 'photo' in request.FILES:
                filename = request.FILES['photo']
                if str(filename).lower().split('.')[-1] in ["jpeg","png","jpg","svg"]:

                    obj.photo = request.FILES['photo']


                else:
                    messages.warning(request, "please upload an image")
                    navshow = 'add'
                    return redirect('viewstudents')
            else:
                obj.photo = None


            obj.admission = form.cleaned_data['admission']
            obj.firstName = form.cleaned_data['firstName']
            obj.middleName = form.cleaned_data['middleName']
            obj.lastName = form.cleaned_data['lastName']
            obj.gender = form.cleaned_data['gender']
            obj.maritalStatus = form.cleaned_data['maritalStatus']
            obj.dob = form.cleaned_data['dob']
            obj.email = form.cleaned_data['email']
            obj.mobile = form.cleaned_data['mobile']

            obj.nationality = form.cleaned_data['nationality']
            obj.aadhar = form.cleaned_data['aadhar']
            obj.religion = form.cleaned_data['religion']
            obj.caste = form.cleaned_data['caste']
            obj.subcaste = form.cleaned_data['subcaste']
            obj.enrollmentdate = form.cleaned_data['enrollmentdate']
            obj.previousuniversity = form.cleaned_data['previousuniversity']
            obj.previouscollege = form.cleaned_data['previouscollege']
            obj.latestacademic = form.cleaned_data['latestacademic']
            obj.studymode = form.cleaned_data['studymode']

            if 'lastdegree' in request.FILES:

                filename = request.FILES['lastdegree']
                if str(filename).lower().split('.')[-1] == 'pdf':
                    if int(len(request.FILES['lastdegree'].read())) < (3 * 1024 * 1024):
                        obj.lastdegree = request.FILES['lastdegree']

                    else:
                        messages.warning(request, "please upload a pdf within 3 mb")
                        return redirect('viewstudents')

                else:
                    messages.warning(request, "please upload a pdf in Previous Academic Certificate")
                    return redirect('viewstudents')

            else:
                obj.lastdegree = request.FILES['lastdegree']

            obj.guardian = form.cleaned_data['guardian']
            obj.guardianRelationship = form.cleaned_data['guardianRelationship']
            obj.guardianproffession = form.cleaned_data['guardianproffession']
            obj.guardiancontact = form.cleaned_data['guardiancontact']
            obj.country = form.cleaned_data['country']
            obj.city = form.cleaned_data['city']
            obj.zipcode = form.cleaned_data['zipcode']
            obj.address = form.cleaned_data['address']
            obj.college = form.cleaned_data['college']
            obj.programme_id = form.cleaned_data['programme']
            obj.batch = form.cleaned_data['batch']
            obj.category = form.cleaned_data['category']
            obj.current_sem_id = form.cleaned_data['current_sem']


            if request.user.is_authenticated:
                obj.created_by_id = request.user.id
            # finally save the object in db
            else:
                pass
            userdata = User()
            userdata.username = form.cleaned_data['admission']
            userdata.password = make_password(form.cleaned_data['admission'])
            userdata.email = form.cleaned_data['email']
            userdata.save()

            assign_role(userdata, 'student')
            obj.user_id = userdata.id
            obj.save()
            messages.success(request, f"{form.cleaned_data['admission']} has been added successfully")
            return redirect('viewstudents')

    else:
        form = StudentForm()

    return render(request, 'student_list.html', {'form': form,
                                                 'students': students,
                                                 'navshow':navshow

                                                 })


def viewperstudent(request, id):
    student = get_object_or_404(Student, id=id)

    context = {
        'student': student
    }

    return render(request, 'perstudent.html', context)

from datetime import datetime

def updateperstudent(request, id):
    student = get_object_or_404(Student, id=id)

    initial = {
        'firstName': student.firstName,
        'middleName': student.middleName,
        'lastName': student.lastName,
        'gender': student.gender,
        'maritalStatus': student.maritalStatus,
        # 'dob': datetime.strptime(f'{student.dob}', '%Y-%m-%d').strftime('%m/%d/%y'),
        'mobile': student.mobile,
        'nationality': student.nationality,
        'aadhar': student.aadhar,
        'religion': student.religion,
        'caste': student.caste,
        'subcaste': student.subcaste,
        # 'enrollmentdate': student.enrollmentdate,
        'previousuniversity': student.previousuniversity,
        'previouscollege': student.previouscollege,
        'latestacademic': student.latestacademic,
        'studymode': student.studymode,
        'guardian': student.guardian,
        'guardianRelationship': student.guardianRelationship,
        'guardianproffession': student.guardianproffession,
        'guardiancontact': student.guardiancontact,
        'country': student.country,
        'city': student.city,
        'zipcode': student.zipcode,
        'address': student.address,
        'college': student.college,
        'programme': student.programme,
        'batch': student.batch,
        'category': student.category,
        'current_sem': student.current_sem,
    }

    form = StudentEditForm(initial=initial)

    if request.method == 'POST':

        form = StudentEditForm(request.POST)

        if form.is_valid():

            # process form data
            obj = Student.objects.filter(id=id)  # gets new object

            # obj.photo = request.FILES['photo']
            firstName = form.cleaned_data['firstName']
            middleName = form.cleaned_data['middleName']
            lastName = form.cleaned_data['lastName']
            gender = form.cleaned_data['gender']
            maritalStatus = form.cleaned_data['maritalStatus']
            # dob = form.cleaned_data['dob']
            # obj.email = form.cleaned_data['email']
            mobile = form.cleaned_data['mobile']

            nationality = form.cleaned_data['nationality']
            aadhar = form.cleaned_data['aadhar']
            religion = form.cleaned_data['religion']
            caste = form.cleaned_data['caste']
            subcaste = form.cleaned_data['subcaste']
            # enrollmentdate = form.cleaned_data['enrollmentdate']
            previousuniversity = form.cleaned_data['previousuniversity']
            previouscollege = form.cleaned_data['previouscollege']
            latestacademic = form.cleaned_data['latestacademic']
            studymode = form.cleaned_data['studymode']
            guardian = form.cleaned_data['guardian']
            guardianRelationship = form.cleaned_data['guardianRelationship']
            guardianproffession = form.cleaned_data['guardianproffession']
            guardiancontact = form.cleaned_data['guardiancontact']
            country = form.cleaned_data['country']
            city = form.cleaned_data['city']
            zipcode = form.cleaned_data['zipcode']
            address = form.cleaned_data['address']
            college = form.cleaned_data['college']
            programme = form.cleaned_data['programme']
            batch = form.cleaned_data['batch']
            category = form.cleaned_data['category']
            current_sem = form.cleaned_data['current_sem']




            if request.user.is_authenticated:
                created_by_id = request.user.id
            # finally save the object in db
            else:
                created_by_id = 1
            # fee_student = Fee_Request.objects.filter(student_id=id)
            # if fee_student.count() != 0:
            #
            #     fee_student = Fee_Request.objects.filter(student_id=id)
            #     remain_fee = fee_student.last().remaining_amount
            #
            #     if current_sem != obj[0].current_sem :
            #         student_country = ""
            #         if obj[0].nationality == "Indian":
            #             student_country = "Local"
            #
            #         else:
            #             student_country = "Foreigner"
            #         remain_fee +=  float(Fee.objects.filter(programme__name__icontains=obj[0].programme).filter(batch=obj[0].batch).filter(
            #         category__icontains=obj[0].category).filter(country__icontains=student_country).values_list('total_amount', flat=True).get(semister=current_sem))

            obj.update(created_by_id=created_by_id, firstName=firstName,
                       middleName=middleName,
                       lastName=lastName,
                       gender=gender,
                       maritalStatus=maritalStatus,
                       # dob=dob,
                       mobile=mobile,
                       nationality=nationality,
                       aadhar=aadhar,
                       religion=religion,
                       caste=caste,
                       subcaste=subcaste,
                       # enrollmentdate=enrollmentdate,
                       previouscollege=previouscollege,
                       previousuniversity=previousuniversity,
                       latestacademic=latestacademic,
                       studymode=studymode,
                       guardian=guardian,
                       guardianRelationship=guardianRelationship,
                       guardianproffession=guardianproffession,
                       guardiancontact=guardiancontact,
                       country=country,
                       city=city,
                       zipcode=zipcode,
                       address=address,
                       category=category,
                       current_sem=current_sem,
                       college=college, programme=programme, batch=batch)
            # if fee_student.count() != 0:
            #     Fee_Request.objects.filter(student_id=id , approved=None).update(
            #     remaining_amount=remain_fee)
            messages.success(request, 'Student record updated successfully!')
            return redirect('viewstudents')

    return render(request, 'update_student.html', {'form': form, 'student': student})

def semesterAjax(request):
    """

    Ajax to update semester choice field according to programme
    """
    sems =  list(Semester.objects.filter(programme_id_id=request.GET.get('programme_id')).values_list('id', 'name'))
    return  JsonResponse(sems, safe=False)

def programmeAjax(request):
    """

Ajax to update Programme choice field according to College
    """
    progs =  list(Programme.objects.filter(college_id=request.GET.get('college')).values_list('id', 'name'))
    return  JsonResponse(progs, safe=False)