from django.contrib import admin
from django.urls import path
from django.conf.urls.static import static
from django.conf import settings
from django.conf.urls import include, url
from django.shortcuts import redirect
from django.views.generic import RedirectView
from django.contrib.auth import views as auth_views

from login import views

urlpatterns = [

                  path('admin/', admin.site.urls),
                  path('login/', include('login.urls'), name='signin'),
                  path('logout/', views.logoutfun, name='logoutfun'),
                  path('dashboard/', include('dashboard.urls')),
                  path('register/', include('register.urls')),
                  path('college/', include('college.urls')),
                  path('programmes/', include('programme.urls')),
                  path('course/', include('course.urls')),
                  path('accomodation/', include('accomodation.urls')),
                  path('hostel/', include('hostel.urls')),
                  path('migration/', include('migration.urls')),
                  path('feetransaction/', include('fee_transaction.urls')),
                  path('convocation_registration/', include('convocation_registration.urls')),
                  path('students/', include('student.urls')),
                  path('fee_request/', include('fee_request.urls')),
                  path('', lambda request: redirect('login/', permanent=True)),
                  path('fee-structure/', include('fee_structure.urls')),
                  path('migration/', include('migration.urls')),
                  path('convocation-registration/', include('convocation_registration.urls')),
                  path('student-profile/', include('each_student_portal.urls')),
                  path('faculty/', include('faculty.urls')),
                  path('exam-appear/', include('exam_appear.urls')),
                  path('exam-registration/', include('exam_registration.urls')),
                  path('exam-timetable/', include('exam_timetable.urls')),
                  path('results/', include('results.urls')),
                  path('marksheet/', include('marksheet.urls')),
                  path('recheck/', include('recheck.urls')),
                  path('backlog/', include('backlog.urls')),
                  path('reassessment/', include('reassessment.urls')),
                  url(r'^password_reset/$', auth_views.PasswordResetView.as_view(), name='password_reset'),
                  url(r'^password_reset/done/$', auth_views.PasswordResetDoneView.as_view(),
                      name='password_reset_done'),
                  url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
                      auth_views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
                  url(r'^reset/done/$', auth_views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),
                  path('change-password', views.change_password, name='change_password'),

              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
